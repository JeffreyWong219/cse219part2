/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileInputStream;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cool7
 */
public class ValidateLoadedDataJUnitTest {
    
    JsonObject data;
    
    public ValidateLoadedDataJUnitTest() {
        try{
            InputStream is = new FileInputStream("C:\\Users\\cool7\\Desktop\\SiteSaveTest.json");
            JsonReader jsonReader = Json.createReader(is);
            JsonObject json = jsonReader.readObject();
            jsonReader.close();
            is.close();
            data = json;
        }
        catch(Exception e){
            System.out.println("whoops");
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void verifyCourseDetailsInformation(){
        JsonArray courseArray = data.getJsonArray("course_details");
        JsonObject items = courseArray.getJsonObject(0);
        assertEquals("CSE",items.getString("subject"));
        assertEquals(219,items.getInt("subject_number"));
        assertEquals("Fall",items.getString("semester"));
        assertEquals("2017",items.getString("year"));
        assertEquals("Computer Science III",items.getString("title"));
        assertEquals("Richard Mckenna",items.getString("instructor_name"));
        assertEquals("http://www.google.com",items.getString("instructor_home"));
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",items.getString("export_path"));
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",items.getString("template_path"));
        assertEquals("C:\\Users\\cool7\\Desktop\\pagefiles\\CSE219_files\\SBUDarkRedShieldLogo.png",items.getString("banner_path"));
        assertEquals("C:\\Users\\cool7\\Desktop\\pagefiles\\CSE219_files\\SBUWhiteShieldLogo.jpg",items.getString("left_path"));
        assertEquals("C:\\Users\\cool7\\Desktop\\pagefiles\\CSE219_files\\SBUDarkRedShieldLogo.png",items.getString("right_path"));
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",items.getString("css_path"));
        JsonArray templateFilesArray = items.getJsonArray("template_files");
        
        JsonObject templateFileOne = templateFilesArray.getJsonObject(0);
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",templateFileOne.getString("file_path"));
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",templateFileOne.getString("js_path"));
        assertEquals(true,templateFileOne.getBoolean("in_use"));
        assertEquals("main", templateFileOne.getString("nav_title"));
        
        JsonObject templateFileTwo = templateFilesArray.getJsonObject(1);
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",templateFileTwo.getString("file_path"));
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",templateFileTwo.getString("js_path"));
        assertEquals(true,templateFileTwo.getBoolean("in_use"));
        assertEquals("syllabus", templateFileTwo.getString("nav_title"));
        
        JsonObject templateFileThree = templateFilesArray.getJsonObject(2);
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",templateFileThree.getString("file_path"));
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",templateFileThree.getString("js_path"));
        assertEquals(true,templateFileThree.getBoolean("in_use"));
        assertEquals("schedule", templateFileThree.getString("nav_title"));
        
        JsonObject templateFileFour = templateFilesArray.getJsonObject(3);
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",templateFileFour.getString("file_path"));
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",templateFileFour.getString("js_path"));
        assertEquals(true,templateFileFour.getBoolean("in_use"));
        assertEquals("hws", templateFileFour.getString("nav_title"));
        
        JsonObject templateFileFive = templateFilesArray.getJsonObject(4);
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",templateFileFive.getString("file_path"));
        assertEquals("C:\\Users\\cool7\\Desktop\\CSE219Part2\\cse219part2\\TAManager\\CourseSiteGenerator",templateFileFive.getString("js_path"));
        assertEquals(false,templateFileFive.getBoolean("in_use"));
        assertEquals("projects", templateFileFive.getString("nav_title"));
        
        JsonArray subjectList = items.getJsonArray("subject_list");
        assertEquals("CSE",subjectList.getString(0));
        JsonArray subjectNumberList = items.getJsonArray("subject_number_list");
        assertEquals(219,subjectNumberList.getInt(0));
        JsonArray semesterList = items.getJsonArray("semester_list");
        assertEquals("Fall",semesterList.getString(0));
        JsonArray yearList = items.getJsonArray("year_list");
        assertEquals("2017",yearList.getString(0));
    }
    
    @Test
    public void verifyTADataInformation(){
        JsonArray taDataArray = data.getJsonArray("ta_data");
        JsonObject information = taDataArray.getJsonObject(0);
        
        assertEquals("9",information.getString("startHour"));
        assertEquals("20",information.getString("endHour"));
        
        JsonArray undergraduateArray = information.getJsonArray("undergrad_tas");
        assertEquals("testfirst1",undergraduateArray.getJsonObject(0).getString("name"));
        assertEquals("testemail1",undergraduateArray.getJsonObject(0).getString("email"));
        
        JsonArray graduateArray = information.getJsonArray("grad_tas");
        assertEquals("testfirst",graduateArray.getJsonObject(0).getString("name"));
        assertEquals("testemail",graduateArray.getJsonObject(0).getString("email"));
        
        JsonArray officeHoursArray = information.getJsonArray("officeHours");
        assertEquals("MONDAY",officeHoursArray.getJsonObject(0).getString("day"));
        assertEquals("12_00pm",officeHoursArray.getJsonObject(0).getString("time"));
        assertEquals("testfirst",officeHoursArray.getJsonObject(0).getString("name"));
        assertEquals("TUESDAY",officeHoursArray.getJsonObject(1).getString("day"));
        assertEquals("1_00pm",officeHoursArray.getJsonObject(1).getString("time"));
        assertEquals("testfirst1",officeHoursArray.getJsonObject(1).getString("name"));
        assertEquals("WEDNESDAY",officeHoursArray.getJsonObject(2).getString("day"));
        assertEquals("3_00pm",officeHoursArray.getJsonObject(2).getString("time"));
        assertEquals("testfirst",officeHoursArray.getJsonObject(2).getString("name"));
        assertEquals("TUESDAY",officeHoursArray.getJsonObject(3).getString("day"));
        assertEquals("4_00pm",officeHoursArray.getJsonObject(3).getString("time"));
        assertEquals("testfirst1",officeHoursArray.getJsonObject(3).getString("name"));
    }
    
    @Test
    public void verifyRecitationDataInformation(){
        JsonArray recitationDataArray = data.getJsonArray("recitation_data");
        JsonObject information = recitationDataArray.getJsonObject(0);
        
        JsonArray recitations = information.getJsonArray("recitations");
        assertEquals("01",recitations.getJsonObject(0).getString("section"));
        assertEquals("Mckenna",recitations.getJsonObject(0).getString("instructor"));
        assertEquals("Monday 11-12pm",recitations.getJsonObject(0).getString("day"));
        assertEquals("New CS 1111",recitations.getJsonObject(0).getString("location"));
        assertEquals("testfirst",recitations.getJsonObject(0).getString("ta1"));
        assertEquals("",recitations.getJsonObject(0).getString("ta2"));
        
        assertEquals("02",recitations.getJsonObject(1).getString("section"));
        assertEquals("Banerjee",recitations.getJsonObject(1).getString("instructor"));
        assertEquals("Thursday 1-2pm",recitations.getJsonObject(1).getString("day"));
        assertEquals("New CS 1111",recitations.getJsonObject(1).getString("location"));
        assertEquals("testfirst1",recitations.getJsonObject(1).getString("ta1"));
        assertEquals("",recitations.getJsonObject(1).getString("ta2"));
        
        assertEquals("03",recitations.getJsonObject(2).getString("section"));
        assertEquals("Mckenna",recitations.getJsonObject(2).getString("instructor"));
        assertEquals("Monday 11-12pm",recitations.getJsonObject(2).getString("day"));
        assertEquals("New CS 1111",recitations.getJsonObject(2).getString("location"));
        assertEquals("testfirst",recitations.getJsonObject(2).getString("ta1"));
        assertEquals("testfirst1",recitations.getJsonObject(2).getString("ta2"));
        
        assertEquals("04",recitations.getJsonObject(3).getString("section"));
        assertEquals("Banerjee",recitations.getJsonObject(3).getString("instructor"));
        assertEquals("Monday 11-12pm",recitations.getJsonObject(3).getString("day"));
        assertEquals("New CS 1111",recitations.getJsonObject(3).getString("location"));
        assertEquals("",recitations.getJsonObject(3).getString("ta1"));
        assertEquals("",recitations.getJsonObject(3).getString("ta2"));
    }
    
    @Test
    public void verifyScheduleDataInformation(){
        JsonArray scheduleDataArray = data.getJsonArray("schedule_data");
        JsonObject information = scheduleDataArray.getJsonObject(0);
        
        assertEquals("2017-04-17",information.getString("start_date"));
        assertEquals("2017-04-21",information.getString("end_date"));
        
        JsonArray scheduleEventArray = information.getJsonArray("schedules");
        assertEquals("holiday",scheduleEventArray.getJsonObject(0).getString("type"));
        assertEquals("Spring Break", scheduleEventArray.getJsonObject(0).getString("title"));
        assertEquals("123", scheduleEventArray.getJsonObject(0).getString("topic"));
        assertEquals("456", scheduleEventArray.getJsonObject(0).getString("time"));
        assertEquals("789", scheduleEventArray.getJsonObject(0).getString("criteria"));
        assertEquals("http://www.google.com",scheduleEventArray.getJsonObject(0).getString("link"));
        assertEquals("2017-04-16",scheduleEventArray.getJsonObject(0).getString("date"));
        
        assertEquals("hw",scheduleEventArray.getJsonObject(1).getString("type"));
        assertEquals("hw1", scheduleEventArray.getJsonObject(1).getString("title"));
        assertEquals("testing", scheduleEventArray.getJsonObject(1).getString("topic"));
        assertEquals("test", scheduleEventArray.getJsonObject(1).getString("time"));
        assertEquals("rest", scheduleEventArray.getJsonObject(1).getString("criteria"));
        assertEquals("http://www.google.com",scheduleEventArray.getJsonObject(1).getString("link"));
        assertEquals("2017-04-16",scheduleEventArray.getJsonObject(1).getString("date"));
        
        assertEquals("lecture",scheduleEventArray.getJsonObject(2).getString("type"));
        assertEquals("multithreading", scheduleEventArray.getJsonObject(2).getString("title"));
        assertEquals("", scheduleEventArray.getJsonObject(2).getString("topic"));
        assertEquals("", scheduleEventArray.getJsonObject(2).getString("time"));
        assertEquals("", scheduleEventArray.getJsonObject(2).getString("criteria"));
        assertEquals("http://www.google.com",scheduleEventArray.getJsonObject(2).getString("link"));
        assertEquals("2017-04-16",scheduleEventArray.getJsonObject(2).getString("date"));
        
        assertEquals("references",scheduleEventArray.getJsonObject(3).getString("type"));
        assertEquals("javafx api", scheduleEventArray.getJsonObject(3).getString("title"));
        assertEquals("", scheduleEventArray.getJsonObject(3).getString("topic"));
        assertEquals("", scheduleEventArray.getJsonObject(3).getString("time"));
        assertEquals("", scheduleEventArray.getJsonObject(3).getString("criteria"));
        assertEquals("http://www.google.com",scheduleEventArray.getJsonObject(3).getString("link"));
        assertEquals("2017-04-16",scheduleEventArray.getJsonObject(3).getString("date"));
    }
    
    @Test
    public void verifyProjectDataInformation(){
        JsonArray projectDataArray = data.getJsonArray("project_data");
        JsonObject information = projectDataArray.getJsonObject(0);
        
        JsonArray teamArray = information.getJsonArray("teams");
        assertEquals("Atomic Comics",teamArray.getJsonObject(0).getString("name"));
        assertEquals("235399", teamArray.getJsonObject(0).getString("color"));
        assertEquals("ffffff",teamArray.getJsonObject(0).getString("text_color"));
        assertEquals("http://www.google.com",teamArray.getJsonObject(0).getString("link"));
        
        assertEquals("C4 Comics",teamArray.getJsonObject(1).getString("name"));
        assertEquals("ffffff", teamArray.getJsonObject(1).getString("color"));
        assertEquals("552211",teamArray.getJsonObject(1).getString("text_color"));
        assertEquals("http://www.google.com",teamArray.getJsonObject(1).getString("link"));
        
        JsonArray studentsArray = information.getJsonArray("students");
        assertEquals("Beau",studentsArray.getJsonObject(0).getString("first_name"));
        assertEquals("Brummell",studentsArray.getJsonObject(0).getString("last_name"));
        assertEquals("Atomic Comics",studentsArray.getJsonObject(0).getString("team"));
        assertEquals("Lead Designer",studentsArray.getJsonObject(0).getString("role"));
        
        assertEquals("Jane",studentsArray.getJsonObject(1).getString("first_name"));
        assertEquals("Doe",studentsArray.getJsonObject(1).getString("last_name"));
        assertEquals("C4 Comics",studentsArray.getJsonObject(1).getString("team"));
        assertEquals("Lead Programmer",studentsArray.getJsonObject(1).getString("role"));
        
        assertEquals("Noonian",studentsArray.getJsonObject(2).getString("first_name"));
        assertEquals("Soong",studentsArray.getJsonObject(2).getString("last_name"));
        assertEquals("Atomic Comics",studentsArray.getJsonObject(2).getString("team"));
        assertEquals("Data Designer",studentsArray.getJsonObject(2).getString("role"));
    }
}
