package course_site.style;

import course_site.data.Recitation;
import course_site.data.ScheduleEvent;
import course_site.data.Student;
import djf.AppTemplate;
import djf.components.AppStyleComponent;
import java.util.HashMap;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import course_site.data.TeachingAssistant;
import course_site.data.Team;
import course_site.workspaces.CourseEditorTab;
import course_site.workspaces.CourseSiteWorkspace;
import course_site.workspaces.ProjectTab;
import course_site.workspaces.RecitationTab;
import course_site.workspaces.ScheduleTab;
import course_site.workspaces.TADataTab;
import course_site.workspaces.TemplateFile;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * This class manages all CSS style for this application.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class CourseSiteStyle extends AppStyleComponent {
    // FIRST WE SHOULD DECLARE ALL OF THE STYLE TYPES WE PLAN TO USE
    
    // WE'LL USE THIS FOR ORGANIZING LEFT AND RIGHT CONTROLS
    public static String CLASS_PLAIN_PANE = "plain_pane";
    
    // THESE ARE THE HEADERS FOR EACH SIDE
    public static String CLASS_HEADER_PANE = "header_pane";
    public static String CLASS_HEADER_LABEL = "header_label";

    // ON THE LEFT WE HAVE THE TA ENTRY
    public static String CLASS_TA_TABLE = "ta_table";
    public static String CLASS_TA_TABLE_COLUMN_HEADER = "ta_table_column_header";
    public static String CLASS_ADD_TA_PANE = "add_ta_pane";
    public static String CLASS_ADD_TA_TEXT_FIELD = "add_ta_text_field";
    public static String CLASS_ADD_TA_BUTTON = "add_ta_button";

    // ON THE RIGHT WE HAVE THE OFFICE HOURS GRID
    public static String CLASS_OFFICE_HOURS_GRID = "office_hours_grid";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE = "office_hours_grid_time_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL = "office_hours_grid_time_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE = "office_hours_grid_day_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL = "office_hours_grid_day_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE = "office_hours_grid_time_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL = "office_hours_grid_time_cell_label";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE = "office_hours_grid_ta_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL = "office_hours_grid_ta_cell_label";
    
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_COLUMN_HOVER = "office_hours_grid_ta_cell_pane_column_hover";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_ROW_HOVER = "office_hours_grid_ta_cell_pane_row_hover";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE_LEFT_HOVER = "office_hours_grid_time_cell_pane_left_hover";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE_HOVER = "office_hours_grid_day_column_header_pane_hover";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE_SELECTED_HOVER = "office_hours_grid_ta_cell_pane_selected_hover";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE_RIGHT_RESTORE = "office_hours_grid_time_cell_pane_right_restoration";
    
    public static String CLASS_TAB_BUTTON = "tab_button";
    public static String CLASS_TAB_BUTTON_SELECTED = "tab_button_selected";
    public static String CLASS_TABLE_BACKGROUND = "table_background";
    public static String CLASS_FIELD_LABELS = "field_labels";
    public static String CLASS_CHANGE_ADD_BUTTONS = "change_add_clear_button";
    public static String CLASS_TITLE_LABEL = "title_labels";
    public static String CLASS_TABLE_TITLE = "table_title_button";
    public static String CLASS_SUPPORTING_TEXT = "supporting_text";
    public static String CLASS_EDIT_TITLES = "edit_title";
    public static String CLASS_EDIT_FIELD_LABELS = "edit_field_labels";
    
    // THIS PROVIDES ACCESS TO OTHER COMPONENTS
    private AppTemplate app;
    
    /**
     * This constructor initializes all style for the application.
     * 
     * @param initApp The application to be stylized.
     */
    public CourseSiteStyle(AppTemplate initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // LET'S USE THE DEFAULT STYLESHEET SETUP
        super.initStylesheet(app);

        // INIT THE STYLE FOR THE FILE TOOLBAR
        app.getGUI().initFileToolbarStyle();

        // AND NOW OUR WORKSPACE STYLE
        initCourseSiteWorkspaceStyle();
    }

    /**
     * This function specifies all the style classes for
     * all user interface controls in the workspace.
     */
    private void initCourseSiteWorkspaceStyle() {
        CourseSiteWorkspace workspace = (CourseSiteWorkspace)app.getWorkspaceComponent();
        workspace.getCourseEditorButton().getStyleClass().add(CLASS_TAB_BUTTON_SELECTED);
        workspace.getTATabButton().getStyleClass().add(CLASS_TAB_BUTTON);
        workspace.getRecitationButton().getStyleClass().add(CLASS_TAB_BUTTON);
        workspace.getScheduleButton().getStyleClass().add(CLASS_TAB_BUTTON);
        workspace.getProjectButton().getStyleClass().add(CLASS_TAB_BUTTON);
        initTADataTab();
        initCourseEditorTab();
        initRecitationTab();
        initScheduleTab();
        initProjectTab();
    }
    
    public void changeTabStyleOnClick(Button button){
        CourseSiteWorkspace workspace = (CourseSiteWorkspace)app.getWorkspaceComponent();
        workspace.getCourseEditorButton().getStyleClass().clear();
        workspace.getCourseEditorButton().getStyleClass().addAll("button",CLASS_TAB_BUTTON);
        workspace.getTATabButton().getStyleClass().clear();
        workspace.getTATabButton().getStyleClass().addAll("button",CLASS_TAB_BUTTON);
        workspace.getRecitationButton().getStyleClass().clear();
        workspace.getRecitationButton().getStyleClass().addAll("button",CLASS_TAB_BUTTON);
        workspace.getScheduleButton().getStyleClass().clear();
        workspace.getScheduleButton().getStyleClass().addAll("button",CLASS_TAB_BUTTON);
        workspace.getProjectButton().getStyleClass().clear();
        workspace.getProjectButton().getStyleClass().addAll("button",CLASS_TAB_BUTTON);
        button.getStyleClass().clear();
        button.getStyleClass().addAll("button",CLASS_TAB_BUTTON_SELECTED);
    }
    
    private void initCourseEditorTab(){
        CourseEditorTab tab = ((CourseSiteWorkspace)app.getWorkspaceComponent()).getCourseEditorTab();
        tab.getCourseInfoTitle().getStyleClass().add(CLASS_TITLE_LABEL);
        tab.getPageStyleTitle().getStyleClass().add(CLASS_TITLE_LABEL);
        tab.getTemplateTitle().getStyleClass().add(CLASS_TITLE_LABEL);
        tab.getTableTitle().getStyleClass().add(CLASS_TITLE_LABEL);
        tab.getSupportingStyleText().getStyleClass().add(CLASS_SUPPORTING_TEXT);
        tab.getSupportingTableText().getStyleClass().add(CLASS_SUPPORTING_TEXT);
        tab.getTable().getStyleClass().add(CLASS_TABLE_BACKGROUND);
        TableView<TemplateFile> table = tab.getTable();
        for (TableColumn tableColumn : table.getColumns()) {
            tableColumn.getStyleClass().add(CLASS_TA_TABLE_COLUMN_HEADER);
        }
        for(Label l: tab.getFieldLabels()){
            l.getStyleClass().add(CLASS_FIELD_LABELS);
        }
        for(Button b: tab.getButtons()){
            b.getStyleClass().add(CLASS_CHANGE_ADD_BUTTONS);
        }
        tab.getExportContainer().getStyleClass().add("field_labels_for_specific_hbox");
        tab.getExportText().getStyleClass().add(CLASS_FIELD_LABELS);
        tab.getTemplateText().getStyleClass().add(CLASS_FIELD_LABELS);
    }
    
    private void initTADataTab(){
        // LEFT SIDE - THE HEADER
        TADataTab tab = ((CourseSiteWorkspace)app.getWorkspaceComponent()).getTATab();
        tab.getTAsHeaderBox().getStyleClass().add(CLASS_HEADER_PANE);
        tab.getTAsHeaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);

        // LEFT SIDE - THE TABLE
        TableView<TeachingAssistant> taTable = tab.getTATable();
        taTable.getStyleClass().add(CLASS_TA_TABLE);
        for (TableColumn tableColumn : taTable.getColumns()) {
            tableColumn.getStyleClass().add(CLASS_TA_TABLE_COLUMN_HEADER);
        }

        // LEFT SIDE - THE TA DATA ENTRY
        tab.getAddBox().getStyleClass().add(CLASS_ADD_TA_PANE);
        tab.getNameTextField().getStyleClass().add(CLASS_ADD_TA_TEXT_FIELD);
        tab.getAddButton().getStyleClass().add(CLASS_ADD_TA_BUTTON);
        tab.getUpdateButton().getStyleClass().add(CLASS_ADD_TA_BUTTON);
        tab.getClearButton().getStyleClass().add(CLASS_ADD_TA_BUTTON);

        // RIGHT SIDE - THE HEADER
        tab.getOfficeHoursSubheaderBox().getStyleClass().add(CLASS_HEADER_PANE);
        tab.getOfficeHoursSubheaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);
        tab.getTATable().getStyleClass().add(CLASS_TABLE_BACKGROUND);
    }
    
    private void initRecitationTab(){
        RecitationTab tab = ((CourseSiteWorkspace)app.getWorkspaceComponent()).getRecitationTab();
        tab.getTitle().getStyleClass().add(CLASS_TABLE_TITLE);
        tab.getEditTitle().getStyleClass().add(CLASS_EDIT_TITLES);
        for(Label l: tab.getFieldLabels()){
            l.getStyleClass().add(CLASS_EDIT_FIELD_LABELS);
        }
        tab.getAddButton().getStyleClass().addAll(CLASS_CHANGE_ADD_BUTTONS);
        tab.getClearButton().getStyleClass().addAll(CLASS_CHANGE_ADD_BUTTONS);
        TableView<Recitation> table = tab.getTable();
        table.getStyleClass().add(CLASS_TABLE_BACKGROUND);
        
        for(TableColumn c: table.getColumns())
        {
            c.getStyleClass().add(CLASS_TA_TABLE_COLUMN_HEADER);
        }
    }
       
    private void initScheduleTab(){
        ScheduleTab tab = ((CourseSiteWorkspace)app.getWorkspaceComponent()).getScheduleTab();
        tab.getTitle().getStyleClass().add(CLASS_TITLE_LABEL);
        tab.getBoundariesTitle().getStyleClass().add(CLASS_TABLE_TITLE);
        tab.getStartDateLabel().getStyleClass().add(CLASS_EDIT_FIELD_LABELS);
        tab.getEndDateLabel().getStyleClass().add(CLASS_EDIT_FIELD_LABELS);
        for(Label l: tab.getFieldLabels()){
            l.getStyleClass().add(CLASS_EDIT_FIELD_LABELS);
        }
        tab.getEditTitle().getStyleClass().add(CLASS_EDIT_TITLES);
        tab.getAddButton().getStyleClass().add(CLASS_CHANGE_ADD_BUTTONS);
        tab.getClearButton().getStyleClass().add(CLASS_CHANGE_ADD_BUTTONS);
        TableView<ScheduleEvent> table = tab.getTable();
        table.getStyleClass().add(CLASS_TABLE_BACKGROUND);
        for(TableColumn l: table.getColumns()){
            l.getStyleClass().add(CLASS_TA_TABLE_COLUMN_HEADER);
        }
        tab.getTableTitle().getStyleClass().add(CLASS_TABLE_TITLE);
    }
    
    private void initProjectTab(){
        ProjectTab tab = ((CourseSiteWorkspace)app.getWorkspaceComponent()).getProjectTab();
        tab.getTitle().getStyleClass().add(CLASS_TITLE_LABEL);
        tab.getTeamAddEditTitle().getStyleClass().add(CLASS_EDIT_TITLES);
        tab.getStudentAddEditTitle().getStyleClass().add(CLASS_EDIT_TITLES);
        for(Label l: tab.getFieldLabels()){
            l.getStyleClass().add(CLASS_EDIT_FIELD_LABELS);
        }
        tab.getTeamTableTitle().getStyleClass().add(CLASS_TABLE_TITLE);
        tab.getStudentTableTitle().getStyleClass().add(CLASS_TABLE_TITLE);
        TableView<Team> teamTable = tab.getTeamTable();
        TableView<Student> studentTable = tab.getStudentTable();
        teamTable.getStyleClass().add(CLASS_TABLE_BACKGROUND);
        studentTable.getStyleClass().add(CLASS_TABLE_BACKGROUND);
        for(TableColumn t: teamTable.getColumns()){
            t.getStyleClass().add(CLASS_TA_TABLE_COLUMN_HEADER);
        }
        for(TableColumn s:studentTable.getColumns()){
            s.getStyleClass().add(CLASS_TA_TABLE_COLUMN_HEADER);
        }
        tab.getTeamAddButton().getStyleClass().add(CLASS_CHANGE_ADD_BUTTONS);
        tab.getTeamClearButton().getStyleClass().add(CLASS_CHANGE_ADD_BUTTONS);
        tab.getStudentAddButton().getStyleClass().add(CLASS_CHANGE_ADD_BUTTONS);
        tab.getStudentClearButton().getStyleClass().add(CLASS_CHANGE_ADD_BUTTONS);
    }
    
    /**
     * This helper method initializes the style of all the nodes in the nodes
     * map to a common style, styleClass.
     */
    private void setStyleClassOnAll(HashMap nodes, String styleClass) {
        for (Object nodeObject : nodes.values()) {
            Node n = (Node)nodeObject;
            n.getStyleClass().add(styleClass);
        }
    }
    
    /**
     * This method initializes the style for all UI components in
     * the office hours grid. Note that this should be called every
     * time a new TA Office Hours Grid is created or loaded.
     */
    public void initOfficeHoursGridStyle() {
        // RIGHT SIDE - THE OFFICE HOURS GRID TIME HEADERS
        TADataTab workspaceComponent = ((CourseSiteWorkspace)app.getWorkspaceComponent()).getTATab();
        workspaceComponent.getOfficeHoursGridPane().getStyleClass().add(CLASS_OFFICE_HOURS_GRID);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeHeaderPanes(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeHeaderLabels(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridDayHeaderPanes(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridDayHeaderLabels(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeCellPanes(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeCellLabels(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTACellPanes(), CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTACellLabels(), CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL);
    }
    public void changeStyleOnHover(TADataTab workspace, int row, int column)
    {
        HashMap<String, Pane> dayHeaderPanes = workspace.getOfficeHoursGridDayHeaderPanes();
        HashMap<String, Pane> timeCellPanes = workspace.getOfficeHoursGridTimeCellPanes();
        if(row > 0 && column > 1)
        {
            for(int i = row-1; i > 0; i--)
            {
                workspace.getTACellPane(column + "_" + i).getStyleClass().clear();
                workspace.getTACellPane(column + "_" + i).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_COLUMN_HOVER);
            }
            for(int i = column - 1; i > 1;i--)
            {
                workspace.getTACellPane(i + "_" + row).getStyleClass().clear();
                workspace.getTACellPane(i + "_" + row).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_ROW_HOVER);
            }
            workspace.getTACellPane(column+"_"+row).getStyleClass().clear();
            workspace.getTACellPane(column+"_"+row).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE_SELECTED_HOVER);
            timeCellPanes.get("0_"+row).getStyleClass().clear();
            timeCellPanes.get("0_"+row).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE_LEFT_HOVER);
            timeCellPanes.get("1_"+row).getStyleClass().clear();
            timeCellPanes.get("1_"+row).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE_RIGHT_RESTORE);
            dayHeaderPanes.get(column+"_"+0).getStyleClass().clear();
            dayHeaderPanes.get(column+"_"+0).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE_HOVER);
            
        }
    }
    
    public void revertChanges(TADataTab workspace, int row, int column)
    {
        HashMap<String, Pane> dayHeaderPanes = workspace.getOfficeHoursGridDayHeaderPanes();
        HashMap<String, Pane> timeCellPanes = workspace.getOfficeHoursGridTimeCellPanes();
        if(row > 0 && column > 1)
        {
            for(int i = row-1; i > 0; i--)
            {
                workspace.getTACellPane(column + "_" + i).getStyleClass().clear();
                workspace.getTACellPane(column + "_" + i).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
            }
            for(int i = column - 1; i > 1;i--)
            {
                workspace.getTACellPane(i + "_" + row).getStyleClass().clear();
                workspace.getTACellPane(i + "_" + row).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
            }
            workspace.getTACellPane(column+"_"+row).getStyleClass().clear();
            workspace.getTACellPane(column+"_"+row).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
            timeCellPanes.get("0_"+row).getStyleClass().clear();
            timeCellPanes.get("0_"+row).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE);
            timeCellPanes.get("1_"+row).getStyleClass().clear();
            timeCellPanes.get("1_"+row).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE);
            dayHeaderPanes.get(column+"_"+0).getStyleClass().clear();
            dayHeaderPanes.get(column+"_"+0).getStyleClass().add(CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE);
        }
    }
    
}