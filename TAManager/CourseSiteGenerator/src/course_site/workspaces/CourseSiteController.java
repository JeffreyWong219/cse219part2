
package course_site.workspaces;

import course_site.CourseSiteManagerApp;
import course_site.CourseSiteManagerProperties;
import course_site.style.CourseSiteStyle;
import djf.ui.AppMessageDialogSingleton;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;

public class CourseSiteController {
    CourseSiteManagerApp app;
    CourseSiteWorkspace main;
    
    
    public CourseSiteController(CourseSiteWorkspace mainTab, CourseSiteManagerApp app) {
        // KEEP THIS FOR LATER
        main = mainTab;
        this.app = app;
    }
    
    
    public void handleStyleChange(Button button){
        ((CourseSiteStyle)app.getStyleComponent()).changeTabStyleOnClick(button);
    }
    
    public void handleTabChange(Pane pane){
        main.getCourseEditorTab().getTab().setVisible(false);
        main.getTATab().getTab().setVisible(false);
        main.getRecitationTab().getTab().setVisible(false);
        main.getScheduleTab().getTab().setVisible(false);
        main.getProjectTab().getTab().setVisible(false);
        pane.setVisible(true);
        pane.requestFocus();
        main.setCurrent(pane);
    }
    
    public void displayAboutMessage(){
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        dialog.show(props.getProperty(CourseSiteManagerProperties.ABOUT_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.ABOUT_MESSAGE.toString()));
    }
}
