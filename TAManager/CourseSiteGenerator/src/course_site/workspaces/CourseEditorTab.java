
package course_site.workspaces;

import course_site.CourseSiteManagerProperties;
import java.io.File;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Callback;
import jtps.jTPS;
import properties_manager.PropertiesManager;

public class CourseEditorTab {
    
    CourseSiteWorkspace app;
    CourseEditorController controller;
    
    VBox courseInfoContainer,styleContainer,templateContainer,leftSideFormatting;
    Label infoLabel,subjectLabel,subjectNumberLabel,semesterLabel,yearLabel,titleLabel,siteTableTitle,
            instructorNameLabel, instructorHomeLabel,exportDirectoryLabel,bannerLabel,leftFooterLabel,
            rightFooterLabel,stylesheetLabel,styleTitle,templateTitle;
    HBox subjectRow,semesterRow,bannerBox,leftBox,rightBox,exportDirectoryBox,exportDirectoryPath;
    ComboBox<String> subjectChoice,semesterChoice, yearChoice;
    ComboBox<Integer> subjectNumberChoice;
    ComboBox<File> stylesheetChoices;
    TextField titleTextField,instructorTextField,instructorHomeTextField;
    Button changeExportButton,templateChooser,changeBannerImage,changeLeftFooter,changeRightFooter;
    Text templateMessage, templatePathText, sheetNote;
    GridPane courseInfoFormat,subjectFormat,container,styleFormatting;
    
    ObservableList<String> subjectList,semesterList,yearList;
    ObservableList<Integer> subjectNumberList;
    ObservableList<TemplateFile> files;
    ObservableList<File> cssFiles;
    
    TableView<TemplateFile> siteTable;
    TableColumn<TemplateFile,String> titleColumn,fileColumn,jsColumn;
    TableColumn<TemplateFile,Boolean> useColumn;
    
    ImageView bannerImage,leftFooterImage,rightFooterImage;
    
    String bannerFile,leftFile,rightFile;
    File exportFile,templateFile,cssFile;
    Text exportPathText;
    
    boolean loading = false;
    
    public CourseEditorTab(CourseSiteWorkspace app)
    {
        //initialize lists and propertiesmanager
        this.app = app;
        controller = new CourseEditorController(this,app.getApp());
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        subjectList = FXCollections.observableList(props.getPropertyOptionsList(CourseSiteManagerProperties.COURSE_CHOICES));
        semesterList = FXCollections.observableList(props.getPropertyOptionsList(CourseSiteManagerProperties.SEMESTER));
        yearList = FXCollections.observableList(props.getPropertyOptionsList(CourseSiteManagerProperties.YEAR));
        ArrayList<String> numberStrings = props.getPropertyOptionsList(CourseSiteManagerProperties.COURSE_NUMBER);
        ArrayList<Integer> numberInts = new ArrayList<>();
        for(String s:numberStrings){
            numberInts.add(Integer.parseInt(s));
        }
        subjectNumberList = FXCollections.observableList(numberInts);
        files = FXCollections.observableList(new ArrayList<>());
        cssFiles = FXCollections.observableList(new ArrayList<>());
        
        //generate container and first subcontainer
        container = new GridPane();
        courseInfoContainer = new VBox();
        infoLabel = new Label(props.getProperty(CourseSiteManagerProperties.COURSE_INFO_TITLE.toString()));
        
        //create and format content of first sub cotainer
        subjectFormat = new GridPane();
        subjectChoice = new ComboBox(subjectList);
        subjectChoice.setOnAction(e->{
            app.getApp().getGUI().fileController.markAsEdited(app.getApp().getGUI());
        });
        subjectChoice.prefWidthProperty().bind(container.widthProperty().multiply(.1));
        subjectLabel = new Label(props.getProperty(CourseSiteManagerProperties.SUBJECT_LABEL.toString()));
        subjectNumberChoice = new ComboBox(subjectNumberList);
        subjectNumberChoice.setOnAction(e->{
            app.getApp().getGUI().fileController.markAsEdited(app.getApp().getGUI());
        });
        subjectNumberChoice.prefWidthProperty().bind(container.widthProperty().multiply(.1));
        subjectNumberLabel = new Label(props.getProperty(CourseSiteManagerProperties.SUBJECT_NUMBER_LABEL.toString()));
        
        subjectFormat.add(subjectLabel,0,0);
        subjectFormat.add(subjectChoice,1,0);
        subjectFormat.add(subjectNumberLabel,2,0);
        subjectFormat.add(subjectNumberChoice,3,0);
        
        semesterChoice = new ComboBox(semesterList);
        semesterChoice.setOnAction(e->{
            app.getApp().getGUI().fileController.markAsEdited(app.getApp().getGUI());
        });
        semesterChoice.prefWidthProperty().bind(container.widthProperty().multiply(.1));
        semesterLabel = new Label(props.getProperty(CourseSiteManagerProperties.SEMESTER_LABEL.toString()));
        yearChoice = new ComboBox(yearList);
        yearChoice.setOnAction(e->{
            app.getApp().getGUI().fileController.markAsEdited(app.getApp().getGUI());
        });
        yearChoice.prefWidthProperty().bind(container.widthProperty().multiply(.1));
        yearLabel = new Label(props.getProperty(CourseSiteManagerProperties.YEAR_LABEL.toString()));
        
        subjectFormat.add(semesterLabel,0,1);
        subjectFormat.add(semesterChoice,1,1);
        subjectFormat.add(yearLabel,2,1);
        subjectFormat.add(yearChoice,3,1);
        
        
        courseInfoFormat = new GridPane();
        titleTextField = new TextField();
        titleTextField.setOnKeyPressed(e->{
            app.getApp().getGUI().fileController.markAsEdited(app.getApp().getGUI());
        });
        titleTextField.setPromptText(props.getProperty(CourseSiteManagerProperties.TITLE_PROMPT_TEXT.toString()));
        titleTextField.prefWidthProperty().bind(container.widthProperty().multiply(.3));
        titleLabel = new Label(props.getProperty(CourseSiteManagerProperties.TITLE_PROMPT_LABEL.toString()));
        instructorTextField = new TextField();
        instructorTextField.setOnKeyPressed(e->{
            app.getApp().getGUI().fileController.markAsEdited(app.getApp().getGUI());
        });
        instructorTextField.setPromptText(props.getProperty(CourseSiteManagerProperties.INSTRUCTOR_PROMPT_TEXT.toString()));
        instructorNameLabel = new Label(props.getProperty(CourseSiteManagerProperties.INSTRUCTOR_PROMPT_LABEL.toString()));
        instructorHomeTextField = new TextField();
        instructorHomeTextField.setOnKeyPressed(e->{
            app.getApp().getGUI().fileController.markAsEdited(app.getApp().getGUI());
        });
        instructorHomeTextField.setPromptText(props.getProperty(CourseSiteManagerProperties.INSTRUCTOR_HOME_PROMPT_TEXT.toString()));
        instructorHomeLabel = new Label(props.getProperty(CourseSiteManagerProperties.INSTRUCTOR_HOME_LABEL.toString()));
        
        courseInfoFormat.add(titleLabel,0,0);
        courseInfoFormat.add(titleTextField,1,0);
        courseInfoFormat.add(instructorNameLabel,0,1);
        courseInfoFormat.add(instructorTextField,1,1);
        courseInfoFormat.add(instructorHomeLabel,0,2);
        courseInfoFormat.add(instructorHomeTextField,1,2);
        
        exportDirectoryBox = new HBox();
        exportDirectoryLabel = new Label(props.getProperty(CourseSiteManagerProperties.EXPORT_LABEL.toString()));
        changeExportButton = new Button(props.getProperty(CourseSiteManagerProperties.CHANGE_BUTTON.toString()));
        //exportDirectoryPath = new Label(exportPath,changeExportButton);
        exportPathText = new Text();
        exportDirectoryPath = new HBox();
        exportDirectoryPath.getChildren().addAll(exportPathText,changeExportButton);
        exportDirectoryBox.getChildren().addAll(exportDirectoryPath);
        
        courseInfoFormat.add(exportDirectoryLabel,0,3);
        courseInfoFormat.add(exportDirectoryPath,1,3);
        courseInfoContainer.getChildren().addAll(infoLabel,subjectFormat,courseInfoFormat);
        
        //create and format second subcontainer and contents
        templateContainer = new VBox();
        templateTitle = new Label(props.getProperty(CourseSiteManagerProperties.SITE_TEMPLATE_TITLE.toString()));
        templateMessage = new Text(props.getProperty(CourseSiteManagerProperties.SITE_TEMPLATE_MESSAGE.toString()));
        templatePathText = new Text();
        templateChooser = new Button(props.getProperty(CourseSiteManagerProperties.TEMPLATE_BUTTON.toString()));
        siteTableTitle = new Label(props.getProperty(CourseSiteManagerProperties.SITE_PAGES_TITLE.toString()));
        
        siteTable = new TableView();
        siteTable.setItems(files);
        templateContainer.setFillWidth(false);
        siteTable.prefWidthProperty().bind(container.widthProperty().multiply(.5));
        String useColumnHeader = props.getProperty(CourseSiteManagerProperties.SITE_PAGES_TITLE.toString());
        String navColumnHeader = props.getProperty(CourseSiteManagerProperties.SITE_PAGES_NAVBAR_COLUMN.toString());
        String fileColumnHeader = props.getProperty(CourseSiteManagerProperties.SITE_PAGES_FILE_COLUMN.toString());
        String scriptColumnHeader = props.getProperty(CourseSiteManagerProperties.SITE_PAGES_SCRIPT_COLUMN.toString());
        
        useColumn = new TableColumn(useColumnHeader);
        useColumn.setCellValueFactory((CellDataFeatures<TemplateFile,Boolean> param) -> {
            if(!loading){
                app.getApp().getGUI().fileController.markAsEdited(app.getApp().getGUI());
            }
            return param.getValue().getBooleanProperty();});
        useColumn.setCellFactory(CheckBoxTableCell.forTableColumn(useColumn));
        useColumn.prefWidthProperty().bind(siteTable.widthProperty().multiply(.25));
        
        titleColumn = new TableColumn(navColumnHeader);
        titleColumn.setCellValueFactory(new PropertyValueFactory<TemplateFile, String>("header"));
        titleColumn.prefWidthProperty().bind(siteTable.widthProperty().multiply(.25));
        fileColumn = new TableColumn(fileColumnHeader);
        fileColumn.setCellValueFactory(new PropertyValueFactory<TemplateFile,String>("siteFileName"));
        fileColumn.prefWidthProperty().bind(siteTable.widthProperty().multiply(.25));
        jsColumn = new TableColumn(scriptColumnHeader);
        jsColumn.setCellValueFactory(new PropertyValueFactory<TemplateFile,String>("jsFileName"));
        jsColumn.prefWidthProperty().bind(siteTable.widthProperty().multiply(.25));
        siteTable.getColumns().addAll(useColumn,titleColumn,fileColumn,jsColumn);
        siteTable.setEditable(true);
        templateContainer.getChildren().addAll(templateTitle,templateMessage,templatePathText,templateChooser,siteTableTitle,siteTable);
        
        
        //create and format third subcontainer and contents
        styleContainer = new VBox();
        styleFormatting = new GridPane();
        styleTitle = new Label(props.getProperty(CourseSiteManagerProperties.PAGE_STYLE_TITLE.toString()));
        bannerImage = new ImageView();
        bannerImage.setFitHeight(100);
        bannerImage.setFitWidth(100);
        bannerImage.setPreserveRatio(true);
        bannerLabel = new Label(props.getProperty(CourseSiteManagerProperties.BANNER_IMAGE_LABEL.toString()));
        changeBannerImage = new Button(props.getProperty(CourseSiteManagerProperties.CHANGE_BUTTON.toString()));
        
        leftFooterImage = new ImageView();
        leftFooterImage.setFitHeight(100);
        leftFooterImage.setFitWidth(100);
        leftFooterImage.setPreserveRatio(true);
        leftFooterLabel = new Label(props.getProperty(CourseSiteManagerProperties.LEFT_FOOTER_LABEL.toString()));
        changeLeftFooter = new Button(props.getProperty(CourseSiteManagerProperties.CHANGE_BUTTON.toString()));
        
        rightFooterImage = new ImageView();
        rightFooterImage.setFitHeight(100);
        rightFooterImage.setFitWidth(100);
        rightFooterImage.setPreserveRatio(true);
        rightFooterLabel = new Label(props.getProperty(CourseSiteManagerProperties.RIGHT_FOOTER_LABEL.toString()));
        changeRightFooter = new Button(props.getProperty(CourseSiteManagerProperties.CHANGE_BUTTON.toString()));
        
        styleFormatting.add(bannerLabel,0,0);
        styleFormatting.add(bannerImage,1,0);
        styleFormatting.add(changeBannerImage,2,0);
        styleFormatting.add(leftFooterLabel,0,1);
        styleFormatting.add(leftFooterImage,1,1);
        styleFormatting.add(changeLeftFooter,2,1);
        styleFormatting.add(rightFooterLabel,0,2);
        styleFormatting.add(rightFooterImage,1,2);
        styleFormatting.add(changeRightFooter,2,2);
        
        
        scanForCSS();
        stylesheetChoices = new ComboBox<>();
        stylesheetChoices.setItems(cssFiles);
        stylesheetChoices.prefWidthProperty().bind(container.widthProperty().multiply(.1));
        stylesheetChoices.setCellFactory(new Callback<ListView<File>,ListCell<File>>(){
            @Override
            public ListCell<File> call(ListView<File> param)
            {
                return new ListCell<File>(){
                    @Override
                    protected void updateItem(File item, boolean empty)
                    {
                        super.updateItem(item,empty);
                        if(item == null || empty)
                        {
                            setText(null);
                        }
                        else
                            setText(item.getName());
                    }
                };
            }
        });
        
        stylesheetChoices.setButtonCell(new ListCell<File>(){
            @Override
            protected void updateItem(File item, boolean empty){
                super.updateItem(item, empty);
                if(item == null || empty){
                    setText(null);
                }
                else{
                    setText(item.getName());
                    if(!loading)
                    app.getApp().getGUI().fileController.markAsEdited(app.getApp().getGUI());
                }
            }
        });
        stylesheetChoices.setOnAction(e->{
                app.getApp().getGUI().fileController.markAsEdited(app.getApp().getGUI());
        });
        
        stylesheetLabel = new Label(props.getProperty(CourseSiteManagerProperties.STYLESHEET_LABEL.toString()),stylesheetChoices);
        stylesheetLabel.setContentDisplay(ContentDisplay.RIGHT);
        sheetNote = new Text(props.getProperty(CourseSiteManagerProperties.STYLESHEET_NOTE.toString()));
        styleContainer.getChildren().addAll(styleTitle,styleFormatting,stylesheetLabel,sheetNote);
        leftSideFormatting = new VBox();
        leftSideFormatting.getChildren().addAll(courseInfoContainer,styleContainer);
        container.add(leftSideFormatting,0,0);
        container.add(templateContainer,1,0);
        ColumnConstraints c1 = new ColumnConstraints();
        c1.setPercentWidth(50);
        container.getColumnConstraints().add(c1);
        changeExportButton.setOnAction(e->{
            controller.handleExportDirectoryChange();
        });
        
        changeBannerImage.setOnAction(e->{
           controller.handleImageChange("banner", bannerFile, bannerImage);
        });
        
        changeLeftFooter.setOnAction(e->{
            controller.handleImageChange("left", leftFile, leftFooterImage);
        });
        
        changeRightFooter.setOnAction(e->{
            controller.handleImageChange("right", rightFile, rightFooterImage);
        });
        
        templateChooser.setOnAction(e->{
             controller.handleTemplateDirectoryChange();
        });
        
        container.setOnKeyPressed(e->{
            if(e.isControlDown() && e.getCode() != null)
            {
                if(e.getCode() == KeyCode.Z)
                    controller.handleUndoAction();
                if(e.getCode() == KeyCode.Y)
                    controller.handleRedoAction();
            }      
        });
        setLoad(false);
    }
    
    public GridPane getTab(){
        return container;
    }
    
    public void clearSelectionFields(){
        subjectChoice.getSelectionModel().clearSelection();
        subjectNumberChoice.getSelectionModel().clearSelection();
        semesterChoice.getSelectionModel().clearSelection();
        yearChoice.getSelectionModel().clearSelection();
        titleTextField.setText("");
        instructorTextField.setText("");
        instructorHomeTextField.setText("");
        exportPathText.setText("");
        templatePathText.setText("");
        stylesheetChoices.getSelectionModel().clearSelection();
        bannerImage.setImage(null);
        leftFooterImage.setImage(null);
        rightFooterImage.setImage(null);
    }
    
    public void resetTab(){
        clearSelectionFields();
        exportFile = null;
        templateFile = null;
        cssFile = null;
        files.clear();
        controller.clearTransactions();
        app.getApp().getGUI().updateExportButton(true);
    }
    
    public Label getCourseInfoTitle(){
        return infoLabel;
    }
    
    public Label getPageStyleTitle(){
        return styleTitle;
    }
    
    public Label getTemplateTitle(){
        return templateTitle;
    }
    
    public Label getTableTitle(){
        return siteTableTitle;
    }

    public Text getSupportingStyleText(){
        return sheetNote;
    }
    
    public Text getSupportingTableText(){
        return templateMessage;
    }
    
    public TableView getTable(){
        return siteTable;
    }
    
    public ArrayList<Label> getFieldLabels(){
        ArrayList<Label> labels = new ArrayList<>();
        labels.add(subjectLabel);
        labels.add(subjectNumberLabel);
        labels.add(bannerLabel);
        labels.add(exportDirectoryLabel);
        labels.add(instructorNameLabel);
        labels.add(instructorHomeLabel);
        labels.add(leftFooterLabel);
        labels.add(subjectNumberLabel);
        labels.add(rightFooterLabel);
        labels.add(semesterLabel);
        labels.add(stylesheetLabel);
        labels.add(titleLabel);
        labels.add(yearLabel);
        return labels;
    }
    
    public ArrayList<Button> getButtons(){
        ArrayList<Button> buttons = new ArrayList<>();
        buttons.add(changeBannerImage);
        buttons.add(changeExportButton);
        buttons.add(changeLeftFooter);
        buttons.add(changeRightFooter);
        buttons.add(templateChooser);
        return buttons;
    }
    
    public HBox getExportContainer(){
        return exportDirectoryPath;
    }
    
    public Text getExportText(){
        return exportPathText;
    }
    
    public Text getTemplateText(){
        return templatePathText;
    }
    
    public TextField getTitleField(){
        return titleTextField;
    }
    
    public TextField getInstructorNameField(){
        return instructorTextField;
    }
    
    public TextField getInstructorHomeTextField(){
        return instructorHomeTextField;
    }
    
    public ComboBox<String> getSubjectChoices(){
        return subjectChoice;
    }
    
    public ComboBox<Integer> getSubjectNumber(){
        return subjectNumberChoice;
    }
    
    public ComboBox<String> getSemesterChoice(){
        return semesterChoice;
    }
    
    public ComboBox<String> getYearChoice(){
        return yearChoice;
    }
    
    public File getTemplateFilePath(){
        return templateFile;
    }
    
    public String getBannerImagePath(){
        if(bannerFile == null)
            return "";
        return bannerFile;
    }
    
    public String getLeftImagePath(){
        if(leftFile == null)
            return "";
        return leftFile;
    }
    
    public String getRightImagePath(){
        if(rightFile == null){
            return "";
        }
        return rightFile;
    }
    
    public ComboBox<File> getStylesheets(){
        return stylesheetChoices;
    }
    
    public File getExportFile(){
        return exportFile;
    }
    
    public ObservableList<TemplateFile> getTemplateFiles(){
        return files;
    }

    public ObservableList<String> getSubjectList(){
        return subjectList;
    }
    
    public ObservableList<String> getSemesterList(){
        return semesterList;
    }
    
    public ObservableList<String> getYearList(){
        return yearList;
    }
    
    public ObservableList<Integer> getSubjectNumberList(){
        return subjectNumberList;
    }
    
    public ObservableList<File> getCSSFiles(){
        return cssFiles;
    }
   
    public jTPS getTransactions(){
        return controller.getTransactions();
    }
    
    public void setExportFile(File file){
        if(file != null && file.exists()){
            exportFile = file;
            exportPathText.setText("..\\"+exportFile.getParentFile().getName()+"\\"+exportFile.getName());
            app.getApp().getGUI().updateExportButton(false);
        }
        else{
            app.getApp().getGUI().updateExportButton(true);
            exportPathText.setText("");
        }
    }
    
    public void setTemplateFile(File file, boolean loading){
        if(file != null && file.exists()){
            templateFile = file;
            templatePathText.setText("..\\"+templateFile.getParentFile().getName()+ "\\" + templateFile.getName());
            if(!loading){
                files.clear();
                siteTable.refresh();
                scanForFiles(file);
            }
        }
        else{
            templatePathText.setText("");
            files.clear();
        }
    }
    
    public void setBannerImagePath(String path){
        bannerFile = path;
    }
    
    public void setLeftImagePath(String path){
        leftFile = path;
    }
    
    public void setRightImagePath(String path){
        rightFile = path;
    }
    
    public void addTemplateFile(boolean isUsed, File js, File site, String navTitle){
        TemplateFile file = new TemplateFile(isUsed,js,site,navTitle,app.getApp());
        files.add(file);
    }
    
    public void reloadWorkspace(){
        bannerImage.setImage(new Image("file:"+bannerFile));
        leftFooterImage.setImage(new Image("file:"+leftFile));
        rightFooterImage.setImage(new Image("file:"+rightFile));
        if(exportFile != null && exportFile.exists()){
            exportPathText.setText("..\\"+exportFile.getParentFile().getName()+"\\"+exportFile.getName());
            
        }
        if(templateFile != null && templateFile.exists())
            templatePathText.setText("..\\"+templateFile.getParentFile().getName()+ "\\" + templateFile.getName());
    }
        
    public boolean verifyExists(File file){
        return file.exists();
    }
    
    public void scanForFiles(File directory){

        File[] fileContainer = directory.listFiles();
        String jsPathBase = System.getProperty("user.dir") + "\\work\\js\\";
        for(File f: fileContainer){
            if(f.isDirectory()){
                scanForFiles(f);
            }
            else{
                switch(f.getName()){
                    case "index.html":{
                        addTemplateFile(true,new File(jsPathBase+"HomeBuilder.js"),f,"Home");
                        break;
                    }
                    case "syllabus.html":{
                        addTemplateFile(true,new File(jsPathBase+"SyllabusBuilder.js"),f,"Syllabus");
                        break;
                    }
                    case "schedule.html":{
                        addTemplateFile(true,new File(jsPathBase+"ScheduleBuilder.js"),f,"Schedule");
                        break;
                    }
                    case "hws.html":{
                        addTemplateFile(true,new File(jsPathBase+"HWsBuilder.js"),f,"HWs");
                        break;
                    }
                    case "projects.html":{
                        addTemplateFile(true,new File(jsPathBase+"ProjectsBuilder.js"),f,"Projects");
                        break;
                    }
                    default:break;
                }       
            }
        }
    }
    
    public void scanForCSS(){
        String cssPathBase = System.getProperty("user.dir") + "\\work\\css";
        File cssFolder = new File(cssPathBase);
        File[] cssFileFolder = cssFolder.listFiles();
        for(File f:cssFileFolder){
            cssFiles.add(f);
        }
    }
    
    public void setLoad(boolean setting){
        loading = setting;
    }
}
