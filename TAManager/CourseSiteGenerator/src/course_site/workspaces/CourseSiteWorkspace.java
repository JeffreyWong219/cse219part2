/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package course_site.workspaces;

import course_site.CourseSiteManagerApp;
import course_site.data.CourseSiteData;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import properties_manager.PropertiesManager;
import course_site.CourseSiteManagerProperties;
import djf.settings.AppPropertyType;
import javafx.scene.layout.Pane;
import jtps.jTPS;


public class CourseSiteWorkspace extends AppWorkspaceComponent {
    
    CourseSiteManagerApp app;
    CourseSiteController controller;
    CourseSiteData data;
    BorderPane container;
    StackPane windows;
    CourseEditorTab courseDetailTab;
    TADataTab taData;
    RecitationTab recitationTab;
    ScheduleTab scheduleTab;
    ProjectTab projectTab;
    HBox tabsControl;
    Button courseTabButton, taDataTabButton,recitationTabButton, scheduleTabButton, projectTabButton;
    Pane current;
    
    Button undoButton,redoButton,aboutButton;
    jTPS chosenTransaction;
    
    public CourseSiteWorkspace(CourseSiteManagerApp app)
    {
        
        this.app = app;
        this.data = (CourseSiteData)app.getDataComponent();
        controller = new CourseSiteController(this,app);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        courseDetailTab = new CourseEditorTab(this);
        taData = new TADataTab(app,this);
        recitationTab = new RecitationTab(this);
        scheduleTab = new ScheduleTab(this);
        projectTab = new ProjectTab(this);
        
        Pane toolbar = this.app.getGUI().getToolbar();
        undoButton = app.getGUI().initChildButton(toolbar,AppPropertyType.UNDO_ICON.toString(),AppPropertyType.UNDO_TOOLTIP.toString(),true);
        redoButton = app.getGUI().initChildButton(toolbar,AppPropertyType.REDO_ICON.toString(),AppPropertyType.REDO_TOOLTIP.toString(),true);
        aboutButton = app.getGUI().initChildButton(toolbar,AppPropertyType.ABOUT_ICON.toString(),AppPropertyType.ABOUT_TOOLTIP.toString(),false);
        
        workspace = new BorderPane();
        tabsControl = new HBox();
        
        String courseEditorString = props.getProperty(CourseSiteManagerProperties.COURSE_DETAILS_TAB_LABEL.toString());
        String taTabString = props.getProperty(CourseSiteManagerProperties.TA_DATA_TAB_LABEL.toString());
        String recitationTabString = props.getProperty(CourseSiteManagerProperties.RECITATION_TAB_LABEL.toString());
        String scheduleTabString = props.getProperty(CourseSiteManagerProperties.SCHEDULE_TAB_LABEL.toString());
        String projectTabString = props.getProperty(CourseSiteManagerProperties.PROJECT_TAB_LABEL.toString());        
        courseTabButton = new Button(courseEditorString);
        taDataTabButton = new Button(taTabString);
        recitationTabButton = new Button(recitationTabString);
        scheduleTabButton = new Button(scheduleTabString);
        projectTabButton = new Button(projectTabString);
         
        
        courseTabButton.prefWidthProperty().bind(workspace.widthProperty().multiply(.2));
        taDataTabButton.prefWidthProperty().bind(workspace.widthProperty().multiply(.2));
        recitationTabButton.prefWidthProperty().bind(workspace.widthProperty().multiply(.2));
        scheduleTabButton.prefWidthProperty().bind(workspace.widthProperty().multiply(.2));
        projectTabButton.prefWidthProperty().bind(workspace.widthProperty().multiply(.2));
        
        tabsControl.getChildren().addAll(courseTabButton,taDataTabButton,recitationTabButton,scheduleTabButton,projectTabButton);
        ((BorderPane)workspace).setTop(tabsControl);
        
        windows = new StackPane();
        windows.getChildren().addAll(projectTab.getTab(),scheduleTab.getTab(),recitationTab.getTab(),taData.getTab(),courseDetailTab.getTab());
        ((BorderPane)workspace).setCenter(windows);
        taData.getTab().setVisible(false);
        recitationTab.getTab().setVisible(false);
        scheduleTab.getTab().setVisible(false);
        projectTab.getTab().setVisible(false);
        
        courseTabButton.setOnAction(e->{
           controller.handleStyleChange(courseTabButton);
           controller.handleTabChange(courseDetailTab.getTab());
           undoButton.setDisable(courseDetailTab.getTransactions().nothingToUndo());
           redoButton.setDisable(courseDetailTab.getTransactions().nothingToRedo());
           chosenTransaction = courseDetailTab.getTransactions();
           recitationTab.setTransition(true);
        });
        
        taDataTabButton.setOnAction(e->{
           controller.handleStyleChange(taDataTabButton);
           controller.handleTabChange(taData.getTab());
           undoButton.setDisable(taData.getTransactions().nothingToUndo());
           redoButton.setDisable(taData.getTransactions().nothingToRedo());
           chosenTransaction = taData.getTransactions();
           recitationTab.setTransition(true);
        });
        
        recitationTabButton.setOnAction(e->{
           controller.handleStyleChange(recitationTabButton);
           controller.handleTabChange(recitationTab.getTab());
           undoButton.setDisable(recitationTab.getTransactions().nothingToUndo());
           redoButton.setDisable(recitationTab.getTransactions().nothingToRedo());
           chosenTransaction = recitationTab.getTransactions();
           recitationTab.setTransition(false);
        });
        
        scheduleTabButton.setOnAction(e->{
           controller.handleStyleChange(scheduleTabButton);
           controller.handleTabChange(scheduleTab.getTab());
           recitationTab.setTransition(true);
           undoButton.setDisable(scheduleTab.getTransactions().nothingToUndo());
           redoButton.setDisable(scheduleTab.getTransactions().nothingToRedo());
           chosenTransaction = scheduleTab.getTransactions();
        });
        
        projectTabButton.setOnAction(e->{
           controller.handleStyleChange(projectTabButton);
           controller.handleTabChange(projectTab.getTab());
           recitationTab.setTransition(true);
           undoButton.setDisable(projectTab.getTransactions().nothingToUndo());
           redoButton.setDisable(projectTab.getTransactions().nothingToRedo());
           chosenTransaction = projectTab.getTransactions();
        });
        
        undoButton.setOnAction(e->{
            if(current == taData.getTab()){
                taData.setButtonPress(true);
                taData.resetAdditionFields();
            }
            if(current == scheduleTab.getTab()){
                scheduleTab.setUndoState(true);
            }
            chosenTransaction.undoTransaction();
            undoButton.setDisable(chosenTransaction.nothingToUndo());
            redoButton.setDisable(chosenTransaction.nothingToRedo());
            current.requestFocus();
        });
        
        redoButton.setOnAction(e->{
            if(current == taData.getTab()){
                taData.setButtonPress(true);
                taData.resetAdditionFields();
            }
            if(current == scheduleTab.getTab()){
                scheduleTab.setUndoState(true);
            }
            chosenTransaction.doTransaction();
            undoButton.setDisable(chosenTransaction.nothingToUndo());
            redoButton.setDisable(chosenTransaction.nothingToRedo());
            current.requestFocus();
        });
        current = courseDetailTab.getTab();
        chosenTransaction = courseDetailTab.getTransactions();
        
        aboutButton.setOnAction(e->{
            controller.displayAboutMessage();
        });
    }
    
    public CourseSiteManagerApp getApp(){
        return app;
    }
    
    @Override
    public void resetWorkspace()
    {
        courseDetailTab.resetTab();
        taData.resetWorkspace();
        taData.clearTransactionHistory();
        recitationTab.resetTab();
        scheduleTab.resetTab();
        projectTab.resetTab();
        redoButton.setDisable(true);
        undoButton.setDisable(true);
        controller.handleStyleChange(courseTabButton);
        controller.handleTabChange(courseDetailTab.getTab());
    }
    
    @Override
    public void reloadWorkspace(AppDataComponent data)
    {
        taData.reloadWorkspace();
        scheduleTab.reloadWorkspace();
    }
    
    public void initializeFields(){
        courseDetailTab.reloadWorkspace();
        scheduleTab.reloadWorkspace();
    }
    
    public TADataTab getTATab(){
        return taData;
    }
    
    public CourseEditorTab getCourseEditorTab(){
        return courseDetailTab;
    }
    
    public RecitationTab getRecitationTab(){
        return recitationTab;
    }
    
    public ScheduleTab getScheduleTab(){
        return scheduleTab;
    }
    
    public ProjectTab getProjectTab(){
        return projectTab;
    }
    
    public Button getCourseEditorButton()
    {
        return courseTabButton;
    }
    
    public Button getTATabButton()
    {
        return taDataTabButton;
    }
    
    public Button getRecitationButton()
    {
        return recitationTabButton;
    }
    
    public Button getScheduleButton(){
        return scheduleTabButton;
    }
    
    public Button getProjectButton(){
        return projectTabButton;
    }
    
    public Button getRedoButton(){
        return redoButton;
    }
    
    public Button getUndoButton(){
        return undoButton;
    }
    
    public void setCurrent(Pane pane){
        current = pane;
    }
    
    
}
