
package course_site.workspaces;

import course_site.CourseSiteManagerApp;
import course_site.CourseSiteManagerProperties;
import course_site.transactions.ChangeExportDirectory_Transaction;
import course_site.transactions.ChangeImage_Transaction;
import course_site.transactions.ChangeTemplateDirectory_Transaction;
import java.io.File;
import javafx.scene.image.ImageView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;

public class CourseEditorController {
    private CourseEditorTab tab;
    private CourseSiteManagerApp app;
    jTPS transactions;
    
    public CourseEditorController(CourseEditorTab tab,CourseSiteManagerApp app){
        this.tab = tab;
        this.app = app;
        transactions = new jTPS();
    }
    
    public void handleExportDirectoryChange(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        DirectoryChooser dc = new DirectoryChooser();
        dc.setTitle(props.getProperty(CourseSiteManagerProperties.CHANGE_EXPORT_TITLE));
        File selectedDirectory = dc.showDialog(app.getGUI().getWindow());
        if(selectedDirectory != null)
        {
            if(tab.getExportFile() == null){
                transactions.addTransaction(new ChangeExportDirectory_Transaction(null,selectedDirectory.getAbsolutePath(),tab));
                app.getGUI().fileController.markAsEdited(app.getGUI());
                return;
            }
          performTransaction(new ChangeExportDirectory_Transaction(tab.getExportFile().getAbsolutePath(),selectedDirectory.getAbsolutePath(),tab));
        }
    }
    
    public void handleTemplateDirectoryChange(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        DirectoryChooser dc = new DirectoryChooser();
        dc.setTitle(props.getProperty(CourseSiteManagerProperties.CHANGE_TEMPLATE_TITLE));
        File selectedDirectory = dc.showDialog(app.getGUI().getWindow());
        if(selectedDirectory != null)
        {
           if(tab.getTemplateFilePath() != null)
               performTransaction(new ChangeTemplateDirectory_Transaction(tab.getTemplateFilePath().getAbsolutePath(),selectedDirectory.getAbsolutePath(),tab));
           else
               performTransaction(new ChangeTemplateDirectory_Transaction(null,selectedDirectory.getAbsolutePath(),tab));
        }
    }
    
    public void handleImageChange(String which,String path,ImageView view){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        FileChooser fc = new FileChooser();
        fc.setTitle(props.getProperty(CourseSiteManagerProperties.CHANGE_IMAGE_TITLE));
        fc.getExtensionFilters().addAll(new ExtensionFilter("JPG", "*.jpg"),
		new ExtensionFilter("jPEG", "*.jpeg"),new ExtensionFilter("PNG", "*.png"),new ExtensionFilter("GIF", "*.gif"));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        if(selectedFile != null)
        {
            performTransaction(new ChangeImage_Transaction(path,selectedFile.getAbsolutePath(),which,view,tab));
        }
    }
    
    public jTPS getTransactions(){
        return transactions;
    }
    
    public void clearTransactions(){
        transactions.clearTransactions();
    }
    
    public void performTransaction(jTPS_Transaction transaction){
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getUndoButton().setDisable(false);
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getRedoButton().setDisable(true);
        transactions.addTransaction(transaction);
        app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void handleUndoAction()
    {
        transactions.undoTransaction();
        updateButtons();
        if(app.getGUI().fileController.isSaved() && !transactions.nothingToUndo())
            app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void handleRedoAction(){
        transactions.doTransaction();
        updateButtons();
        if(app.getGUI().fileController.isSaved() && !transactions.nothingToRedo())
            app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void updateButtons(){
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getRedoButton().setDisable(transactions.nothingToRedo());
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getUndoButton().setDisable(transactions.nothingToUndo());
    }
}
