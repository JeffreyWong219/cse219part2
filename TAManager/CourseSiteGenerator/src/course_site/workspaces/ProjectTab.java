package course_site.workspaces;

import course_site.CourseSiteManagerProperties;
import course_site.data.CourseSiteData;
import course_site.data.ProjectData;
import course_site.data.Student;
import course_site.data.Team;
import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.util.Callback;
import jtps.jTPS;
import properties_manager.PropertiesManager;

public class ProjectTab {
    
    CourseSiteWorkspace main;
    ProjectData data;
    ProjectController controller;
    boolean updateTeam = false;
    boolean updateStudent = false;
    
    VBox container,studentContent,teamContent,studentTableContent,studentEditContent,teamEditContent,teamTableContent,teamEditSubcontainer;
    HBox colorPickerContainer,textColorPickerContainer;
    GridPane teamFormatting,studentFormatting,teamEditFormatting,studentEditFormatting,teamColorInternal,textColorInternal;
    Label teamTitle,teamName,addEditLabelTeam,addEditLabelStudent,linkLabel,studentLabel,studentFirstLabel,studentLastLabel,studentTeamLabel,studentRoleLabel,
            colorLabel,textColorLabel,title;
    Button teamDeleteButton,addButton,clearButton,studentDeleteButton,studentAddButton,studentClearButton;
    TextField teamNameBox,linkTextField,studentFirstText,studentLastText,studentRoleText;
    Circle teamColorCircle, textColorCircle;
    StackPane teamColorCircleHolder,teamTextColorCircleHolder;
    Text teamColorText,teamTextColorText;
    ColorPicker teamColorPicker,teamTextColorPicker;
    ComboBox studentTeamChoice;
    
    TableView<Team> projectsTable;
    TableView<Student> studentsTable;
    TableColumn<Team,String> teamNameColumn,teamColorColumn,teamTextColorColumn,teamLinkColumn;
    TableColumn<Student,String> studentFirstColumn,studentLastColumn,studentTeamColumn,studentRoleColumn;
    
    public ProjectTab(CourseSiteWorkspace app){
        main = app;
        data = ((CourseSiteData)(main.getApp().getDataComponent())).getProjectData();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        controller = new ProjectController(this,app.getApp(),data);
        
        container = new VBox();
        title = new Label(props.getProperty(CourseSiteManagerProperties.PROJECT_TITLE.toString()));
        teamContent = new VBox();
        teamFormatting = new GridPane();
        teamDeleteButton = new Button("-");
        teamTitle = new Label(props.getProperty(CourseSiteManagerProperties.TEAM_TITLE.toString()),teamDeleteButton);
        teamTitle.setContentDisplay(ContentDisplay.RIGHT);
        teamTableContent = new VBox();
        teamTableContent.getChildren().add(teamTitle);
        
        //create table and format it
        projectsTable = new TableView(data.getTeams());
        projectsTable.prefWidthProperty().bind(container.widthProperty().multiply(.5));
        projectsTable.prefHeightProperty().bind(container.heightProperty().multiply(.33));
        String teamNameText = props.getProperty(CourseSiteManagerProperties.NAME_COLUMN_TEXT.toString());
        String teamColor = props.getProperty(CourseSiteManagerProperties.COLOR_COLUMN.toString());
        String teamTextColor = props.getProperty(CourseSiteManagerProperties.TEXT_COLOR_COLUMN.toString());
        String teamLink = props.getProperty(CourseSiteManagerProperties.LINK_COLUMN.toString());
        teamNameColumn = new TableColumn(teamNameText);
        teamNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        teamNameColumn.prefWidthProperty().bind(projectsTable.widthProperty().multiply(.25));
        teamColorColumn = new TableColumn(teamColor);
        teamColorColumn.setCellValueFactory(new PropertyValueFactory<>("color"));
        teamColorColumn.prefWidthProperty().bind(projectsTable.widthProperty().multiply(.25));
        teamTextColorColumn = new TableColumn(teamTextColor);
        teamTextColorColumn.setCellValueFactory(new PropertyValueFactory<>("textColor"));
        teamTextColorColumn.prefWidthProperty().bind(projectsTable.widthProperty().multiply(.25));
        teamLinkColumn = new TableColumn(teamLink);
        teamLinkColumn.setCellValueFactory(new PropertyValueFactory<>("link"));
        teamLinkColumn.prefWidthProperty().bind(projectsTable.widthProperty().multiply(.25));
        projectsTable.getColumns().addAll(teamNameColumn,teamColorColumn,teamTextColorColumn,teamLinkColumn);
        teamTableContent.getChildren().add(projectsTable);
        teamFormatting.add(teamTableContent,0,0);
        
        //create title for the section and formatting components

        addEditLabelTeam = new Label(props.getProperty(CourseSiteManagerProperties.ADD_EDIT_TITLE.toString()));
        teamEditFormatting = new GridPane();
        teamName = new Label(props.getProperty(CourseSiteManagerProperties.TEAM_LABEL.toString())+":");
        teamNameBox = new TextField();       
        colorLabel = new Label(props.getProperty(CourseSiteManagerProperties.COLOR_LABEL.toString()));
        
        //create team color part
        teamColorCircleHolder = new StackPane();
        teamColorInternal = new GridPane();
        teamColorCircle = new Circle(50,Color.WHITE);
        teamColorText = new Text(Color.WHITE.toString());
        colorPickerContainer = new HBox();
        teamColorPicker = new ColorPicker();
        teamColorPicker.setValue(Color.WHITE);
        teamColorInternal.add(teamColorPicker,0,0);
        colorPickerContainer.getChildren().addAll(teamColorInternal);
        
        //create text color part
        textColorLabel = new Label(props.getProperty(CourseSiteManagerProperties.TEXT_COLOR_LABEL.toString()));
        teamTextColorCircleHolder = new StackPane();
        textColorInternal = new GridPane();
        textColorCircle = new Circle(50,Color.WHITE);
        teamTextColorText = new Text(Color.WHITE.toString());
        teamTextColorCircleHolder.getChildren().addAll(textColorCircle,teamTextColorText);
        teamTextColorPicker = new ColorPicker();
        teamTextColorPicker.setValue(Color.BLACK);
        textColorInternal.add(textColorLabel,0,0);
        textColorInternal.add(teamTextColorPicker,1,0);
        colorPickerContainer.getChildren().add(textColorInternal);
        
        //create link label and field along with buttons
        linkLabel = new Label(props.getProperty(CourseSiteManagerProperties.LINK_COLUMN.toString())+":");
        linkTextField = new TextField();
        addButton = new Button(props.getProperty(CourseSiteManagerProperties.ADD_UPDATE_BUTTON.toString()));
        clearButton = new Button(props.getProperty(CourseSiteManagerProperties.CLEAR_BUTTON_TEXT.toString()));
        
        //format the edit fields
        teamEditSubcontainer = new VBox();
        teamEditSubcontainer.getChildren().add(addEditLabelTeam);
        teamEditFormatting.add(teamName,0,0);
        teamEditFormatting.add(teamNameBox,1,0);
        teamEditFormatting.setFillWidth(teamNameBox, false);
        teamEditFormatting.add(colorLabel,0,1);
        teamEditFormatting.add(colorPickerContainer,1,1);
        teamEditFormatting.add(linkLabel,0,2);
        teamEditFormatting.add(linkTextField,1,2);  
        teamEditFormatting.add(addButton,0,3);
        teamEditFormatting.add(clearButton,1,3);
        teamEditFormatting.setHgrow(addButton,Priority.ALWAYS);
        teamEditSubcontainer.getChildren().add(teamEditFormatting);
        teamFormatting.add(teamEditSubcontainer,1,0);
        ColumnConstraints t1 = new ColumnConstraints();
        //t1.setPercentWidth(50);
        ColumnConstraints t2 = new ColumnConstraints();
        //t2.setPercentWidth(50);
        textColorLabel.setAlignment(Pos.CENTER);
        teamFormatting.getColumnConstraints().addAll(t1,t2);
        teamNameBox.prefWidthProperty().bind(teamFormatting.widthProperty().multiply(.20));
        teamContent.getChildren().addAll(teamFormatting);
        //end team ui additons
        
        studentContent = new VBox();
        studentFormatting = new GridPane();
        
        //build header for table
        studentTableContent = new VBox();
        studentDeleteButton = new Button("-");
        studentLabel = new Label(props.getProperty(CourseSiteManagerProperties.STUDENT_TITLE.toString()),studentDeleteButton);
        studentLabel.setContentDisplay(ContentDisplay.RIGHT);
        studentTableContent.getChildren().add(studentLabel);
        
        //build table and its columns
        studentsTable = new TableView(data.getStudents());
        studentsTable.prefWidthProperty().bind(container.widthProperty().multiply(.5));
        studentsTable.prefHeightProperty().bind(container.heightProperty().multiply(.33));
        String firstColumn = props.getProperty(CourseSiteManagerProperties.FIRST_NAME_LABEL.toString());
        String lastColumn = props.getProperty(CourseSiteManagerProperties.LAST_NAME_LABEL.toString());
        String teamColumn = props.getProperty(CourseSiteManagerProperties.TEAM_LABEL.toString());
        String roleColumn = props.getProperty(CourseSiteManagerProperties.ROLE_LABEL.toString());
        studentFirstColumn = new TableColumn(firstColumn);
        studentFirstColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        studentFirstColumn.prefWidthProperty().bind(studentsTable.widthProperty().multiply(.25));
        studentLastColumn = new TableColumn(lastColumn);
        studentLastColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        studentLastColumn.prefWidthProperty().bind(studentsTable.widthProperty().multiply(.25));
        studentTeamColumn = new TableColumn(teamColumn);
        studentTeamColumn.setCellValueFactory(new PropertyValueFactory<>("teamName"));
        studentTeamColumn.prefWidthProperty().bind(studentsTable.widthProperty().multiply(.25));
        studentRoleColumn = new TableColumn(roleColumn);
        studentRoleColumn.setCellValueFactory(new PropertyValueFactory<>("role"));
        studentRoleColumn.prefWidthProperty().bind(studentsTable.widthProperty().multiply(.25));
        studentsTable.getColumns().addAll(studentFirstColumn,studentLastColumn,studentTeamColumn,studentRoleColumn);
        studentTableContent.getChildren().add(studentsTable);
        studentFormatting.add(studentTableContent, 0, 0);
        
        studentEditContent = new VBox();
        studentEditFormatting = new GridPane();
        addEditLabelStudent = new Label(props.getProperty(CourseSiteManagerProperties.ADD_EDIT_TITLE.toString()));
        studentEditContent.getChildren().add(addEditLabelStudent);
        
        studentFirstLabel = new Label(props.getProperty(CourseSiteManagerProperties.FIRST_NAME_LABEL.toString())+":");
        studentFirstText = new TextField();
        studentLastLabel = new Label(props.getProperty(CourseSiteManagerProperties.LAST_NAME_LABEL.toString())+":");
        studentLastText = new TextField();
        studentTeamLabel = new Label(props.getProperty(CourseSiteManagerProperties.TEAM_LABEL.toString())+":");
        studentTeamChoice = new ComboBox(data.getTeams());
        studentTeamChoice.setCellFactory(new Callback<ListView<Team>,ListCell<Team>>(){
            @Override
            public ListCell<Team> call(ListView<Team> param)
            {
                return new ListCell<Team>(){
                    @Override
                    protected void updateItem(Team item, boolean empty)
                    {
                        super.updateItem(item,empty);
                        if(item == null || empty)
                        {
                            setText(null);
                        }
                        else
                            setText(item.getName());
                    }
                };
            }
        });
        
        studentRoleLabel = new Label(props.getProperty(CourseSiteManagerProperties.ROLE_LABEL.toString())+":");
        studentRoleText = new TextField();
        studentAddButton = new Button(props.getProperty(CourseSiteManagerProperties.ADD_UPDATE_BUTTON.toString()));
        studentClearButton = new Button(props.getProperty(CourseSiteManagerProperties.CLEAR_BUTTON_TEXT.toString()));
        
        studentEditFormatting.add(studentFirstLabel,0,0);
        studentEditFormatting.add(studentFirstText,1,0);
        studentEditFormatting.add(studentLastLabel,0,1);
        studentEditFormatting.add(studentLastText,1,1);
        studentEditFormatting.add(studentTeamLabel,0,2);
        studentEditFormatting.add(studentTeamChoice,1,2);
        studentEditFormatting.add(studentRoleLabel,0,3);
        studentEditFormatting.add(studentRoleText,1,3);
        studentEditFormatting.add(studentAddButton,0,4);
        studentEditFormatting.add(studentClearButton,1,4);
        studentEditContent.getChildren().add(studentEditFormatting);
        studentFormatting.add(studentEditContent,1,0);
        ColumnConstraints c1 = new ColumnConstraints();
        c1.setPercentWidth(50);
        ColumnConstraints c2 = new ColumnConstraints();
        c1.setPercentWidth(50);
        studentFormatting.getColumnConstraints().addAll(c1,c2);
        studentContent.getChildren().add(studentFormatting);
        studentFirstText.prefWidthProperty().bind(teamFormatting.widthProperty().multiply(.20));
        studentLastText.prefWidthProperty().bind(teamFormatting.widthProperty().multiply(.20));
        studentRoleText.prefWidthProperty().bind(teamFormatting.widthProperty().multiply(.20));
        studentTeamChoice.prefWidthProperty().bind(teamFormatting.widthProperty().multiply(.10));
        container.getChildren().addAll(title,teamContent,studentContent);
        addButton.setTranslateX(75);
        addButton.setTranslateY(10);
        clearButton.setTranslateX(85);
        clearButton.setTranslateY(10);
        studentAddButton.setTranslateX(75);
        studentAddButton.setTranslateY(10);
        studentClearButton.setTranslateX(75);
        studentClearButton.setTranslateY(10);
        
        addButton.setOnAction(e->{
            if(!updateTeam)
                controller.handleAddTeam();
            else
                controller.handleUpdateTeam();
        });
        studentAddButton.setOnAction(e->{
            if(!updateStudent)
                controller.handleAddStudent();
            else
                controller.handleUpdateStudent();
           
        });
        
        studentDeleteButton.setOnAction(e->{
            if(studentsTable.getSelectionModel().getSelectedItem() != null){
                controller.handleDeleteStudent();
                clearStudentSelectionFields();
            }
        });
        studentsTable.setOnKeyPressed(e->{
            if(e.getCode() != null && e.getCode() == KeyCode.BACK_SPACE && !studentsTable.getItems().isEmpty() && studentsTable.getSelectionModel().getSelectedItem()!=null){
                controller.handleDeleteStudent();
                clearStudentSelectionFields();
            }
        });
        
        projectsTable.setOnMouseClicked(e->{
            if(projectsTable.getSelectionModel().getSelectedItem() != null){
                controller.handleTeamTableSelection();
            }
        });
        
        teamDeleteButton.setOnAction(e->{
            if(projectsTable.getSelectionModel().getSelectedItem() != null){
                controller.handleDeleteTeam();
                clearTeamSelectionFields();
                studentsTable.refresh();
            }
        });
        
        projectsTable.setOnKeyPressed(e->{
            if(e.getCode() != null && e.getCode() == KeyCode.BACK_SPACE && !projectsTable.getItems().isEmpty() && projectsTable.getSelectionModel().getSelectedItem()!=null){
                controller.handleDeleteTeam();
                clearTeamSelectionFields();
                studentsTable.refresh();
            }
        });
        
        studentsTable.setOnMouseClicked(e->{
            if(studentsTable.getSelectionModel().getSelectedItem() != null){
                controller.handleStudentTableSelection();
            }
        });
        
        clearButton.setOnAction(e->{
           clearTeamSelectionFields(); 
        });
        
        studentClearButton.setOnAction(e->{
            clearStudentSelectionFields();
        });
        
        container.setOnKeyPressed(e->{
            if(e.isControlDown() && e.getCode() != null)
            {
                if(e.getCode() == KeyCode.Z)
                    controller.handleUndoAction();
                if(e.getCode() == KeyCode.Y)
                    controller.handleRedoAction();
            }      
        });
    }
    
    public jTPS getTransactions(){
        return controller.getTransactions();
    }
    
    public ProjectController getController(){
        return controller;
    }
    
    public boolean getTeamUpdateStatus(){
        return updateTeam;
    }
    
    public void setUpdateTeamStatus(boolean set){
        updateTeam = set;
    }
    
    public boolean getStudentUpdateStatus(){
        return updateStudent;
    }
    
    public void setUpdateStudentStatus(boolean set){
        updateStudent = set;
    }
    
    public VBox getTab(){
        return container;
    }
    
    public Label getTitle(){
        return title;
    }
    
    public Label getTeamAddEditTitle(){
        return addEditLabelTeam;
    }
    
    public Label getStudentAddEditTitle(){
        return addEditLabelStudent;
    }
    
    public ArrayList<Label> getFieldLabels(){
        ArrayList<Label> labels = new ArrayList<>();
        labels.add(colorLabel);
        labels.add(linkLabel);
        labels.add(studentFirstLabel);
        labels.add(studentLastLabel);
        labels.add(studentRoleLabel);
        labels.add(studentTeamLabel);
        labels.add(textColorLabel);
        labels.add(teamName);
        return labels;
    }
    
    public Label getTeamTableTitle(){
        return teamTitle;
    }
    
    public Label getStudentTableTitle(){
        return studentLabel;
    }
    
    public TableView<Team> getTeamTable(){
        return projectsTable;
    }
    
    public TableView<Student> getStudentTable(){
        return studentsTable;
    }
    
    public Button getTeamAddButton(){
        return addButton;
    }
    
    public Button getTeamClearButton(){
        return clearButton;
    }
    
    public Button getStudentAddButton(){
        return studentAddButton;
    }
    
    public Button getStudentClearButton(){
        return studentClearButton;
    }
    
    public TextField getTeamNameChoice(){
        return teamNameBox;
    }
    
    public ColorPicker getTeamColor(){
        return teamColorPicker;
    }
    
    public ColorPicker getTeamTextColor(){
        return teamTextColorPicker;
    }
    
    public TextField getTeamLinkChoice(){
        return linkTextField;
    }
    
    public TextField getStudentFirstNameChoice(){
        return studentFirstText;
    }
    
    public TextField getStudentLastNameChoice(){
        return studentLastText;
    }
    
    public ComboBox<Team> getTeamSelection(){
        return studentTeamChoice;
    }
    
    public TextField getStudentRoleChoice(){
        return studentRoleText;
    }
    
    public void clearStudentSelectionFields(){
        studentFirstText.setText("");
        studentLastText.setText("");
        studentRoleText.setText("");
        studentsTable.getSelectionModel().clearSelection();
        studentTeamChoice.getSelectionModel().clearSelection();
        updateStudent = false;
    }
    
    public void clearTeamSelectionFields(){
        teamNameBox.setText("");
        linkTextField.setText("");
        teamColorPicker.setValue(Color.WHITE);
        teamTextColorPicker.setValue(Color.BLACK);
        projectsTable.getSelectionModel().clearSelection();
        updateTeam = false;
    }
    
    
    public void clearSelectionFields(){
        clearStudentSelectionFields();
        clearTeamSelectionFields();
    }
    
    public void resetTab(){
        clearSelectionFields();
        controller.clearTransactions();
    }
}
