
package course_site.workspaces;

import course_site.CourseSiteManagerApp;
import course_site.CourseSiteManagerProperties;
import course_site.data.ScheduleData;
import course_site.data.ScheduleEvent;
import course_site.transactions.AddOrDeleteSchedule_Transaction;
import course_site.transactions.UpdateScheduleStartEnd_Transaction;
import course_site.transactions.UpdateSchedule_Transaction;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.time.LocalDate;
import java.util.Collections;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;

public class ScheduleController {
    ScheduleTab tab;
    CourseSiteManagerApp app;
    ScheduleData data;
    jTPS transactions;
    
    public ScheduleController(ScheduleTab tab, CourseSiteManagerApp workspace, ScheduleData data)
    {
        this.tab = tab;
        app = workspace;
        this.data = data;
        
        transactions = new jTPS();
    }
    
    public void handleAddScheduleSelection(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        LocalDate date = tab.getChosenEventDate().getValue();
        if(date != null){
            if(verifyDateWithinBounds(date)){
                tab.getAddButton().setDisable(true);
            }
            else{
                tab.getAddButton().setDisable(false);
            }
        }
        else{
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_EVENT_DATE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_EVENT_DATE_MESSAGE.toString()));
            return;
        }   
        String type = tab.getEventChoice().getSelectionModel().getSelectedItem();
        String time = tab.getTimeTextField().getText();
        String title = tab.getTitleTextField().getText();
        String topic = tab.getTopicTextField().getText();
        String link = tab.getLinkTextField().getText();
        String criteria = tab.getCriteriaTextField().getText();
        if(type == null)
        {
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_EVENT_TYPE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_EVENT_TYPE_MESSAGE.toString()));
            return;
        }
        performTransaction(new AddOrDeleteSchedule_Transaction(type,time,date,title,topic,link,criteria,false,data));
    }
    
    public void handleDeleteScheduleSelection(){
        AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        dialog.show(props.getProperty(CourseSiteManagerProperties.DELETE_SCHEDULE_EVENT_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.DELETE_SCHEDULE_EVENT_MESSAGE.toString()));
        if(dialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES)||dialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES_SPANISH)){
            ScheduleEvent e = tab.getTable().getSelectionModel().getSelectedItem();
            performTransaction(new AddOrDeleteSchedule_Transaction(e.getType(),e.getTime(),LocalDate.parse(e.getDate()),e.getTitle(),e.getTopic(),e.getLink(),e.getCriteria(),true,data));
        }
    }
    
    public void handleDisableFields(){
        String chosenItem = tab.getEventChoice().getSelectionModel().getSelectedItem();
        tab.restoreFields();
        if(chosenItem != null){
            switch(chosenItem){
                case "Holiday":
                case "Fiesta":{
                    tab.getTimeTextField().setDisable(true);
                    tab.getTimeTextField().setText("");
                    tab.getTopicTextField().setDisable(true);
                    tab.getTopicTextField().setText("");
                    tab.getCriteriaTextField().setText("");
                    tab.getCriteriaTextField().setDisable(true);
                    break;
                }
                
                case "Lecture":
                case "Clase":
                case "Reference":
                case "Referencia":{
                    tab.getTimeTextField().setDisable(true);
                    tab.getTimeTextField().setText("");
                    tab.getCriteriaTextField().setText("");
                    tab.getCriteriaTextField().setDisable(true);
                    break;
                }
                
                case "Recitation":
                case "Recitación":{
                    tab.getTimeTextField().setDisable(true);
                    tab.getTimeTextField().setText("");
                    tab.getCriteriaTextField().setText("");
                    tab.getCriteriaTextField().setDisable(true);
                    tab.getLinkTextField().setDisable(true);
                    tab.getLinkTextField().setText("");
                    break;
                }  
            }
        }
    }
    
    public void handleUpdateEvent(){
        LocalDate newDate = tab.getChosenEventDate().getValue();
        String newType = tab.getEventChoice().getSelectionModel().getSelectedItem();
        String newTime = tab.getTimeTextField().getText();
        String newTitle = tab.getTitleTextField().getText();
        String newTopic = tab.getTopicTextField().getText();
        String newLink = tab.getLinkTextField().getText();
        String newCriteria = tab.getCriteriaTextField().getText();
        ScheduleEvent e = tab.getTable().getSelectionModel().getSelectedItem();
        
        performTransaction(new UpdateSchedule_Transaction(e.getType(),e.getTime(), LocalDate.parse(e.getDate()),e.getTitle(),e.getTopic(),e.getLink(),e.getCriteria(),
                                        newType,newTime,newDate,newTitle,newTopic,newLink,newCriteria,data));
        tab.getTable().refresh();
        Collections.sort(data.getSchedules());
        handleTableSelection();
    }
    
    public void handleChangeStartDate(){
        AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        LocalDate l = tab.getChosenStartDate().getValue();
        
        if(l != null){
            if(l.compareTo(tab.getChosenEndDate().getValue())>0||l.getDayOfWeek().getValue() != 1){
                messageDialog.show(props.getProperty(CourseSiteManagerProperties.INVALID_START_DATE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.INVALID_START_DATE_MESSAGE.toString()));
                tab.setUndoState(true);
                data.setStartDate(data.getStartDate());
                tab.setUndoState(false);
                return;
            }
            if(data.checkIfAffectsStart(l)){
                dialog.show(props.getProperty(CourseSiteManagerProperties.VERIFY_START_DATE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.VERIFY_START_DATE_MESSAGE.toString()));
                if(dialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES)||dialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES_SPANISH))
                {
                    performTransaction(new UpdateScheduleStartEnd_Transaction(data.getStartDate().toString(),l.toString(),data.getAffectedStartHours(l),data,false));
                }
                else{
                    tab.setUndoState(true);
                    data.setStartDate(data.getStartDate());
                    tab.setUndoState(false);
                }
            }
            else{
                performTransaction(new UpdateScheduleStartEnd_Transaction(data.getStartDate().toString(),l.toString(),data.getAffectedStartHours(l),data,false));
            }
        }
        else{
            messageDialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_START_DATE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_START_DATE_MESSAGE.toString()));
            tab.setUndoState(true);
            data.setStartDate(data.getStartDate());
            tab.setUndoState(false);
        }
    }
    
    public void handleChangeEndDate(){
        AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        LocalDate l = tab.getChosenEndDate().getValue();
        
        if(l != null){
            if(l.compareTo(tab.getChosenStartDate().getValue())<0 ||l.getDayOfWeek().getValue() != 5){
                messageDialog.show(props.getProperty(CourseSiteManagerProperties.INVALID_END_DATE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.INVALID_END_DATE_MESSAGE.toString()));
                tab.setUndoState(true);
                data.setEndDate(data.getEndDate());
                tab.setUndoState(false);
                return;
            }
            if(data.checkIfAffectsEnd(l)){
                dialog.show(props.getProperty(CourseSiteManagerProperties.VERIFY_END_DATE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.VERIFY_END_DATE_MESSAGE.toString()));
                if(dialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES)||dialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES_SPANISH))
                {
                    performTransaction(new UpdateScheduleStartEnd_Transaction(data.getEndDate().toString(),l.toString(),data.getAffectedEndHours(l),data,true));
                }
                else{
                    tab.setUndoState(true);
                    data.setEndDate(data.getEndDate());
                    tab.setUndoState(false);
                }
            }
            else{
                performTransaction(new UpdateScheduleStartEnd_Transaction(data.getEndDate().toString(),l.toString(),data.getAffectedEndHours(l),data,true));
            }
        }
        else{
            messageDialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_END_DATE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_END_DATE_MESSAGE.toString()));
            tab.setUndoState(true);
            data.setEndDate(data.getEndDate());
            tab.setUndoState(false);
        }
    }
    
    public void handleTableSelection(){
        ScheduleEvent e = tab.getTable().getSelectionModel().getSelectedItem();
        tab.getEventChoice().getSelectionModel().select(e.getType());
        LocalDate date = LocalDate.parse(e.getDate());
        tab.getChosenEventDate().setValue(date);
        tab.getTimeTextField().setText(e.getTime());
        tab.getTitleTextField().setText(e.getTitle());
        tab.getTopicTextField().setText(e.getTopic());
        tab.getLinkTextField().setText(e.getLink());
        tab.getCriteriaTextField().setText(e.getCriteria());
        tab.setUpdateState(true);
    }
    
    public void handleEventDateSelection(){
        LocalDate date = tab.getChosenEventDate().getValue();
        if(date != null){
            if(verifyDateWithinBounds(date)){
                tab.getAddButton().setDisable(true);
            }
            else{
                tab.getAddButton().setDisable(false);
            }
        }
    }
    
    public boolean verifyDateWithinBounds(LocalDate date){
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        boolean failed = false;
        if(tab.getChosenStartDate().getValue() != null){
                if(date.compareTo(tab.getChosenStartDate().getValue()) < 0){
                    failed = true;
                }
            }
        if(tab.getChosenEndDate().getValue() != null)
        {
            if(date.compareTo(tab.getChosenEndDate().getValue()) > 0){
                failed = true;
            }
        }
        if(date.getDayOfWeek().getValue() == 6 || date.getDayOfWeek().getValue() == 7){
            failed = true;
        }
        if(failed){
            dialog.show(props.getProperty(CourseSiteManagerProperties.INVALID_EVENT_DATE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.INVALID_EVENT_DATE_MESSAGE.toString()));
        }
        return failed;
    }
    
    
    public jTPS getTransactions(){
        return transactions;
    }
    
    public void clearTransactions(){
        transactions.clearTransactions();
    }
    
    public void performTransaction(jTPS_Transaction transaction){
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getUndoButton().setDisable(false);
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getRedoButton().setDisable(true);
        transactions.addTransaction(transaction);
        app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void handleUndoAction()
    {
        tab.setUndoState(true);
        transactions.undoTransaction();
        updateButtons();
        if(app.getGUI().fileController.isSaved() && !transactions.nothingToUndo())
            app.getGUI().fileController.markAsEdited(app.getGUI());
        tab.getTable().refresh();
        tab.setUndoState(false);
    }
    
    public void handleRedoAction(){
        tab.setUndoState(true);
        transactions.doTransaction();
        updateButtons();
        if(app.getGUI().fileController.isSaved() && !transactions.nothingToRedo())
            app.getGUI().fileController.markAsEdited(app.getGUI());
        tab.getTable().refresh();
        tab.setUndoState(false);
    }
    
    public void updateButtons(){
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getRedoButton().setDisable(transactions.nothingToRedo());
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getUndoButton().setDisable(transactions.nothingToUndo());
    }
}
