/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package course_site.workspaces;

import course_site.CourseSiteManagerProperties;
import course_site.data.CourseSiteData;
import course_site.data.ScheduleData;
import course_site.data.ScheduleEvent;
import java.util.ArrayList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import jtps.jTPS;
import properties_manager.PropertiesManager;

public class ScheduleTab {
   CourseSiteWorkspace app;
   ScheduleData data;
   ScheduleController controller;
   VBox container,startEndDateBox,tableContainer,editContainer;
   HBox dateChooserContainer;
   GridPane itemsContainer,addEditFormatting;
   Label title,startEndDateTitle,editTitle;
   Label startDateLabel,endDateLabel,itemsTitle,typeChooserLabel,dateChooserLabel,timeLabel,titleLabel,topicLabel,linkLabel,criteriaLabel;
   Button deleteButton,addButton,clearButton;
   TextField timeField,titleField,topicField,linkField,criteriaField;
   DatePicker startDateChooser,endDateChooser,dateChooser;
   ComboBox<String> typeChooser;
   
   boolean update = false;
   boolean undo = false;
   
   TableView<ScheduleEvent> scheduleTable;
   TableColumn<ScheduleEvent,String> scheduleTypeColumn,scheduleDateColumn,scheduleTitleColumn,scheduleTopicColumn;
   
   public ScheduleTab(CourseSiteWorkspace app)
   {
       this.app = app;
       data = ((CourseSiteData)(app.getApp().getDataComponent())).getScheduleData();
       PropertiesManager props = PropertiesManager.getPropertiesManager();
       controller = new ScheduleController(this,app.getApp(),data);
       
       container = new VBox();
       
       startEndDateBox = new VBox();
       startEndDateTitle = new Label(props.getProperty(CourseSiteManagerProperties.START_END_EDITOR_TITLE.toString()));
       dateChooserContainer = new HBox();
       startDateChooser = new DatePicker();
       startDateChooser.setValue(data.getStartDate());
       endDateChooser = new DatePicker();
       endDateChooser.setValue(data.getEndDate());
       startDateLabel = new Label(props.getProperty(CourseSiteManagerProperties.START_DATE.toString()),startDateChooser);
       startDateLabel.setContentDisplay(ContentDisplay.RIGHT);
       endDateLabel = new Label(props.getProperty(CourseSiteManagerProperties.END_DATE.toString()),endDateChooser);
       endDateLabel.setContentDisplay(ContentDisplay.RIGHT);
       dateChooserContainer.getChildren().addAll(startDateLabel,startDateChooser,endDateLabel,endDateChooser);
       startEndDateBox.getChildren().addAll(startEndDateTitle,dateChooserContainer);
       
       itemsContainer = new GridPane();
       tableContainer = new VBox();
       deleteButton = new Button("-");
       itemsTitle = new Label(props.getProperty(CourseSiteManagerProperties.ITEM_TITLE.toString()),deleteButton);
       itemsTitle.setContentDisplay(ContentDisplay.RIGHT);
       
       scheduleTable = new TableView(data.getSchedules());
       scheduleTable.prefWidthProperty().bind(container.widthProperty().multiply(.50));
       String typeString = props.getProperty(CourseSiteManagerProperties.TYPE_COLUMN.toString());
       String dateColumn = props.getProperty(CourseSiteManagerProperties.DATE_COLUMN.toString());
       String titleColumn = props.getProperty(CourseSiteManagerProperties.TITLE_COLUMN.toString());
       String topicColumn = props.getProperty(CourseSiteManagerProperties.TOPIC_COLUMN.toString());
       scheduleTypeColumn = new TableColumn(typeString);
       scheduleTypeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
       scheduleTypeColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(.25));
       scheduleDateColumn = new TableColumn(dateColumn);
       scheduleDateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
       scheduleDateColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(.25));
       scheduleTitleColumn = new TableColumn(titleColumn);
       scheduleTitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
       scheduleTitleColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(.25));
       scheduleTopicColumn = new TableColumn(topicColumn);
       scheduleTopicColumn.setCellValueFactory(new PropertyValueFactory<>("topic"));
       scheduleTopicColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(.25));
       scheduleTable.getColumns().addAll(scheduleTypeColumn,scheduleDateColumn,scheduleTitleColumn,scheduleTopicColumn);
       tableContainer.getChildren().addAll(itemsTitle,scheduleTable);
       itemsContainer.add(tableContainer,0,0);
       
       addEditFormatting = new GridPane();
       editContainer = new VBox();
       editTitle = new Label(props.getProperty(CourseSiteManagerProperties.ADD_EDIT_TITLE.toString()));
       typeChooserLabel = new Label(props.getProperty(CourseSiteManagerProperties.TYPE_COLUMN.toString())+":");
       typeChooser = new ComboBox<>(data.getEventChoices());
       dateChooserLabel = new Label(props.getProperty(CourseSiteManagerProperties.DATE_COLUMN.toString())+":");
       dateChooser = new DatePicker(null);
       timeLabel = new Label(props.getProperty(CourseSiteManagerProperties.TIME_LABEL.toString()));
       timeField = new TextField();
       titleLabel = new Label(props.getProperty(CourseSiteManagerProperties.TITLE_COLUMN.toString())+":");
       titleField = new TextField();
       topicLabel = new Label(props.getProperty(CourseSiteManagerProperties.TOPIC_COLUMN.toString())+":");
       topicField = new TextField();
       linkLabel = new Label(props.getProperty(CourseSiteManagerProperties.LINK_LABEL.toString()));
       linkField = new TextField();
       criteriaLabel = new Label(props.getProperty(CourseSiteManagerProperties.CRITERIA_LABEL.toString()));
       criteriaField = new TextField();
       addButton = new Button(props.getProperty(CourseSiteManagerProperties.ADD_UPDATE_BUTTON.toString()));
       clearButton = new Button(props.getProperty(CourseSiteManagerProperties.CLEAR_BUTTON_TEXT.toString()));
       
       addEditFormatting.add(typeChooserLabel,0,0);
       addEditFormatting.add(typeChooser,1,0);
       addEditFormatting.add(dateChooserLabel,0,1);
       addEditFormatting.add(dateChooser,1,1);
       addEditFormatting.add(timeLabel,0,2);
       addEditFormatting.add(timeField,1,2);
       addEditFormatting.add(titleLabel,0,3);
       addEditFormatting.add(titleField,1,3);
       addEditFormatting.add(topicLabel,0,4);
       addEditFormatting.add(topicField,1,4);
       addEditFormatting.add(linkLabel,0,5);
       addEditFormatting.add(linkField,1,5);
       addEditFormatting.add(criteriaLabel,0,6);
       addEditFormatting.add(criteriaField,1,6);
       addEditFormatting.add(addButton,0,7);
       addEditFormatting.add(clearButton,1,7);
       editContainer.getChildren().addAll(editTitle,addEditFormatting);
       itemsContainer.add(editContainer,1,0);
       ColumnConstraints c1 = new ColumnConstraints();
       c1.setPercentWidth(50);
       itemsContainer.getColumnConstraints().add(c1);
       typeChooser.prefWidthProperty().bind(itemsContainer.widthProperty().multiply(.1));
       dateChooser.prefWidthProperty().bind(itemsContainer.widthProperty().multiply(.1));
       timeField.prefWidthProperty().bind(itemsContainer.widthProperty().multiply(.2));
       titleField.prefWidthProperty().bind(itemsContainer.widthProperty().multiply(.2));
       topicField.prefWidthProperty().bind(itemsContainer.widthProperty().multiply(.2));
       linkField.prefWidthProperty().bind(itemsContainer.widthProperty().multiply(.2));
       criteriaField.prefWidthProperty().bind(itemsContainer.widthProperty().multiply(.2));
       
       title = new Label(props.getProperty(CourseSiteManagerProperties.SCHEDULE_TITLE.toString()));
       container.getChildren().addAll(title,startEndDateBox,itemsContainer);
       addButton.setTranslateX(75);
       addButton.setTranslateY(10);
       clearButton.setTranslateX(75);
       clearButton.setTranslateY(10);
       
       typeChooser.setOnAction(e->{
           controller.handleDisableFields();
       });
       addButton.setOnAction(e->{
           if(!update)
               controller.handleAddScheduleSelection();
           else
               controller.handleUpdateEvent();
       });
       deleteButton.setOnAction(e->{
           if(scheduleTable.getSelectionModel().getSelectedItem()!=null){
               controller.handleDeleteScheduleSelection();
           }
       });
       scheduleTable.setOnKeyReleased(e -> { 
            if(e.getCode() != null && e.getCode() == KeyCode.BACK_SPACE && !scheduleTable.getItems().isEmpty() && scheduleTable.getSelectionModel().getSelectedItem()!=null){
                controller.handleDeleteScheduleSelection();
            }
        });
       container.setOnKeyPressed(e->{
            if(e.isControlDown() && e.getCode() != null)
            {
                if(e.getCode() == KeyCode.Z)
                    controller.handleUndoAction();
                if(e.getCode() == KeyCode.Y)
                    controller.handleRedoAction();
            }      
        });
       clearButton.setOnAction(e->{
           clearSelectionFields();
       });
       scheduleTable.setOnMouseClicked(e->{
           if(scheduleTable.getSelectionModel().getSelectedItem() != null){
               controller.handleTableSelection();
           }
       });
       dateChooser.setOnAction(e->{
           controller.handleEventDateSelection();
       });
       startDateChooser.setOnAction(e->{
           if(!undo)
            controller.handleChangeStartDate();
       });
       endDateChooser.setOnAction(e->{
           if(!undo)
            controller.handleChangeEndDate();
       });
   }
   
   public jTPS getTransactions(){
       return controller.getTransactions();
   }
   
   public boolean getUpdateState(){
       return update;
   }
   
   public void setUpdateState(boolean set){
       update = set;
   }
   
   public boolean getUndoState(){
       return undo;
   }
   
   public void setUndoState(boolean set){
       undo = set;
   }
   
   public VBox getTab(){
       return container;
   }
   
   public Label getTitle(){
       return title;
   }
   
   public Label getBoundariesTitle(){
       return startEndDateTitle;
   }
   
   public Label getStartDateLabel(){
       return startDateLabel;
   }
   
   public Label getEndDateLabel(){
       return endDateLabel;
   }
   
   public TableView<ScheduleEvent> getTable(){
       return scheduleTable;
   }
   
   public Label getTableTitle(){
       return itemsTitle;
   }
   
   public ComboBox<String> getEventChoice(){
       return typeChooser;
   }
   
   public TextField getTitleTextField(){
       return titleField;
   }
   
   public TextField getTimeTextField(){
       return timeField;
   }
   
   public TextField getTopicTextField(){
       return topicField;
   }
   
   public TextField getLinkTextField(){
       return linkField;
   }
   
   public TextField getCriteriaTextField(){
       return criteriaField;
   }
   
   public DatePicker getChosenEventDate(){
       return dateChooser;
   }
   
   public DatePicker getChosenStartDate(){
       return startDateChooser;
   }
   
   public DatePicker getChosenEndDate(){
       return endDateChooser;
   }
   
   public ArrayList<Label> getFieldLabels(){
       ArrayList<Label> labels = new ArrayList<>();
       labels.add(criteriaLabel);
       labels.add(dateChooserLabel);
       labels.add(linkLabel);
       labels.add(timeLabel);
       labels.add(titleLabel);
       labels.add(topicLabel);
       labels.add(typeChooserLabel);
       
       return labels;
   }
   
   public Label getEditTitle(){
       return editTitle;
   }
   
   public Button getAddButton(){
       return addButton;
   }
   
   public Button getClearButton(){
       return clearButton;
   }
   
   public void clearSelectionFields(){
       restoreFields();
       scheduleTable.getSelectionModel().clearSelection();
       typeChooser.getSelectionModel().clearSelection();
       dateChooser.setValue(null);
       timeField.setText("");
       titleField.setText("");
       topicField.setText("");
       linkField.setText("");
       criteriaField.setText("");
       addButton.setDisable(false);
       update = false;
   }
   
   public void restoreFields(){
       timeField.setDisable(false);
       titleField.setDisable(false);
       topicField.setDisable(false);
       linkField.setDisable(false);
       criteriaField.setDisable(false);  
   }
   
   public void resetTab(){
       clearSelectionFields();
       undo = true;
       startDateChooser.setValue(data.getStartDate());
       endDateChooser.setValue(data.getEndDate());
       undo = false;
       controller.clearTransactions();
   }
   
   public void reloadWorkspace(){
       undo = true;
       startDateChooser.setValue(data.getStartDate());
       endDateChooser.setValue(data.getEndDate());
       undo = false;
   }
}
