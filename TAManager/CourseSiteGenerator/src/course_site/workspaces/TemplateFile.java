
package course_site.workspaces;

import course_site.CourseSiteManagerApp;
import java.io.File;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;


public class TemplateFile {
    private BooleanProperty inUse;
    private File siteFile;
    private StringProperty siteFileName;
    private File jsFile;
    private StringProperty jsFileName;
    private StringProperty header;
    CourseSiteManagerApp app;
    
    public TemplateFile(boolean used, File js, File site, String header,CourseSiteManagerApp app){
        inUse = new SimpleBooleanProperty(used);
        jsFile = js;
        jsFileName = new SimpleStringProperty(js.getName());
        siteFile = site;
        siteFileName = new SimpleStringProperty(site.getName());
        this.header = new SimpleStringProperty(header);
        this.app = app;
        
        inUse.addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> value,Boolean wasSelected,Boolean isSelected){
                app.getGUI().fileController.markAsEdited(app.getGUI());
            }
        });
        
    }
    
    public BooleanProperty getBooleanProperty(){
        return inUse;
    }
    
    public boolean getBoolean(){
        return inUse.get();
    }
    
    public String getSiteFileName(){
        return siteFileName.get();
    }
    
    public String getFullSiteFileName(){
        return siteFile.getAbsolutePath();
    }
    
    public String getJsFileName(){
        return jsFileName.get();
    }
    
    public String getFullJSFileName(){
        return jsFile.getAbsolutePath();
    }
    
    public String getHeader(){
        return header.get();
    }
    
    @Override
    public String toString(){
        return "" + getBoolean() +";" +getHeader()+";"+siteFile.getAbsolutePath()+";"+jsFile.getAbsolutePath();
    }
}
