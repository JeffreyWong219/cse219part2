
package course_site.workspaces;

import course_site.CourseSiteManagerApp;
import course_site.CourseSiteManagerProperties;
import course_site.data.CourseSiteData;
import course_site.data.TADataTabData;
import course_site.data.TeachingAssistant;
import course_site.style.CourseSiteStyle;
import djf.ui.AppMessageDialogSingleton;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import jtps.jTPS;
import properties_manager.PropertiesManager;


public class TADataTab {
    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    CourseSiteManagerApp app;
    CourseSiteWorkspace main;
    TADataTabController controller;
    TADataTabData data;
    BorderPane workspace;
    boolean doneOnce = false;
    boolean undo = true;
    boolean isButtonPressed = false;
    
    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    
    // FOR THE HEADER ON THE LEFT
    HBox tasHeaderBox;
    Label tasHeaderLabel;
    
    // FOR THE TA TABLE
    TableView<TeachingAssistant> taTable;
    TableColumn<TeachingAssistant, String> nameColumn,emailColumn;
    TableColumn<TeachingAssistant, Boolean> graduateColumn;

    // THE TA INPUT
    HBox addBox;
    TextField nameTextField, emailTextField;
    Button addButton,updateButton,clearButton;
    StackPane buttonStack;

    // THE HEADER ON THE RIGHT
    HBox officeHoursHeaderBox;
    Label officeHoursHeaderLabel;
    
    VBox comboBox;
    HBox startComboContainers,endComboContainers;
    Button confirmButton,deleteButton;
    Label startLabel,endLabel;
    ComboBox startCombinations, endCombinations;
    ObservableList<String> comboBoxHours;
    String selectedTA = "";
    
    // THE OFFICE HOURS GRID
    GridPane officeHoursGridPane;
    HashMap<String, Pane> officeHoursGridTimeHeaderPanes;
    HashMap<String, Label> officeHoursGridTimeHeaderLabels;
    HashMap<String, Pane> officeHoursGridDayHeaderPanes;
    HashMap<String, Label> officeHoursGridDayHeaderLabels;
    HashMap<String, Pane> officeHoursGridTimeCellPanes;
    HashMap<String, Label> officeHoursGridTimeCellLabels;
    HashMap<String, Pane> officeHoursGridTACellPanes;
    HashMap<String, Label> officeHoursGridTACellLabels;
    
    boolean toggle = false;
    
    public TADataTab(CourseSiteManagerApp initApp,CourseSiteWorkspace main) {
        // KEEP THIS FOR LATER
        app = initApp;
        this.main = main;
        
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        data = ((CourseSiteData)app.getDataComponent()).getTAData();
        
        // INIT THE HEADER ON THE LEFT
        tasHeaderBox = new HBox();
        String tasHeaderText = props.getProperty(CourseSiteManagerProperties.TAS_HEADER_TEXT.toString());
        tasHeaderLabel = new Label(tasHeaderText);
        deleteButton = new Button("-");
        tasHeaderBox.getChildren().addAll(tasHeaderLabel,deleteButton);
        deleteButton.setOnAction(e->{
            if(taTable.getSelectionModel().getSelectedItem() != null){
                controller.handleTableDeletion();
                resetAdditionFields();
                clearButton.setDisable(!toggle);
                taTable.getSelectionModel().clearSelection();
            }
        });

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        taTable = new TableView();
        
        taTable.setOnMouseClicked(e ->{
            if(taTable.getSelectionModel().getSelectedItem() != null)
            {
                taTable.setEditable(true);
                TeachingAssistant ta = (TeachingAssistant) taTable.getSelectionModel().getSelectedItem();
                nameTextField.setText(ta.getName());
                emailTextField.setText(ta.getEmail());
                addButton.toBack();
                toggle = true;
                clearButton.setDisable(!toggle);
            }
        });
        
        taTable.setOnKeyReleased(e -> { 
            if(e.getCode() != null && e.getCode() == KeyCode.BACK_SPACE && !taTable.getItems().isEmpty() && taTable.getSelectionModel().getSelectedItem() != null){
                controller.handleTableDeletion();
                resetAdditionFields();
                clearButton.setDisable(!toggle);
                taTable.getSelectionModel().clearSelection();
            }
        });
        
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();
        taTable.setItems(tableData);
        String nameColumnText = props.getProperty(CourseSiteManagerProperties.NAME_COLUMN_TEXT.toString());
        String emailColumnText = props.getProperty(CourseSiteManagerProperties.EMAIL_COLUMN_TEXT.toString());
        String undergradText = props.getProperty(CourseSiteManagerProperties.UNDERGRAD_COLUMN_TEXT.toString());
        
        nameColumn = new TableColumn(nameColumnText);
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(.4));
        emailColumn = new TableColumn(emailColumnText);
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(.4));
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("name")
        );
        emailColumn.setCellValueFactory(new PropertyValueFactory<TeachingAssistant,String>("email"));
        
        graduateColumn = new TableColumn(undergradText);
        graduateColumn.setCellValueFactory(new Callback<CellDataFeatures<TeachingAssistant,Boolean>,ObservableValue<Boolean>>(){
        
        @Override
        public ObservableValue<Boolean> call(CellDataFeatures<TeachingAssistant, Boolean> param){
                if(taTable.getSelectionModel().getSelectedItem() != null && !doneOnce){
                    doneOnce = true;
                    if(!undo){
                        controller.handleToggleTAStatus(param.getValue().getName(),param.getValue().getBoolean());
                        undo = true;
                    }
                    return param.getValue().getBooleanProperty();
                }
                else{
                    doneOnce = false;
                    if(isButtonPressed()){
                        setButtonPress(false);
                    }
                    return param.getValue().getBooleanProperty();     
                }
        }

        
    });
        graduateColumn.setCellFactory(CheckBoxTableCell.forTableColumn(graduateColumn));
        graduateColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(.2));
        taTable.setEditable(false);
        //graduateColumn.setEditable(true);
        
        taTable.getColumns().addAll(graduateColumn,nameColumn,emailColumn);

        // ADD BOX FOR ADDING A TA
        String namePromptText = props.getProperty(CourseSiteManagerProperties.NAME_PROMPT_TEXT.toString());
        String emailPromptText = props.getProperty(CourseSiteManagerProperties.EMAIL_PROMPT_TEXT.toString());
        String addButtonText = props.getProperty(CourseSiteManagerProperties.ADD_BUTTON_TEXT.toString());
        String updateButtonText = props.getProperty(CourseSiteManagerProperties.UPDATE_BUTTON_TEXT.toString());
        String clearButtonText = props.getProperty(CourseSiteManagerProperties.CLEAR_BUTTON_TEXT.toString());
        String changeTimeButtonText = props.getProperty(CourseSiteManagerProperties.CHANGE_TIME_BUTTON.toString());
        nameTextField = new TextField();
        nameTextField.setPromptText(namePromptText);
        emailTextField = new TextField();
        emailTextField.setPromptText(emailPromptText);
        addButton = new Button(addButtonText);
        updateButton = new Button(updateButtonText);
        clearButton = new Button(clearButtonText);
        confirmButton = new Button(changeTimeButtonText);
        addBox = new HBox();
        nameTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.3));
        emailTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.3));
        
        buttonStack = new StackPane();
        buttonStack.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));  
        buttonStack.getChildren().add(updateButton);
        buttonStack.getChildren().add(addButton);
        
        //combo box stuff
        comboBoxHours = buildComboBoxEntries();
        startCombinations = new ComboBox(comboBoxHours);
        endCombinations = new ComboBox(buildComboBoxEntries());
        startCombinations.getSelectionModel().select(buildCellText(data.getStartHour(),"00"));
        endCombinations.getSelectionModel().select(buildCellText(data.getEndHour(),"00"));
        startLabel = new Label(props.getProperty(CourseSiteManagerProperties.START_COMBO_BOX_LABEL));
        endLabel = new Label(props.getProperty(CourseSiteManagerProperties.END_COMBO_BOX_LABEL));
        VBox comboBox = new VBox();
        startComboContainers = new HBox(startLabel,startCombinations);
        endComboContainers = new HBox(endLabel, endCombinations);
        comboBox.prefWidthProperty().bind(app.getGUI().getPrimaryScene().widthProperty().multiply(.15));
        comboBox.getChildren().addAll(startComboContainers,endComboContainers,confirmButton);
        
        
        
                
        addButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        updateButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        addBox.getChildren().add(nameTextField);
        addBox.getChildren().add(emailTextField);
        addBox.getChildren().add(buttonStack);
        addBox.getChildren().add(clearButton);

        // INIT THE HEADER ON THE RIGHT
        officeHoursHeaderBox = new HBox();
        String officeHoursGridText = props.getProperty(CourseSiteManagerProperties.OFFICE_HOURS_SUBHEADER.toString());
        officeHoursHeaderLabel = new Label(officeHoursGridText);
        officeHoursHeaderBox.getChildren().add(officeHoursHeaderLabel);
        
        // THESE WILL STORE PANES AND LABELS FOR OUR OFFICE HOURS GRID
        officeHoursGridPane = new GridPane();
        officeHoursGridTimeHeaderPanes = new HashMap();
        officeHoursGridTimeHeaderLabels = new HashMap();
        officeHoursGridDayHeaderPanes = new HashMap();
        officeHoursGridDayHeaderLabels = new HashMap();
        officeHoursGridTimeCellPanes = new HashMap();
        officeHoursGridTimeCellLabels = new HashMap();
        officeHoursGridTACellPanes = new HashMap();
        officeHoursGridTACellLabels = new HashMap();

        // ORGANIZE THE LEFT AND RIGHT PANES
        VBox leftPane = new VBox();
        leftPane.getChildren().add(tasHeaderBox);        
        leftPane.getChildren().add(taTable);        
        leftPane.getChildren().add(addBox);
        VBox rightPane = new VBox();  
        rightPane.getChildren().add(officeHoursHeaderBox);
        rightPane.getChildren().add(officeHoursGridPane);
        
        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane = new SplitPane(leftPane, new ScrollPane(rightPane));
        sPane.setDividerPositions(0.4);
        rightPane.prefWidthProperty().bind(sPane.widthProperty().multiply(1-sPane.getDividers().get(0).getPosition()));
        workspace = new BorderPane();
        
        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(sPane);
        ((BorderPane) workspace).setRight(comboBox);

        // MAKE SURE THE TABLE EXTENDS DOWN FAR ENOUGH
        taTable.prefHeightProperty().bind(workspace.heightProperty().multiply(1.9));
            taTable.prefWidthProperty().bind(app.getGUI().getPrimaryScene().widthProperty().multiply(.85));

        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new TADataTabController(app,data,props,this);

        // CONTROLS FOR ADDING TAs
        nameTextField.setOnAction(e -> {
            if(toggle == false)
                controller.handleAddTA();
            else
                controller.handleUpdateAction();
        });
       
        emailTextField.setOnAction( e -> {
            if(toggle == false)
                controller.handleAddTA();
            else
                controller.handleUpdateAction();
        });
        
        addButton.setOnAction(e -> {
            controller.handleAddTA();
        });
        
        updateButton.setOnAction(e->{
            controller.handleUpdateAction();
        });
        
        clearButton.setOnAction(e->{
            resetAdditionFields();
            nameTextField.requestFocus();
            taTable.getSelectionModel().clearSelection();
        });
        
        startCombinations.setOnAction(e->{
            controller.handleDisableComboButton(this);
        });
        
        endCombinations.setOnAction(e->{
            controller.handleDisableComboButton(this);
        });
        
        confirmButton.setOnAction(e->{
            controller.handleHourChanges(this,data);
        });
        
        workspace.setOnKeyPressed(e->{
            if(e.isControlDown() && e.getCode() != null)
            {
                if(e.getCode() == KeyCode.Z)
                    controller.handleUndoAction();
                if(e.getCode() == KeyCode.Y)
                    controller.handleRedoAction();
            }      
        });
    }
    
    public CourseSiteWorkspace getWorkspace(){
        return main;
    }
    
    public jTPS getTransactions(){
        return controller.getTransactions();
    }
    
    public void setUndo(boolean undo){
        this.undo = undo;
    }
    
    public void setButtonPress(boolean pressed){
        isButtonPressed = pressed;
    }
    
    public boolean isButtonPressed(){
        return isButtonPressed;
    }
    
    public BorderPane getTab()
    {
        return workspace;
    }
    
    public HBox getTAsHeaderBox() {
        return tasHeaderBox;
    }

    public Label getTAsHeaderLabel() {
        return tasHeaderLabel;
    }

    public TableView getTATable() {
        return taTable;
    }

    public HBox getAddBox() {
        return addBox;
    }

    public TextField getNameTextField() {
        return nameTextField;
    }
    
    public TextField getEmailTextField(){
        return emailTextField;
    }

    public Button getAddButton() {
        return addButton;
    }
    
    public Button getUpdateButton(){
        return updateButton;
    }
    
    public Button getClearButton(){
        return clearButton;
    }
    
    public Button getConfirmButton(){
        return confirmButton;
    }

    public HBox getOfficeHoursSubheaderBox() {
        return officeHoursHeaderBox;
    }

    public Label getOfficeHoursSubheaderLabel() {
        return officeHoursHeaderLabel;
    }

    public GridPane getOfficeHoursGridPane() {
        return officeHoursGridPane;
    }

    public ObservableList<String> getComboBoxHours(){
        return comboBoxHours;
    }
    
    public String getChosenStartTime(){
        return (String)startCombinations.getSelectionModel().getSelectedItem();
    }
    
    public String getChosenEndTime(){
        return (String)endCombinations.getSelectionModel().getSelectedItem();
    }
    
    public HashMap<String, Pane> getOfficeHoursGridTimeHeaderPanes() {
        return officeHoursGridTimeHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeHeaderLabels() {
        return officeHoursGridTimeHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridDayHeaderPanes() {
        return officeHoursGridDayHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridDayHeaderLabels() {
        return officeHoursGridDayHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeCellPanes() {
        return officeHoursGridTimeCellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeCellLabels() {
        return officeHoursGridTimeCellLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTACellPanes() {
        return officeHoursGridTACellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTACellLabels() {
        return officeHoursGridTACellLabels;
    }
    
    public String getCellKey(Pane testPane) {
        for (String key : officeHoursGridTACellLabels.keySet()) {
            if (officeHoursGridTACellPanes.get(key) == testPane) {
                return key;
            }
        }
        return null;
    }

    public Label getTACellLabel(String cellKey) {
        return officeHoursGridTACellLabels.get(cellKey);
    }

    public Pane getTACellPane(String cellPane) {
        return officeHoursGridTACellPanes.get(cellPane);
    }

    public String buildCellKey(int col, int row) {
        return "" + col + "_" + row;
    }

    public String buildCellText(int militaryHour, String minutes) {
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        if(hour == 0)
            hour = 12;
        String cellText = "" + hour + ":" + minutes;
        if (militaryHour < 12 || militaryHour == 0) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
    public void resetAdditionFields()
    {
        addButton.toFront();
        nameTextField.setText("");
        emailTextField.setText("");
        toggle = false;
        clearButton.setDisable(!toggle);
    }
    
    public void resetComboBoxSelection()
    {
        startCombinations.getSelectionModel().select(buildCellText((data.getStartHour()),"00"));
        endCombinations.getSelectionModel().select(buildCellText((data.getEndHour()),"00"));
        confirmButton.setDisable(true);
    }

    public void clearTransactionHistory(){
        controller.transactions.clearTransactions();
    }
    
    public void resetWorkspace() {
        // CLEAR OUT THE GRID PANE
        officeHoursGridPane.getChildren().clear();
        // AND THEN ALL THE GRID PANES AND LABELS
        officeHoursGridTimeHeaderPanes.clear();
        officeHoursGridTimeHeaderLabels.clear();
        officeHoursGridDayHeaderPanes.clear();
        officeHoursGridDayHeaderLabels.clear();
        officeHoursGridTimeCellPanes.clear();
        officeHoursGridTimeCellLabels.clear();
        officeHoursGridTACellPanes.clear();
        officeHoursGridTACellLabels.clear();
    }
    
    public void reloadWorkspace() {
        reloadOfficeHoursGrid(data);
    }

    public void reloadOfficeHoursGrid(TADataTabData dataComponent) {        
        ArrayList<String> gridHeaders = dataComponent.getGridHeaders();

        // ADD THE TIME HEADERS
        for (int i = 0; i < 2; i++) {
            addCellToGrid(dataComponent, officeHoursGridTimeHeaderPanes, officeHoursGridTimeHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }
        
        // THEN THE DAY OF WEEK HEADERS
        for (int i = 2; i < 7; i++) {
            addCellToGrid(dataComponent, officeHoursGridDayHeaderPanes, officeHoursGridDayHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));            
        }
        
        // THEN THE TIME AND TA CELLS
        int row = 1;
        int fixedEndHour = dataComponent.getEndHour();
        boolean sameHours = dataComponent.getStartHour() == dataComponent.getEndHour();
        int fixedStartHour = dataComponent.getStartHour();
        if(sameHours)
            fixedEndHour++;
        //buildOfficeHourRowOnHour(dataComponent,1,dataComponent.getStartHour());
        //row++;
        for (int i = fixedStartHour; i < fixedEndHour; i++) {
            // START TIME COLUMN
            /*if(!sameHours)
            {
                if(i < fixedEndHour-1)
                {
                    buildOfficeHourRowOnHalf(dataComponent,row,i);
                     row++;
                    buildOfficeHourRowOnHour(dataComponent,row,i+1);
                    row++;
                }
            }*/
            
            //if(i == fixedEndHour -1){
                buildOfficeHourRowOnHour(dataComponent,row,i);
                row++;
                buildOfficeHourRowOnHalf(dataComponent,row,i);
                row++;  
            //}


        }
        for(int i = 1; i < row; i++)
            for(int c = 2; c < 7; c++)
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, c, i);
            
        // CONTROLS FOR TOGGLING TA OFFICE HOURS
        for (Pane p : officeHoursGridTACellPanes.values()) {
            p.setOnMouseClicked(e -> {
                controller.handleCellToggle((Pane) e.getSource());
            });
            
            p.setOnMouseEntered(e -> {
                Node hoveredPane = (Node)e.getSource();
                int rowOfPane = GridPane.getRowIndex(hoveredPane);
                int columnOfPane = GridPane.getColumnIndex(hoveredPane);
                controller.handleCellHighlighting(rowOfPane,columnOfPane);
            });
            
            p.setOnMouseExited(e -> {
                Node exitedPane = (Node)e.getSource();
                int rowOfPane = GridPane.getRowIndex(exitedPane);
                int columnOfPane = GridPane.getColumnIndex(exitedPane);
                controller.handleCellLeaving(rowOfPane,columnOfPane);
            });
            
        }
        
        // AND MAKE SURE ALL THE COMPONENTS HAVE THE PROPER STYLE
        CourseSiteStyle taStyle = (CourseSiteStyle)app.getStyleComponent();
        taStyle.initOfficeHoursGridStyle();
    }
    
    public void buildOfficeHourRowOnHour(TADataTabData dataComponent, int row, int hourStart){
        addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, 0, row);
        dataComponent.getCellTextProperty(0, row).set(buildCellText(hourStart,"00" ));
        addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, 1, row);
        dataComponent.getCellTextProperty(1, row).set(buildCellText(hourStart, "30"));
        for(int i = 2; i < 7;i++)
            addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, i, row);
    }
    
    public void buildOfficeHourRowOnHalf(TADataTabData dataComponent, int row, int hourStart){
        addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, 0, row);
        dataComponent.getCellTextProperty(0, row).set(buildCellText(hourStart,"30" ));
        addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, 1, row);
        dataComponent.getCellTextProperty(1, row).set(buildCellText(hourStart+1, "00"));
        for(int i = 2; i < 7;i++)
            addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, i, row);
    }
    
    public ObservableList<String> buildComboBoxEntries()
    {
        ObservableList<String> hoursStrings = FXCollections.observableArrayList();
        for(int i = 0; i < 24; i++)
        {
            hoursStrings.add(buildCellText(i,"00"));
        }
        return hoursStrings;
    }
    
    public void addCellToGrid(TADataTabData dataComponent, HashMap<String, Pane> panes, HashMap<String, Label> labels, int col, int row) {       
        // MAKE THE LABEL IN A PANE
        Label cellLabel = new Label("");
        HBox cellPane = new HBox();
        cellPane.setAlignment(Pos.CENTER);
        cellPane.getChildren().add(cellLabel);

        // BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
        String cellKey = dataComponent.getCellKey(col, row);
        cellPane.setId(cellKey);
        cellLabel.setId(cellKey);
        
        // NOW PUT THE CELL IN THE WORKSPACE GRID
        officeHoursGridPane.add(cellPane, col, row);
        
        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
        panes.put(cellKey, cellPane);
        labels.put(cellKey, cellLabel);
        
        // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
        // SO IT CAN MANAGE ALL CHANGES
        dataComponent.setCellProperty(col, row, cellLabel.textProperty());        
    }
}
