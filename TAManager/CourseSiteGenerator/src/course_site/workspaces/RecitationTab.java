
package course_site.workspaces;

import course_site.CourseSiteManagerProperties;
import course_site.data.CourseSiteData;
import course_site.data.Recitation;
import course_site.data.RecitationData;
import course_site.data.TeachingAssistant;
import java.util.ArrayList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import javafx.scene.layout.ColumnConstraints;
import jtps.jTPS;

public class RecitationTab {
    CourseSiteWorkspace main;
    RecitationData data;
    RecitationController controller;
    GridPane container, inputFormat;
    VBox inputContainer;
    Button deleteButton,addButton,clearButton;
    Label recitationTitle,sectionInputLabel,instructorInputLabel,dayInputLabel,locationInputLabel,taOneLabel,taTwoLabel,addTitle;
    ComboBox<TeachingAssistant> taOneChoice,taTwoChoice;
    TextField sectionInput,instructorInput,dayInput,locationInput;
    boolean transition = false;
    boolean update = false;
    
    TableView<Recitation> recitationTable;
    TableColumn<Recitation,String> sectionColumn,instructorColumn,locationColumn,dayColumn,taOneColumn,taTwoColumn;
    
    public RecitationTab(CourseSiteWorkspace workspace){
        main = workspace;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        data = ((CourseSiteData)(workspace.getApp().getDataComponent())).getRecitationData();
        controller = new RecitationController(this,main.getApp(),data);
        
        container = new GridPane();
        
        deleteButton = new Button("-");
        recitationTitle = new Label(props.getProperty(CourseSiteManagerProperties.RECITATION_TITLE.toString()),deleteButton);
        recitationTitle.setContentDisplay(ContentDisplay.RIGHT);
        
        inputContainer = new VBox();
        inputFormat = new GridPane();
        addTitle = new Label(props.getProperty(CourseSiteManagerProperties.ADD_EDIT_TITLE.toString()));
        inputContainer.getChildren().add(addTitle);
        
        sectionInputLabel = new Label(props.getProperty(CourseSiteManagerProperties.SECTION_INPUT_LABEL.toString()));
        sectionInput = new TextField();
        instructorInputLabel = new Label(props.getProperty(CourseSiteManagerProperties.INSTRUCTOR_INPUT_LABEL.toString()));
        instructorInput = new TextField();
        dayInputLabel = new Label(props.getProperty(CourseSiteManagerProperties.DAY_TIME_INPUT_LABEL.toString()));
        dayInput = new TextField();
        locationInputLabel = new Label(props.getProperty(CourseSiteManagerProperties.LOCATION_INPUT_LABEL.toString()));
        locationInput = new TextField();
        
        taOneLabel = new Label(props.getProperty(CourseSiteManagerProperties.TEACHING_ASSISTANT_COLUMN_HEADER.toString()));
        taTwoLabel = new Label(props.getProperty(CourseSiteManagerProperties.TEACHING_ASSISTANT_COLUMN_HEADER.toString()));
        taOneChoice = new ComboBox();
        taOneChoice.setItems(data.getTeachingAssistants());
        taOneChoice.setCellFactory(new Callback<ListView<TeachingAssistant>,ListCell<TeachingAssistant>>(){
            @Override
            public ListCell<TeachingAssistant> call(ListView<TeachingAssistant> param)
            {
                return new ListCell<TeachingAssistant>(){
                    @Override
                    protected void updateItem(TeachingAssistant item, boolean empty)
                    {
                        super.updateItem(item,empty);
                        if(item == null || empty)
                        {
                            setText(null);
                        }
                        else
                            setText(item.getName());
                    }
                };
            }
        });
        
        taTwoChoice = new ComboBox<>(data.getTeachingAssistants());
        taTwoChoice.setCellFactory(new Callback<ListView<TeachingAssistant>,ListCell<TeachingAssistant>>(){
            @Override
            public ListCell<TeachingAssistant> call(ListView<TeachingAssistant> param)
            {
                return new ListCell<TeachingAssistant>(){
                    @Override
                    protected void updateItem(TeachingAssistant item, boolean empty)
                    {
                        super.updateItem(item,empty);
                        if(item == null || empty)
                        {
                            setText(null);
                            
                            
                        }
                        else{
                            setText(item.getName());
                        }
                    }
                };
            }
        });
        
        addButton = new Button(props.getProperty(CourseSiteManagerProperties.ADD_UPDATE_BUTTON.toString()));
        clearButton = new Button(props.getProperty(CourseSiteManagerProperties.CLEAR_BUTTON_TEXT.toString()));
        
        inputFormat.add(sectionInputLabel,0,0);
        inputFormat.add(sectionInput,1,0);
        inputFormat.add(instructorInputLabel,0,1);
        inputFormat.add(instructorInput,1,1);
        inputFormat.add(dayInputLabel,0,2);
        inputFormat.add(dayInput,1,2);
        inputFormat.add(locationInputLabel,0,3);
        inputFormat.add(locationInput,1,3);
        inputFormat.add(taOneLabel,0,4);
        inputFormat.add(taOneChoice,1,4);
        inputFormat.add(taTwoLabel,0,5);
        inputFormat.add(taTwoChoice,1,5);
        inputFormat.add(addButton,0,6);
        inputFormat.add(clearButton,1,6);
        inputContainer.getChildren().add(inputFormat);
        
        recitationTable = new TableView();
        recitationTable.setItems(data.getRecitations());
        String sectionColumnText = props.getProperty(CourseSiteManagerProperties.SECTION_COLUMN_HEADER.toString());
        String instructorColumnText = props.getProperty(CourseSiteManagerProperties.INSTRUCTOR_COLUMN_HEADER.toString());
        String locationColumnText = props.getProperty(CourseSiteManagerProperties.LOCATION_COLUMN_HEADER.toString());
        String dayColumnText = props.getProperty(CourseSiteManagerProperties.DAY_COLUMN_HEADER.toString());
        String taOneAndTwoColumnText = props.getProperty(CourseSiteManagerProperties.TEACHING_ASSISTANT_COLUMN_HEADER.toString());
        
        sectionColumn = new TableColumn(sectionColumnText);
        sectionColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("section"));
        instructorColumn = new TableColumn(instructorColumnText);
        instructorColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("instructor"));
        dayColumn = new TableColumn(dayColumnText);
        dayColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("day"));
        locationColumn = new TableColumn(locationColumnText);
        locationColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("location"));
        taOneColumn = new TableColumn(taOneAndTwoColumnText);
        taOneColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("taOne"));
        taTwoColumn = new TableColumn(taOneAndTwoColumnText);
        taTwoColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("taTwo"));
        recitationTable.getColumns().addAll(sectionColumn,instructorColumn,dayColumn,locationColumn,taOneColumn,taTwoColumn);
             
        container.add(recitationTitle,0,0);
        container.add(inputContainer,1,1);
        container.add(recitationTable,0,1);
        ColumnConstraints c1 = new ColumnConstraints();
        c1.setPercentWidth(50);
        container.getColumnConstraints().add(c1);
        recitationTable.prefWidthProperty().bind(container.widthProperty().multiply(.5));
        sectionColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(1/6.0));
        instructorColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(1/6.0));
        dayColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(1/6.0));
        locationColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(1/6.0));
        taOneColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(1/6.0));
        taTwoColumn.prefWidthProperty().bind(recitationTable.widthProperty().multiply(1/6.0));
        
        sectionInput.prefWidthProperty().bind(container.widthProperty().multiply(.2));
        instructorInput.prefWidthProperty().bind(container.widthProperty().multiply(.2));
        dayInput.prefWidthProperty().bind(container.widthProperty().multiply(.2));
        locationInput.prefWidthProperty().bind(container.widthProperty().multiply(.2));
        taOneChoice.prefWidthProperty().bind(container.widthProperty().multiply(.1));
        taTwoChoice.prefWidthProperty().bind(container.widthProperty().multiply(.1));
        addButton.setTranslateX(75);
        addButton.setTranslateY(10);
        clearButton.setTranslateX(75);
        clearButton.setTranslateY(10);
        
        addButton.setOnAction(e->{
            if(!update)
                controller.handleAddRecitation();
            else
                controller.handleUpdateAction();
        });
        
        deleteButton.setOnAction(e->{
            if(recitationTable.getSelectionModel().getSelectedItem() != null)
                controller.handleDeleteRecitation();
        });
        
        clearButton.setOnAction(e->{
            clearTextFields();
        });
        
        recitationTable.setOnKeyReleased(e -> { 
            if(e.getCode() != null && e.getCode() == KeyCode.BACK_SPACE && !recitationTable.getItems().isEmpty() && recitationTable.getSelectionModel().getSelectedItem() != null){
                controller.handleDeleteRecitation();
                clearTextFields();
            }
        });
        recitationTable.setOnMouseClicked(e->{
            if(recitationTable.getSelectionModel().getSelectedItem()!=null){
                controller.handleTableSelection();
                update = true;
            }
        });
        
        container.setOnKeyPressed(e->{
            if(e.isControlDown() && e.getCode() != null)
            {
                if(e.getCode() == KeyCode.Z)
                    controller.handleUndoAction();
                if(e.getCode() == KeyCode.Y)
                    controller.handleRedoAction();
            }      
        });
        
        taOneChoice.setOnAction(e->{
            controller.handleTASelection();
        });
        
        taTwoChoice.setOnAction(e->{
            controller.handleTASelection();
        });
        
    }
    
    public GridPane getTab(){
        return container;
    }
    
    public jTPS getTransactions(){
        return controller.getTransactions();
    }
    
    public RecitationController getController(){
        return controller;
    }
    
    public Label getTitle(){
        return recitationTitle;
    }
    
    public Label getEditTitle(){
        return addTitle;
    }
    
    public boolean getTransition(){
        return transition;
    }
    
    public void setTransition(boolean set){
        transition = set;
    }
    
    public boolean getUpdate(){
        return update;
    }
    
    public void setUpdate(boolean set){
        update = set;
    }
    
    public ArrayList<Label> getFieldLabels(){
        ArrayList<Label> labels = new ArrayList<>();
        labels.add(dayInputLabel);
        labels.add(locationInputLabel);
        labels.add(instructorInputLabel);
        labels.add(sectionInputLabel);
        labels.add(taOneLabel);
        labels.add(taTwoLabel);
        return labels;
    }
    
    public Button getAddButton(){
        return addButton;
    }
    
    public Button getClearButton(){
        return clearButton;
    }
    
    public TableView<Recitation> getTable(){
        return recitationTable;
    }
    
    public TextField getSectionInput(){
        return sectionInput;
    }
    
    public TextField getInstructorInput(){
        return instructorInput;
    }
    
    public TextField getDayInput(){
        return dayInput;
    }
    
    public TextField getLocationInput(){
        return locationInput;
    }
    
    public ComboBox<TeachingAssistant> getTAOneChoice(){
        return taOneChoice;
    }
    
    public ComboBox<TeachingAssistant> getTATwoChoice(){
        return taTwoChoice;
    }
    
    public TeachingAssistant getTeachingAssistantOne(){
        return taOneChoice.getSelectionModel().getSelectedItem();
    }
    
    public TeachingAssistant getTeachingAssistantTwo(){
        return taTwoChoice.getSelectionModel().getSelectedItem();
    }
  
    public void clearTextFields(){
        transition = true;
        taOneChoice.getSelectionModel().select(null);
        taTwoChoice.getSelectionModel().select(null);
        sectionInput.setText("");
        instructorInput.setText("");
        dayInput.setText("");
        locationInput.setText("");
        recitationTable.getSelectionModel().clearSelection();
        update = false;
        transition = false;
    }
    
    public void resetTab(){
        clearTextFields();
        controller.clearTransactions();
    }
}
