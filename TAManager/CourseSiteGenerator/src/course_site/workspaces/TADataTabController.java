/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package course_site.workspaces;

import course_site.CourseSiteManagerApp;
import static course_site.CourseSiteManagerProperties.DELETE_TABLE_ROW_MESSAGE;
import static course_site.CourseSiteManagerProperties.DELETE_TABLE_ROW_TITLE;
import static course_site.CourseSiteManagerProperties.MISSING_TA_EMAIL_MESSAGE;
import static course_site.CourseSiteManagerProperties.MISSING_TA_EMAIL_TITLE;
import static course_site.CourseSiteManagerProperties.MISSING_TA_NAME_MESSAGE;
import static course_site.CourseSiteManagerProperties.MISSING_TA_NAME_TITLE;
import static course_site.CourseSiteManagerProperties.REMOVE_TA_FROM_TIMES_MESSAGE;
import static course_site.CourseSiteManagerProperties.REMOVE_TA_FROM_TIMES_TITLE;
import static course_site.CourseSiteManagerProperties.TA_EMAIL_NOT_RIGHT_FORMAT_MESSAGE;
import static course_site.CourseSiteManagerProperties.TA_EMAIL_NOT_RIGHT_FORMAT_TITLE;
import static course_site.CourseSiteManagerProperties.TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE;
import static course_site.CourseSiteManagerProperties.TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE;
import course_site.data.TADataTabData;
import course_site.data.TeachingAssistant;
import course_site.style.CourseSiteStyle;
import course_site.transactions.AddTA_Transaction;
import course_site.transactions.DeleteTA_Transaction;
import course_site.transactions.ToggleGraduate_Transaction;
import course_site.transactions.ToggleTA_Transaction;
import course_site.transactions.UpdateOfficeHours_Transaction;
import course_site.transactions.UpdateTA_Transaction;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;

/**
 *
 * @author cool7
 */
public class TADataTabController {
    CourseSiteManagerApp app;
    TADataTab tab;
    TADataTabData data;
    PropertiesManager properties;
    jTPS transactions;
    /**
     * Constructor, note that the app must already be constructed.
     */
    public TADataTabController(CourseSiteManagerApp initApp,TADataTabData data,PropertiesManager properties, TADataTab tab) {
        // KEEP THIS FOR LATER
        app = initApp;
        this.data = data;
        this.properties = properties;
        this.tab = tab;
        transactions = new jTPS();
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        TextField nameTextField = tab.getNameTextField();
        TextField emailTextField = tab.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        if(checkValidity(name, email)){
            // ADD THE NEW TA TO THE DATA
            performTransaction(new AddTA_Transaction(name,email,data));
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            app.getGUI().fileController.markAsEdited(app.getGUI());
        }
    }
    
    public boolean checkValidity(String nameText,String emailText)
    { 
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        return checkIfEmptyName(props,nameText)&&checkIfEmptyEmail(props,emailText)
                &&checkIfUniqueAndValidEmail(props,emailText)&&checkIfUniqueTAName(props,nameText);
    }
    
    public boolean checkIfEmptyName(PropertiesManager props, String nameText){
        Pattern namePattern = Pattern.compile("^['a-zA-Z0-9 ]*$");
        Matcher nameMatcher = namePattern.matcher(nameText);
        if (nameText.isEmpty() || !nameMatcher.matches()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));  
            return false;
        }
        return true;
    }
    
    public boolean checkIfEmptyEmail(PropertiesManager props, String emailText)
    {
        if(emailText.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
            return false;
        }
        return true;
    }
    
    public boolean checkIfUniqueAndValidEmail(PropertiesManager props, String emailText)
    {
        Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher emailMatcher = emailPattern.matcher(emailText);
        if(!emailMatcher.matches())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TA_EMAIL_NOT_RIGHT_FORMAT_TITLE), props.getProperty(TA_EMAIL_NOT_RIGHT_FORMAT_MESSAGE));
            return false;
        }
        else if(data.containsTAEmail(emailText))
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE)); 
            return false;
        }
        return true;
    }
    
    public boolean checkIfUniqueTAName(PropertiesManager props, String nameText)
    {
        if(data.containsTA(nameText))
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE)); 
            return false;
        }
        return true;
    }

    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TableView taTable = tab.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        
        // GET THE TA
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String taName = ta.getName();
        String cellKey = pane.getId();
        performTransaction(new ToggleTA_Transaction(data,taName,cellKey));
        app.getGUI().fileController.markAsEdited(app.getGUI());
        
    }
    
    public void handleTableDeletion(){
        TableView table = tab.getTATable();
        PropertiesManager props = PropertiesManager.getPropertiesManager();    
        AppYesNoCancelDialogSingleton confirmation = AppYesNoCancelDialogSingleton.getSingleton();
        confirmation.show(props.getProperty(DELETE_TABLE_ROW_TITLE),props.getProperty(DELETE_TABLE_ROW_MESSAGE));
        String choice = confirmation.getSelection();
        if(choice.equals(AppYesNoCancelDialogSingleton.YES)|| choice.equals(AppYesNoCancelDialogSingleton.YES_SPANISH))
        {
            Object selected = table.getSelectionModel().getSelectedItem();
            if(selected != null)
            {
                TeachingAssistant ta = (TeachingAssistant)selected;
                performTransaction(new DeleteTA_Transaction(ta.getName(),ta.getEmail(),ta.getBoolean(),data.retrieveAllInstances(ta.getName()),data));
                app.getGUI().fileController.markAsEdited(app.getGUI());
            }
        }
    }
    
    public void handleCellHighlighting(int row, int column)
    {
        ((CourseSiteStyle)app.getStyleComponent()).changeStyleOnHover(tab, row, column);
    }
    
    public void handleCellLeaving(int row, int column)
    {
        ((CourseSiteStyle)app.getStyleComponent()).revertChanges(tab, row, column);
    }
    
    public void handleUpdateAction()
    {
        ObservableList<TeachingAssistant> list  = data.getTeachingAssistants();
        
        TableView table = tab.getTATable();
        TextField nameTextField = tab.getNameTextField();
        TextField emailTextField = tab.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        
        TeachingAssistant selected = data.getTA(((TeachingAssistant)table.getSelectionModel().getSelectedItem()).getName());
        String originalName = selected.getName();
        String originalEmail = selected.getEmail();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        boolean proceed = true;
        if(!originalEmail.equals(email) && originalName.equals(name))
           proceed = checkIfUniqueAndValidEmail(props,email) && checkIfEmptyEmail(props,email);
        if(originalEmail.equals(email)&& !originalName.equals(name))
           proceed = checkIfUniqueTAName(props,name) && checkIfEmptyName(props,name);
        if(!originalEmail.equals(email)&& !originalName.equals(name))
           proceed = checkValidity(name,email);
        if(proceed){
            performTransaction(new UpdateTA_Transaction(originalName,originalEmail,name,email,data,tab));
            app.getGUI().fileController.markAsEdited(app.getGUI());
        }
    }
    
    public void handleToggleTAStatus(String name, boolean toggleStatus){
        
        performTransaction(new ToggleGraduate_Transaction(toggleStatus,name,data));
        app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void handleDisableComboButton(TADataTab workspace)
    {   
        int startWeight = workspace.getComboBoxHours().indexOf(workspace.getChosenStartTime());
        int endWeight = workspace.getComboBoxHours().indexOf(workspace.getChosenEndTime());
        if(startWeight >= endWeight)
            workspace.getConfirmButton().setDisable(true);
        else if(workspace.getConfirmButton().isDisabled())
            workspace.getConfirmButton().setDisable(false);
    }
            
    public void handleHourChanges(TADataTab workspace,TADataTabData data){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppYesNoCancelDialogSingleton confirmation = AppYesNoCancelDialogSingleton.getSingleton();
        String newStart = data.getMilitaryHour(workspace.getChosenStartTime());
        String newEnd = data.getMilitaryHour(workspace.getChosenEndTime());
        String earliestHour = data.findEarliestHour();
        String latestHour = data.findLatestHour();
        boolean overwriteEarliestHour = false;
        boolean overwriteLatestHour = false;
        if(latestHour != null && earliestHour != null)
        {
            overwriteEarliestHour = data.compareMilitaryHour(earliestHour, newStart) < 0;
            overwriteLatestHour = data.compareMilitaryHour(latestHour, newEnd) > 0;
        }
        String choice = "";
        if(overwriteEarliestHour || overwriteLatestHour){
            confirmation.show(props.getProperty(REMOVE_TA_FROM_TIMES_TITLE),props.getProperty(REMOVE_TA_FROM_TIMES_MESSAGE));
            choice = confirmation.getSelection();
        }
        if((choice.equals(AppYesNoCancelDialogSingleton.YES)|| choice.equals(AppYesNoCancelDialogSingleton.YES_SPANISH))|| (!overwriteEarliestHour && !overwriteLatestHour))
        {
            performTransaction(new UpdateOfficeHours_Transaction(data.getTimeString(data.getStartHour(),false),
                    data.getTimeString(data.getEndHour(),false),workspace.getChosenStartTime(),workspace.getChosenEndTime(),data,workspace));
            app.getGUI().fileController.markAsEdited(app.getGUI());
        }
    }
    
    public void performTransaction(jTPS_Transaction transaction){
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getUndoButton().setDisable(false);
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getRedoButton().setDisable(true);
        transactions.addTransaction(transaction);
    }
    
    public void handleUndoAction()
    {
        tab.setButtonPress(true);
        transactions.undoTransaction();
        updateButtons();
        tab.resetAdditionFields();
        if(app.getGUI().fileController.isSaved() && !transactions.nothingToUndo())
            app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void handleRedoAction(){
        tab.setButtonPress(true);
        transactions.doTransaction();
        updateButtons();
        tab.resetAdditionFields();
        if(app.getGUI().fileController.isSaved() && !transactions.nothingToRedo())
            app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void clearTransactions(){
        transactions.clearTransactions();
    }
    
    public jTPS getTransactions(){
        return transactions;
    }
    
    public void updateButtons(){
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getRedoButton().setDisable(transactions.nothingToRedo());
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getUndoButton().setDisable(transactions.nothingToUndo());
    }
}
