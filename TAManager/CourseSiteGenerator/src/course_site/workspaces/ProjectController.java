
package course_site.workspaces;

import course_site.CourseSiteManagerApp;
import course_site.CourseSiteManagerProperties;
import course_site.data.ProjectData;
import course_site.data.Student;
import course_site.data.Team;
import course_site.transactions.AddOrDeleteStudent_Transaction;
import course_site.transactions.AddTeam_Transaction;
import course_site.transactions.DeleteTeam_Transaction;
import course_site.transactions.UpdateStudent_Transaction;
import course_site.transactions.UpdateTeam_Transaction;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.ArrayList;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import javafx.scene.paint.Color;


public class ProjectController {
    ProjectTab tab;
    CourseSiteManagerApp app;
    ProjectData data;
    jTPS transactions;
    PropertiesManager props;
    AppYesNoCancelDialogSingleton choiceDialog;
    AppMessageDialogSingleton dialog;
    
    public ProjectController(ProjectTab tab,CourseSiteManagerApp workspace, ProjectData data){
        this.tab = tab;
        app = workspace;
        this.data = data;
        transactions = new jTPS();
        dialog = AppMessageDialogSingleton.getSingleton();
        choiceDialog = AppYesNoCancelDialogSingleton.getSingleton();
        props = PropertiesManager.getPropertiesManager();
    }
    
    public void handleAddTeam(){
        String teamName = tab.getTeamNameChoice().getText();
        Color teamColor = tab.getTeamColor().getValue();
        Color teamTextColor = tab.getTeamTextColor().getValue();
        String teamLink = tab.getTeamLinkChoice().getText();
        if(teamName.equals("")||teamLink.equals("")){
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_TEAM_NAME_TITLE.toString()), props.getProperty(CourseSiteManagerProperties.MISSING_TEAM_NAME_MESSAGE.toString()));
            return;
        }
        if(data.getTeam(teamName)!= null){
            dialog.show(props.getProperty(CourseSiteManagerProperties.UNIQUE_TEAM_NAME_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.UNIQUE_TEAM_NAME_MESSAGE.toString()));
            return;
        }
        else{
            performTransaction(new AddTeam_Transaction(teamName+";"+data.convertColorToHexCode(teamColor)+";"+data.convertColorToHexCode(teamTextColor)+";"+teamLink,data));
            tab.clearTeamSelectionFields();
        }
    }
    
    public void handleDeleteTeam(){
        Team t = tab.getTeamTable().getSelectionModel().getSelectedItem();
        String name = t.getName();
        String color = t.getColor();
        String textColor = t.getTextColor();
        String link = t.getLink();
        ArrayList<String> students = t.getStudents();
        choiceDialog.show(props.getProperty(CourseSiteManagerProperties.VERIFY_DELETE_TEAM_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.VERIFY_DELETE_TEAM_MESSAGE.toString()));
        if(choiceDialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES)||choiceDialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES_SPANISH)){
            performTransaction(new DeleteTeam_Transaction(name,color,textColor,link,students,data,tab));
        }
    }
    
    public void handleUpdateTeam(){
        Team t = tab.getTeamTable().getSelectionModel().getSelectedItem();
        String teamName = tab.getTeamNameChoice().getText();
        Color teamColor = tab.getTeamColor().getValue();
        Color teamTextColor = tab.getTeamTextColor().getValue();
        String teamLink = tab.getTeamLinkChoice().getText();
        if(teamName.equals("")||teamLink.equals("")){
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_TEAM_NAME_TITLE.toString()), props.getProperty(CourseSiteManagerProperties.MISSING_TEAM_NAME_MESSAGE.toString()));
            return;
        }
        if(!t.getName().equals(teamName) && data.getTeam(teamName)!= null){
            dialog.show(props.getProperty(CourseSiteManagerProperties.UNIQUE_TEAM_NAME_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.UNIQUE_TEAM_NAME_MESSAGE.toString()));
            return;
        }
        else
            performTransaction(new UpdateTeam_Transaction(t.transcribeData(),teamName+";"+teamColor+";"+teamTextColor+";"+teamLink,data,tab));
        
    }
    
    public void handleAddStudent(){
        String studentFirst = tab.getStudentFirstNameChoice().getText();
        String studentLast = tab.getStudentLastNameChoice().getText();
        Team studentChoice = tab.getTeamSelection().getSelectionModel().getSelectedItem();
        String studentRole = tab.getStudentRoleChoice().getText();
        if(studentFirst.equals("") || studentLast.equals("")){
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_STUDENT_NAME_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_STUDENT_NAME_MESSAGE.toString()));
            return;
        }
        if(!studentRole.equals(""))
            switch(studentRole){
                case "Lead Programmer":
                case "Programador principal":
                case "Project Manager":
                case "Gerente de proyecto":
                case "Lead Designer":
                case "Diseñador encargado":
                case "Data Designer":
                case "Diseñador de datos":
                case "Mobile Developer":
                case "Desarrollador Móvil":    
                    break;
                default:{
                    dialog.show(props.getProperty(CourseSiteManagerProperties.INVALID_ROLE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.INVALID_ROLE_MESSAGE.toString()));
                    return;
                }
            }
        if(data.getStudent(studentFirst,studentLast) != null){
            dialog.show(props.getProperty(CourseSiteManagerProperties.UNIQUE_STUDENT_NAME_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.UNIQUE_STUDENT_NAME_MESSAGE.toString()));
            return;
        }
        else{
            String teamName = "";
            if(studentChoice != null)
                teamName = studentChoice.getName();
            performTransaction(new AddOrDeleteStudent_Transaction(studentFirst,studentLast,teamName,studentRole,false,data));
            tab.clearStudentSelectionFields();
        }
    }
    
    public void handleDeleteStudent(){
        Student s = tab.getStudentTable().getSelectionModel().getSelectedItem();
        choiceDialog.show(props.getProperty(CourseSiteManagerProperties.VERIFY_DELETE_STUDENT_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.VERIFY_DELETE_STUDENT_MESSAGE.toString()));
        if(choiceDialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES)||choiceDialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES_SPANISH))
            performTransaction(new AddOrDeleteStudent_Transaction(s.getFirstName(),s.getLastName(),s.getTeamName(),s.getRole(),true,data));
    }
    
    public void handleUpdateStudent(){
        Student s = tab.getStudentTable().getSelectionModel().getSelectedItem();
        String studentFirst = tab.getStudentFirstNameChoice().getText();
        String studentLast = tab.getStudentLastNameChoice().getText();
        Team studentChoice = tab.getTeamSelection().getSelectionModel().getSelectedItem();
        String studentRole = tab.getStudentRoleChoice().getText();
        String information = "";
        boolean changeTeam = false;
         if(studentFirst.equals("") || studentLast.equals("")){
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_STUDENT_NAME_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_STUDENT_NAME_MESSAGE.toString()));
            return;
        }
        if((!s.getFirstName().equals(studentFirst) || !s.getLastName().equals(studentLast)) &&data.getStudent(studentFirst,studentLast) != null){
            dialog.show(props.getProperty(CourseSiteManagerProperties.UNIQUE_STUDENT_NAME_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.UNIQUE_STUDENT_NAME_MESSAGE.toString()));
            return;
        }
        
        if(!studentRole.equals(""))
            switch(studentRole){
                case "Lead Programmer":
                case "Programador principal":
                case "Project Manager":
                case "Gerente de proyecto":
                case "Lead Designer":
                case "Diseñador encargado":
                case "Data Designer":
                case "Diseñador de datos":
                case "Mobile Developer":
                case "Desarrollador Móvil":    
                    break;
                default:{
                    dialog.show(props.getProperty(CourseSiteManagerProperties.INVALID_ROLE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.INVALID_ROLE_MESSAGE.toString()));
                    return;
                }
            }
        
        if(!studentFirst.equals(s.getFirstName()))
            information += studentFirst+";";
        else
            information += s.getFirstName()+";";
        
        if(!studentLast.equals(s.getLastName()))
            information += studentLast+";";
        else
            information += s.getLastName()+";";
        
        if(studentChoice == null){
            information += "^&"+";";  
        }
        else if(!studentChoice.getName().equals(s.getTeamName())){
            changeTeam = true;
            information += studentChoice.getName() +";";
        }
        else
            information += s.getTeamName() +";";
        
        if(studentRole.equals("")){
            information += "^&";
        }
        else if(!studentRole.equals(s.getRole())){
            information += studentRole;
        }
        else
            information += s.getRole();
        
        performTransaction(new UpdateStudent_Transaction(s.toString(),information,data,tab,changeTeam));
    }
    
    public void handleStudentTableSelection(){
        Student s = tab.getStudentTable().getSelectionModel().getSelectedItem();
        tab.getStudentFirstNameChoice().setText(s.getFirstName());
        tab.getStudentLastNameChoice().setText(s.getLastName());
        Team t = data.getTeam(s.getTeamName());
        if(t != null)
            tab.getTeamSelection().getSelectionModel().select(t);
        else
            tab.getTeamSelection().getSelectionModel().select(null);
        tab.getStudentRoleChoice().setText(s.getRole());
        tab.setUpdateStudentStatus(true);
    }
    
    public void handleTeamTableSelection(){
        Team t = tab.getTeamTable().getSelectionModel().getSelectedItem();
        tab.getTeamNameChoice().setText(t.getName());
        tab.getTeamLinkChoice().setText(t.getLink());
        tab.getTeamColor().setValue(Color.web(t.getColor()));
        tab.getTeamTextColor().setValue(Color.web(t.getTextColor()));
        tab.setUpdateTeamStatus(true);
    }
    
    public jTPS getTransactions(){
        return transactions;
    }
    
    public void clearTransactions(){
        transactions.clearTransactions();
    }
    
    public void performTransaction(jTPS_Transaction transaction){
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getUndoButton().setDisable(false);
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getRedoButton().setDisable(true);
        transactions.addTransaction(transaction);
        app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void handleUndoAction()
    {   
        transactions.undoTransaction();
        updateButtons();
        if(app.getGUI().fileController.isSaved() && !transactions.nothingToUndo())
            app.getGUI().fileController.markAsEdited(app.getGUI());

    }
    
    public void handleRedoAction(){
        transactions.doTransaction();
        updateButtons();
        if(app.getGUI().fileController.isSaved() && !transactions.nothingToRedo())
            app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void updateButtons(){
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getRedoButton().setDisable(transactions.nothingToRedo());
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getUndoButton().setDisable(transactions.nothingToUndo());
    }
}
