
package course_site.workspaces;

import course_site.CourseSiteManagerApp;
import course_site.CourseSiteManagerProperties;
import course_site.data.Recitation;
import course_site.data.RecitationData;
import course_site.data.TeachingAssistant;
import course_site.transactions.AddOrDeleteRecitation_Transaction;
import course_site.transactions.UpdateRecitation_Transaction;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import javafx.collections.ObservableList;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;

public class RecitationController {
    RecitationTab tab;
    CourseSiteManagerApp app;
    RecitationData data;
    jTPS transactions;
    
    public RecitationController(RecitationTab workspace, CourseSiteManagerApp app, RecitationData data){
        tab = workspace;
        this.app = app;
        this.data = data;
        transactions = new jTPS();
    }
    
    public void handleAddRecitation(){
        String section = tab.getSectionInput().getText();
        String instructor = tab.getInstructorInput().getText();
        String day = tab.getDayInput().getText();
        String location = tab.getLocationInput().getText();
        TeachingAssistant taOne = tab.getTeachingAssistantOne();
        TeachingAssistant taTwo = tab.getTeachingAssistantTwo();
        
        if(!verifyAllConditions(section,instructor,day,location,taOne,taTwo))
            return;
        
        String taOneName = "?";
        if(taOne != null){
            taOneName = taOne.getName();
        }
        String taTwoName = "?";
        if(taTwo != null){
            taTwoName = taTwo.getName();
        }    
        String info = section+";"+instructor+";"+day+";"+location+";"+taOneName+";"+taTwoName;
        performTransaction(new AddOrDeleteRecitation_Transaction(info,false,data));
        tab.clearTextFields();
    }
    
    public void handleDeleteRecitation(){
        Recitation r = tab.getTable().getSelectionModel().getSelectedItem();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if(r != null){
            AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
            dialog.show(props.getProperty(CourseSiteManagerProperties.VERIFY_DELETE_RECITATION_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.VERIFY_DELETE_RECITATION_MESSAGE.toString()));
            if(dialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES)|| dialog.getSelection().equals(AppYesNoCancelDialogSingleton.YES_SPANISH))
                performTransaction(new AddOrDeleteRecitation_Transaction(r.transcribeData(),true,data));
        }
    }
    
    public boolean verifyAllConditions(String section,String instructor,String day,String location,TeachingAssistant taOne,TeachingAssistant taTwo){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        if(!verifyAllFilled(section,instructor,day,location)){
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_RECITATION_INFO_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_RECITATION_INFO_MESSAGE.toString()));
            return false;
        }
        if(taOne != null && taTwo != null && taOne == taTwo){
            dialog.show(props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_MESSAGE.toString()));
            return false;
        }
        /*if(taOne != null && !verifyUniqueTAAndTime(taOne.getName(),day)){  commented out as mckenna has changed his specifications
            dialog.show(props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_MESSAGE.toString()));
            return false;
        }
        if(taTwo != null && !verifyUniqueTAAndTime(taTwo.getName(),day)){
            dialog.show(props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_MESSAGE.toString()));
            return false;
        }*/
        if(!verifyUniqueSection(section)){
            dialog.show(props.getProperty(CourseSiteManagerProperties.UNIQUE_SECTION_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.UNIQUE_SECTION_MESSAGE.toString()));
            return false;
        }
        return true;
    }
    
    public boolean verifyUniqueTAAndTime(String taAttempt, String timeAttempt){
        ObservableList<Recitation> recitations = data.getRecitations();
        for(Recitation r:recitations){
            if(r.getTaOne().equals(taAttempt) && r.getDay().equals(timeAttempt))
                return false;
            if(r.getTaTwo().equals(taAttempt)&&r.getDay().equals(timeAttempt))
                return false;
        }
        return true;
        //return !(data.containsTA(taAttempt) && data.containsTime(timeAttempt));
    }
    
    public boolean verifyUniqueSection(String section){
        return !data.containsSection(section);
    }
    
    public boolean verifyAllFilled(String section,String instructor,String day, String location){
        return !section.equals("") && !instructor.equals("") && !day.equals("") && !location.equals("");
    }
    
    public void handleTASelection(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        if(tab.getTeachingAssistantOne() == tab.getTeachingAssistantTwo() && !tab.getTransition())
        {
            dialog.show(props.getProperty(CourseSiteManagerProperties.SAME_RECITATION_TA_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.SAME_RECITATION_TA_MESSAGE.toString()));
            tab.getAddButton().setDisable(true);
        }
        else
        {
            tab.getAddButton().setDisable(false);
        }
    }
    
    public jTPS getTransactions(){
        return transactions;
    }
    
    public void clearTransactions(){
        transactions.clearTransactions();
    }
    
    public void performTransaction(jTPS_Transaction transaction){
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getUndoButton().setDisable(false);
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getRedoButton().setDisable(true);
        transactions.addTransaction(transaction);
        app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void handleUndoAction()
    {
        transactions.undoTransaction();
        updateButtons();
        if(app.getGUI().fileController.isSaved() && !transactions.nothingToUndo())
            app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void handleRedoAction(){
        transactions.doTransaction();
        updateButtons();
        if(app.getGUI().fileController.isSaved() && !transactions.nothingToRedo())
            app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void updateButtons(){
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getRedoButton().setDisable(transactions.nothingToRedo());
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getUndoButton().setDisable(transactions.nothingToUndo());
    }
    
    public void handleTableSelection(){
        tab.setTransition(true);
        Recitation r = tab.getTable().getSelectionModel().getSelectedItem();
        tab.getSectionInput().setText(r.getSection());
        tab.getInstructorInput().setText(r.getInstructor());
        tab.getDayInput().setText(r.getDay());
        tab.getLocationInput().setText(r.getLocation());
        if(r.getTaOne().equals("")){
            tab.getTAOneChoice().getSelectionModel().select(null);
        }
        else{
            tab.getTAOneChoice().getSelectionModel().select(data.getTA(r.getTaOne()));
        }
        if(r.getTaTwo().equals("")){
            tab.getTATwoChoice().getSelectionModel().select(null);
        }
        else{
            tab.getTATwoChoice().getSelectionModel().select(data.getTA(r.getTaTwo()));
        }
        tab.setTransition(false);
    }
    
    public void handleUpdateAction(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        String section = tab.getSectionInput().getText();
        String instructor = tab.getInstructorInput().getText();
        String day = tab.getDayInput().getText();
        String location = tab.getLocationInput().getText();
        TeachingAssistant taOne = tab.getTeachingAssistantOne();
        TeachingAssistant taTwo = tab.getTeachingAssistantTwo();
        Recitation r = tab.getTable().getSelectionModel().getSelectedItem();
        
        if(!verifyAllFilled(section,instructor,day,location)){
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_RECITATION_INFO_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_RECITATION_INFO_MESSAGE.toString()));
            return;
        }
        
        if(!r.getSection().equals(section))
            if(!verifyUniqueSection(section)){
                dialog.show(props.getProperty(CourseSiteManagerProperties.UNIQUE_SECTION_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.UNIQUE_SECTION_MESSAGE.toString()));
                return;
            }
        if(taOne != null && taTwo != null && taOne == taTwo){
            dialog.show(props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_MESSAGE.toString()));
            return;
        }
        /*if(taOne != null && !r.getTaOne().equals(taOne.getName())){ commented out as mckenna has changed his specifications
            if(!verifyUniqueTAAndTime(taOne.getName(), day)){
                dialog.show(props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_MESSAGE.toString()));
                return;
            }
        }
        
        if(taTwo != null && !r.getTaTwo().equals(taTwo.getName())){
            if(!verifyUniqueTAAndTime(taTwo.getName(), day)){
                dialog.show(props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.TA_TIME_CONFLICT_MESSAGE.toString()));
                return;
            }
        }*/
        
        String firstTA = "";
        if(taOne == null)
            firstTA = "?";
        else
            firstTA = taOne.getName();
        String secondTA = "";
        if(taTwo == null)
            secondTA = "?";
        else
            secondTA = taTwo.getName();
        String newInformation = section + ";"+instructor+";"+day+";"+location+";"+firstTA+";"+secondTA;
        performTransaction(new UpdateRecitation_Transaction(r.transcribeData(),newInformation,data,tab));
        tab.getTable().refresh();
    }

}
