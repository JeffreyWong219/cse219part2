package course_site.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import course_site.CourseSiteManagerApp;
import course_site.CourseSiteManagerProperties;
import course_site.data.CourseSiteData;
import course_site.data.ProjectData;
import course_site.data.Recitation;
import course_site.data.RecitationData;
import course_site.data.ScheduleData;
import course_site.data.ScheduleEvent;
import course_site.data.Student;
import course_site.data.TADataTabData;
import course_site.data.TeachingAssistant;
import course_site.data.Team;
import course_site.test_bed.TestSave;
import course_site.workspaces.CourseEditorTab;
import course_site.workspaces.CourseSiteWorkspace;
import course_site.workspaces.TADataTab;
import course_site.workspaces.TemplateFile;
import djf.settings.AppPropertyType;
import djf.ui.AppMessageDialogSingleton;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import javax.json.JsonObjectBuilder;
import properties_manager.PropertiesManager;

/**
 * This class serves as the file component for the TA
 * manager app. It provides all saving and loading 
 * services for the application.
 * 
 * @author Richard McKenna
 */
public class CourseSiteFiles implements AppFileComponent {
    // THIS IS THE APP ITSELF
    CourseSiteManagerApp app;
    
    //constants related to course editor tab
    static final String JSON_COURSE_SUBJECT = "subject";
    static final String JSON_COURSE_SUBJECT_NUMBER = "subject_number";
    static final String JSON_COURSE_SEMESTER = "semester";
    static final String JSON_COURSE_YEAR = "year";
    static final String JSON_COURSE_TITLE = "title";
    static final String JSON_COURSE_INSTRUCTOR = "instructor_name";
    static final String JSON_COURSE_INSTRUCTOR_LINK = "instructor_home";
    static final String JSON_COURSE_EXPORT_PATH = "export_path";
    static final String JSON_COURSE_TEMPLATE_PATH = "template_path";
    static final String JSON_COURSE_BANNER_PATH = "banner_path";
    static final String JSON_COURSE_LEFT_PATH = "left_path";
    static final String JSON_COURSE_RIGHT_PATH = "right_path";
    static final String JSON_COURSE_CSS_PATH = "css_path";
    
    static final String JSON_COURSE_SUBJECT_LIST = "subject_list";
    static final String JSON_COURSE_NUMBER_LIST = "subject_number_list";
    static final String JSON_COURSE_SEMESTER_LIST = "semester_list";
    static final String JSON_COURSE_YEAR_LIST = "year_list";
    
    static final String JSON_TEMPLATE_FILES = "template_files";
    static final String JSON_FILE_PATH = "file_path";
    static final String JSON_JS_FILE_PATH = "js_path";
    static final String JSON_FILE_USE = "in_use";
    static final String JSON_NAVBAR_TITLE = "nav_title";
    
    
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_EMAIL = "email";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_GRAD_TAS = "grad_tas";
    
    //constants related to recitation tab
    static final String JSON_SECTION = "section";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_LOCATION = "location";
    static final String JSON_TA_ONE = "ta1";
    static final String JSON_TA_TWO = "ta2";
    static final String JSON_RECITATIONS = "recitations";
    
    //constants related to schedule tab
    static final String JSON_TYPE = "type";
    static final String JSON_TITLE = "title";
    static final String JSON_TOPIC = "topic";
    static final String JSON_CRITERIA = "criteria";
    static final String JSON_LINK = "link";
    static final String JSON_DATE = "date";
    static final String JSON_START_DATE = "start_date";
    static final String JSON_END_DATE = "end_date";
    static final String JSON_SCHEDULES = "schedules";
    
    //constants related to project tab
    static final String JSON_COLOR = "color";
    static final String JSON_TEXT_COLOR = "text_color";
    static final String JSON_FIRST_NAME = "first_name";
    static final String JSON_LAST_NAME = "last_name";
    static final String JSON_TEAM = "team";
    static final String JSON_ROLE = "role";
    static final String JSON_TEAMS = "teams";
    static final String JSON_STUDENTS = "students";
    
    static final String JSON_COURSE_DETAILS = "course_details";
    static final String JSON_TA_TAB = "ta_data";
    static final String JSON_RECITATION_TAB = "recitation_data";
    static final String JSON_SCHEDULE_TAB = "schedule_data";
    static final String JSON_PROJECT_TAB = "project_data";
    
    public CourseSiteFiles(CourseSiteManagerApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        JsonObject json = loadJSONFile(filePath);
        app.getWorkspaceComponent().resetWorkspace();
        loadCourseDetailsTab(json.getJsonArray(JSON_COURSE_DETAILS));
        loadTADataTab(((CourseSiteData)data).getTAData(),json.getJsonArray(JSON_TA_TAB));
        loadRecitationTab(((CourseSiteData)data).getRecitationData(),json.getJsonArray(JSON_RECITATION_TAB));
        loadScheduleTab(((CourseSiteData)data).getScheduleData(),json.getJsonArray(JSON_SCHEDULE_TAB));
        loadProjectTab(((CourseSiteData)data).getProjectData(),json.getJsonArray(JSON_PROJECT_TAB));
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).initializeFields();   
    }
      
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        //testSaving(filePath);
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                                         .add(JSON_COURSE_DETAILS,Json.createArrayBuilder().add(saveCourseDetailsTab((CourseSiteWorkspace)app.getWorkspaceComponent())).build())
                                         .add(JSON_TA_TAB, Json.createArrayBuilder().add(saveTADataTab((CourseSiteData)data)).build())
                                         .add(JSON_RECITATION_TAB, Json.createArrayBuilder().add(saveRecitationTab((CourseSiteData)data)).build())
                                         .add(JSON_SCHEDULE_TAB,Json.createArrayBuilder().add(saveScheduleTab((CourseSiteData)data)).build())
                                         .add(JSON_PROJECT_TAB,Json.createArrayBuilder().add(saveProjectTab((CourseSiteData)data)).build()).build();
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    public void testSaving(String filePath)throws IOException{
        CourseSiteData data = (CourseSiteData)app.getDataComponent();
        TestSave test = new TestSave(app,((CourseSiteWorkspace)app.getWorkspaceComponent()).getCourseEditorTab(),data.getTAData(),data.getRecitationData(),data.getScheduleData(),data.getProjectData());
        test.generateTestDataOne();
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                                         .add(JSON_COURSE_DETAILS,Json.createArrayBuilder().add(saveCourseDetailsTab((CourseSiteWorkspace)app.getWorkspaceComponent())).build())
                                         .add(JSON_TA_TAB, Json.createArrayBuilder().add(saveTADataTab((CourseSiteData)data)).build())
                                         .add(JSON_RECITATION_TAB, Json.createArrayBuilder().add(saveRecitationTab((CourseSiteData)data)).build())
                                         .add(JSON_SCHEDULE_TAB,Json.createArrayBuilder().add(saveScheduleTab((CourseSiteData)data)).build())
                                         .add(JSON_PROJECT_TAB,Json.createArrayBuilder().add(saveProjectTab((CourseSiteData)data)).build()).build();
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
        OutputStream os = new FileOutputStream("C:\\Users\\cool7\\Desktop\\SiteSaveTest.json");
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter("C:\\Users\\cool7\\Desktop\\SiteSaveTest.json");
	pw.write(prettyPrinted);
	pw.close();
    }
    
    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        CourseEditorTab tab = ((CourseSiteWorkspace)app.getWorkspaceComponent()).getCourseEditorTab();
        
        filePath = tab.getExportFile().getAbsolutePath();
        
        File sourceFile = tab.getTemplateFilePath();
        if(sourceFile == null){
            return;
        }
        String sourcePath = sourceFile.getAbsolutePath();
        copyFolder(sourcePath,filePath);
        File jsFolder = new File(filePath + "\\js");
        File cssFolder = new File(filePath + "\\css");
        File imgFolder = new File(filePath + "\\img");
        if(!jsFolder.exists())
            jsFolder.mkdirs();
        if(!cssFolder.exists())
            cssFolder.mkdirs();
        if(!imgFolder.exists())
            imgFolder.mkdirs();
        
        if(!tab.getBannerImagePath().equals("")){
            File bannerImage = new File(tab.getBannerImagePath());
            File copiedImage = new File(filePath + "\\img\\"+ bannerImage.getName());
            if(!copiedImage.exists())
                Files.copy(bannerImage.toPath(),copiedImage.toPath());
        }
        
        if(!tab.getLeftImagePath().equals("")){
            File leftImage = new File(tab.getLeftImagePath());
            File copiedImage = new File(filePath +"\\img\\" + leftImage.getName());
            if(!copiedImage.exists())
                Files.copy(leftImage.toPath(),copiedImage.toPath());
        }
        
        if(!tab.getRightImagePath().equals("")){
            File rightImage = new File(tab.getRightImagePath());
            File copiedImage = new File(filePath +"\\img\\" + rightImage.getName());
            if(!copiedImage.exists())
                Files.copy(rightImage.toPath(),copiedImage.toPath());
        }
        
        if(tab.getStylesheets().getSelectionModel().getSelectedItem() != null){
            File cssFile = new File(tab.getStylesheets().getSelectionModel().getSelectedItem().getAbsolutePath());
            File copiedCSS = new File(filePath+"\\css\\"+cssFile.getName());
            if(!copiedCSS.exists())
                Files.copy(cssFile.toPath(),copiedCSS.toPath());
        }
        
        exportTAData(((CourseSiteData)data),filePath+"\\js\\OfficeHoursGridData.json");
        exportRecitationData(((CourseSiteData)data),filePath+"\\js\\RecitationsData.json");
        exportProjectData(((CourseSiteData)data),filePath+"\\js");
        exportOtherProjectData(((CourseSiteData)data),filePath+"\\js");
        exportScheduleData(((CourseSiteData)data),filePath+"\\js\\\\ScheduleData.json");
        generateHTMLAndJS(tab,filePath);
        exportCourseDetails(((CourseSiteWorkspace)app.getWorkspaceComponent()),filePath+"\\js\\CourseDetails.json");
        AppMessageDialogSingleton.getSingleton().show(props.getProperty(AppPropertyType.EXPORT_COMPLETED_TITLE),props.getProperty(AppPropertyType.EXPORT_COMPLETED_MESSAGE));
    }
    
    public void generateHTMLAndJS(CourseEditorTab tab, String path) throws IOException{
        ObservableList<TemplateFile> files = tab.getTemplateFiles();
        for(TemplateFile f: files){
            if(!f.getBoolean())
                continue;
            File siteFile = new File(f.getFullSiteFileName());
            File target = new File(path+"\\"+f.getSiteFileName());
            Files.copy(siteFile.toPath(),target.toPath(),StandardCopyOption.REPLACE_EXISTING);
            File jsFile = new File(f.getFullJSFileName());
            File jsTarget = new File(path+"\\js\\"+jsFile.getName());
            Files.copy(jsFile.toPath(),jsTarget.toPath(),StandardCopyOption.REPLACE_EXISTING);
        }
        String upperPath = System.getProperty("user.dir") + "\\work\\js\\jquery.js";
        File pathOut = new File(upperPath);
        File pathIn = new File(path + "\\js\\jquery.js");
        Files.copy(pathOut.toPath(),pathIn.toPath(),StandardCopyOption.REPLACE_EXISTING);
        
    }
    
    public void copyFolder(String copyFrom, String copyTo) throws IOException
    {
        handleFolderCopy(copyFrom,copyTo);    
    }
    
    public void handleFolderCopy(String folderPath, String otherFolderPath) throws IOException
    {
        File folder = new File(folderPath);
        File otherFolder = new File(otherFolderPath);
        if(!otherFolder.exists())
            otherFolder.mkdirs();
        File[] directory = folder.listFiles();
        for(int i = 0; i < directory.length;i++)
        {
            if(!directory[i].isDirectory() && directory[i].getName().split("\\.")[1].equals("html")||!directory[i].isDirectory() && directory[i].getName().split("\\.")[1].equals("js")||!directory[i].isDirectory() && directory[i].getName().split("\\.")[1].equals("json"))
                continue;
            if(directory[i].isDirectory())
                handleFolderCopy(folderPath+"\\"+directory[i].getName(),otherFolderPath+"\\"+directory[i].getName());
            else
            {
                File notFolder = new File(otherFolderPath + "\\" + directory[i].getName());
                Files.copy(directory[i].toPath(),notFolder.toPath(),StandardCopyOption.REPLACE_EXISTING);
            }        
        }
    }
    
    public JsonObjectBuilder saveCourseDetailsTab(CourseSiteWorkspace workspace){
        
        CourseEditorTab tab = workspace.getCourseEditorTab();
        
        JsonObjectBuilder details = Json.createObjectBuilder();
        String subject = tab.getSubjectChoices().getSelectionModel().getSelectedItem();
        if(subject == null)
            subject = "";
        details.add(JSON_COURSE_SUBJECT,subject);
        Integer subjectNumber = tab.getSubjectNumber().getSelectionModel().getSelectedItem();
        if(subjectNumber == null)
            subjectNumber = -1;
        details.add(JSON_COURSE_SUBJECT_NUMBER,subjectNumber);
        String semester = tab.getSemesterChoice().getSelectionModel().getSelectedItem();
        if(semester == null)
            semester = "";
        details.add(JSON_COURSE_SEMESTER,semester);
        String year = tab.getYearChoice().getSelectionModel().getSelectedItem();
        if(year == null)
            year = "";
        details.add(JSON_COURSE_YEAR, year)
               .add(JSON_COURSE_TITLE,tab.getTitleField().getText())
               .add(JSON_COURSE_INSTRUCTOR,tab.getInstructorNameField().getText())
               .add(JSON_COURSE_INSTRUCTOR_LINK,tab.getInstructorHomeTextField().getText());
        File exportFile = tab.getExportFile();
        if(exportFile == null){
            details.add(JSON_COURSE_EXPORT_PATH,"");
        }
        else
            details.add(JSON_COURSE_EXPORT_PATH,exportFile.getAbsolutePath());
         
        File templateFilePath = tab.getTemplateFilePath();
        if(templateFilePath == null){
            details.add(JSON_COURSE_TEMPLATE_PATH,"");
        }
        else
           details.add(JSON_COURSE_TEMPLATE_PATH,tab.getTemplateFilePath().getAbsolutePath());
        
        details.add(JSON_COURSE_BANNER_PATH,tab.getBannerImagePath())
               .add(JSON_COURSE_LEFT_PATH,tab.getLeftImagePath())
               .add(JSON_COURSE_RIGHT_PATH,tab.getRightImagePath());
        File cssPath = tab.getStylesheets().getSelectionModel().getSelectedItem();
        if(cssPath == null){
            details.add(JSON_COURSE_CSS_PATH,"");
        }
        else
            details.add(JSON_COURSE_CSS_PATH,cssPath.getAbsolutePath());
        
        JsonArrayBuilder filesArrayBuilder = Json.createArrayBuilder();
        ObservableList<TemplateFile> files = tab.getTemplateFiles();
        for(TemplateFile f: files){
            JsonObject file = Json.createObjectBuilder()
                                  .add(JSON_FILE_PATH,f.getFullSiteFileName())
                                  .add(JSON_JS_FILE_PATH,f.getFullJSFileName())
                                  .add(JSON_FILE_USE,f.getBoolean())
                                  .add(JSON_NAVBAR_TITLE,f.getHeader()).build();
            filesArrayBuilder.add(file);
        }
                
        details.add(JSON_TEMPLATE_FILES,filesArrayBuilder);
        return details;
    }
    
    
    
    public JsonObjectBuilder saveTADataTab(CourseSiteData data){
        
        TADataTabData dataManager = data.getTAData();
        
	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {
            if(ta.getBoolean()){
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL,ta.getEmail()).build();
                taArrayBuilder.add(taJson);
            }
	}
	JsonArray undergradTAsArray = taArrayBuilder.build();
        
        
        JsonArrayBuilder gradTAArrayBuilder = Json.createArrayBuilder();
	for (TeachingAssistant ta : tas) {
            if(!ta.getBoolean()){
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL,ta.getEmail()).build();
                gradTAArrayBuilder.add(taJson);
            }
	}
	JsonArray gradTAsArray = gradTAArrayBuilder.build();
	// NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
                    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        
        JsonObjectBuilder jsonHolder = Json.createObjectBuilder();
        jsonHolder.add(JSON_START_HOUR, "" + dataManager.getStartHour())
                  .add(JSON_END_HOUR, "" + dataManager.getEndHour())
                  .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                  .add(JSON_GRAD_TAS,gradTAsArray)
                  .add(JSON_OFFICE_HOURS, timeSlotsArray);
        return jsonHolder;
    }
    
    public JsonObjectBuilder saveRecitationTab(CourseSiteData dataComponent){
        RecitationData data = dataComponent.getRecitationData();
        
        
        JsonObjectBuilder recitationHolder = Json.createObjectBuilder();
        JsonArrayBuilder recitationsArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = data.getRecitations();
        for(Recitation r: recitations){
            JsonObject recitationJson = Json.createObjectBuilder()
                                            .add(JSON_SECTION,r.getSection())
                                            .add(JSON_INSTRUCTOR, r.getInstructor())
                                            .add(JSON_DAY, r.getDay())
                                            .add(JSON_LOCATION, r.getLocation())
                                            .add(JSON_TA_ONE,r.getTaOne())
                                            .add(JSON_TA_TWO,r.getTaTwo()).build();
            recitationsArrayBuilder.add(recitationJson);     
        }
        recitationHolder.add(JSON_RECITATIONS,recitationsArrayBuilder);
        return recitationHolder;
        
    }
    
    public JsonObjectBuilder saveScheduleTab(CourseSiteData dataComponent){
        ScheduleData data = dataComponent.getScheduleData();
       
        JsonObjectBuilder scheduleDataHolder = Json.createObjectBuilder();
        JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();
        ObservableList<ScheduleEvent> schedules = data.getSchedules();
        for(ScheduleEvent s: schedules){
            JsonObject scheduleJson = Json.createObjectBuilder()
                                          .add(JSON_TYPE, s.getType())
                                          .add(JSON_TITLE,s.getTitle())
                                          .add(JSON_TOPIC,s.getTopic())
                                          .add(JSON_TIME,s.getTime())
                                          .add(JSON_CRITERIA,s.getCriteria())
                                          .add(JSON_LINK,s.getLink())
                                          .add(JSON_DATE,s.getDate()).build();
            scheduleArrayBuilder.add(scheduleJson);
        }
        
        scheduleDataHolder.add(JSON_START_DATE,data.getStartDate().toString());
        scheduleDataHolder.add(JSON_END_DATE,data.getEndDate().toString());
        scheduleDataHolder.add(JSON_SCHEDULES,scheduleArrayBuilder);
        
        return scheduleDataHolder;
        
    }
    
    public JsonObjectBuilder saveProjectTab(CourseSiteData dataComponent){
        ProjectData data = dataComponent.getProjectData();

        JsonObjectBuilder projectDataHolder = Json.createObjectBuilder();
        JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
        ObservableList<Team> teams = data.getTeams();
        for(Team t: teams){
            JsonObject teamJson = Json.createObjectBuilder()
                                      .add(JSON_NAME,t.getName())
                                      .add(JSON_COLOR,t.getColor())
                                      .add(JSON_TEXT_COLOR,t.getTextColor())
                                      .add(JSON_LINK,t.getLink()).build();
            teamArrayBuilder.add(teamJson);
        }
        
        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = data.getStudents();
        for(Student s: students){
            JsonObject studentJson = Json.createObjectBuilder()
                                         .add(JSON_FIRST_NAME,s.getFirstName())
                                         .add(JSON_LAST_NAME,s.getLastName())
                                         .add(JSON_TEAM,s.getTeamName())
                                         .add(JSON_ROLE,s.getRole()).build();
            studentArrayBuilder.add(studentJson);
        }
        
        projectDataHolder.add(JSON_TEAMS,teamArrayBuilder);
        projectDataHolder.add(JSON_STUDENTS,studentArrayBuilder);
        
        return projectDataHolder;
    }
    
    public void loadCourseDetailsTab(JsonArray array){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        JsonObject information = array.getJsonObject(0);
        CourseEditorTab tab = ((CourseSiteWorkspace)app.getWorkspaceComponent()).getCourseEditorTab();
        
        tab.setLoad(true);
        String chosenSubject = information.getString(JSON_COURSE_SUBJECT);
        if(!chosenSubject.equals(""))
            tab.getSubjectChoices().getSelectionModel().select(chosenSubject);
        
        Integer chosenNumber = information.getInt(JSON_COURSE_SUBJECT_NUMBER);
        if(chosenNumber != -1)
            tab.getSubjectNumber().getSelectionModel().select(chosenNumber);
        
        String chosenSemester = information.getString(JSON_COURSE_SEMESTER);
        if(!chosenSemester.equals(""))
            tab.getSemesterChoice().getSelectionModel().select(chosenSemester);
        
        String chosenYear = information.getString(JSON_COURSE_YEAR);
        if(!chosenYear.equals(""))
            tab.getYearChoice().getSelectionModel().select(chosenYear);
        
        tab.getTitleField().setText(information.getString(JSON_COURSE_TITLE));
        tab.getInstructorNameField().setText(information.getString(JSON_COURSE_INSTRUCTOR));
        tab.getInstructorHomeTextField().setText(information.getString(JSON_COURSE_INSTRUCTOR_LINK));
        String exportPath = information.getString(JSON_COURSE_EXPORT_PATH);
        if(!exportPath.equals("")){
            File chosenExport = new File(exportPath);
            if(!chosenExport.exists())
            {
               AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
               dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_EXPORT_DIRECTORY_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_EXPORT_DIRECTORY_MESSAGE.toString()));
               tab.setExportFile(null);
            }
            else
                tab.setExportFile(new File(exportPath));
        }
        else
            tab.setExportFile(null);
        
        String templatePath = information.getString(JSON_COURSE_TEMPLATE_PATH);
        if(!templatePath.equals("")){
            String templateFolderBase = System.getProperty("user.dir") + "\\work\\template";
            File templateFolderDirectory = new File(templatePath);
            File templateDirectoryItems = new File(templateFolderBase + "\\"+templateFolderDirectory.getName());
            if(tab.verifyExists(new File(templatePath))){   
                tab.setTemplateFile(new File(templatePath),true);

                JsonArray fileArray = information.getJsonArray(JSON_TEMPLATE_FILES);
                for(int i = 0; i < fileArray.size();i++){
                    JsonObject object = fileArray.getJsonObject(i);
                    String sitePath = object.getString(JSON_FILE_PATH);
                    String jsPath = object.getString(JSON_JS_FILE_PATH);
                    boolean usage = object.getBoolean(JSON_FILE_USE);
                    String title = object.getString(JSON_NAVBAR_TITLE);
                    if(!tab.verifyExists(new File(sitePath))){
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_TEMPLATE_FILE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_TEMPLATE_FILE_MESSAGE.toString()));
                        continue;
                    }
                    tab.addTemplateFile(usage,new File(jsPath), new File(sitePath),title);
                }
            }
            else if(tab.verifyExists(templateDirectoryItems)){
                tab.setTemplateFile(templateDirectoryItems,true);
                
                JsonArray fileArray = information.getJsonArray(JSON_TEMPLATE_FILES);
                for(int i = 0; i < fileArray.size();i++){
                    JsonObject object = fileArray.getJsonObject(i);
                    String sitePath = object.getString(JSON_FILE_PATH);
                    String jsPath = object.getString(JSON_JS_FILE_PATH);
                    File siteFile = new File(sitePath);
                    String siteFileName = siteFile.getName();
                    boolean usage = object.getBoolean(JSON_FILE_USE);
                    String title = object.getString(JSON_NAVBAR_TITLE);
                    if(tab.verifyExists(new File(templateDirectoryItems.getAbsolutePath() + "\\"+siteFileName))){
                        tab.addTemplateFile(usage,new File(jsPath), new File(templateDirectoryItems.getAbsolutePath() + "\\"+siteFileName),title);
                    }
                    else{
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_TEMPLATE_FILE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_TEMPLATE_FILE_MESSAGE.toString()));
                        continue;
                    }
                }
            }
            else{
               AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
               dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_TEMPLATE_DIRECTORY_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_TEMPLATE_DIRECTORY_MESSAGE.toString()));
               tab.setTemplateFile(null,true);
            }
        }
        else
            tab.setTemplateFile(null,true);
        
        
        String imageFolderBase = System.getProperty("user.dir") + "\\work\\brands";
        File bannerImage = new File(information.getString(JSON_COURSE_BANNER_PATH));
        File alternateBannerImage = new File(imageFolderBase + "\\" + bannerImage.getName());
        if(tab.verifyExists(bannerImage)){
            tab.setBannerImagePath(information.getString(JSON_COURSE_BANNER_PATH)); 
        }
        else if(tab.verifyExists(alternateBannerImage)){
            tab.setBannerImagePath(alternateBannerImage.getAbsolutePath()); 
        }
        else
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_IMAGE_FILE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_IMAGE_FILE_MESSAGE.toString()));
        }
        
        File leftImage = new File(information.getString(JSON_COURSE_LEFT_PATH));
        File alternateLeftImage = new File(imageFolderBase+"\\"+ leftImage.getName());
        if(tab.verifyExists(leftImage)){
            tab.setLeftImagePath(information.getString(JSON_COURSE_LEFT_PATH)); 
        }
        else if(tab.verifyExists(alternateLeftImage)){
            tab.setLeftImagePath(alternateLeftImage.getAbsolutePath()); 
        }
        else
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_IMAGE_FILE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_IMAGE_FILE_MESSAGE.toString()));
        }
        
        File rightImage = new File(information.getString(JSON_COURSE_RIGHT_PATH));
        File alternateRightImage = new File(imageFolderBase  + "\\" + rightImage.getName());
        
        if(tab.verifyExists(rightImage)){
            tab.setRightImagePath(information.getString(JSON_COURSE_RIGHT_PATH)); 
        }
        else if(tab.verifyExists(alternateRightImage)){
            tab.setRightImagePath(alternateRightImage.getAbsolutePath()); 
        }
        else
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_IMAGE_FILE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_IMAGE_FILE_MESSAGE.toString()));
        }
        
        String cssPath = information.getString(JSON_COURSE_CSS_PATH);
        File chosenCSSFile = new File(cssPath);    
        String cssFolderBase = System.getProperty("user.dir") + "\\work\\css";
        File cssFolderFile = new File(cssFolderBase);
        chosenCSSFile = new File(cssFolderBase + "\\"+chosenCSSFile.getName());
        if(chosenCSSFile.exists()){
            File[] cssFolder = cssFolderFile.listFiles();
            for(File f:cssFolder){
                if(f.getName().equals(chosenCSSFile.getName()))
                    tab.getStylesheets().getSelectionModel().select(f);
            }  
        }
        else{
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(CourseSiteManagerProperties.MISSING_CSS_FILE_TITLE.toString()),props.getProperty(CourseSiteManagerProperties.MISSING_CSS_FILE_MESSAGE.toString()));
            tab.getStylesheets().getSelectionModel().select(null);
        }
        
        
    }
    
    public void loadTADataTab(TADataTabData data, JsonArray array){
        // CLEAR THE OLD DATA OUT
        
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = array.getJsonObject(0);

	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        data.initHours(startHour, endHour);
        
        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA
        TADataTab workspaceComponent = ((CourseSiteWorkspace)app.getWorkspaceComponent()).getTATab();
        workspaceComponent.resetAdditionFields();
        workspaceComponent.resetComboBoxSelection();
        workspaceComponent.clearTransactionHistory();
        app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());
        

        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            data.addTA(name,email,true);
        }
        
        JsonArray jsonGradTAArray = json.getJsonArray(JSON_GRAD_TAS);
        for(int i = 0; i < jsonGradTAArray.size();i++){
            JsonObject jsonTA = jsonGradTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            data.addTA(name,email,false);
        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            data.addOfficeHoursReservation(day, time, name);
        }
    }
    
    public void loadRecitationTab(RecitationData data, JsonArray array){
        JsonObject information = array.getJsonObject(0);
        JsonArray recitations = information.getJsonArray(JSON_RECITATIONS);
        for(int i = 0; i < recitations.size();i++){
            JsonObject object = recitations.getJsonObject(i);
            String section = object.getString(JSON_SECTION);
            String instructor = object.getString(JSON_INSTRUCTOR);
            String day = object.getString(JSON_DAY);
            String location = object.getString(JSON_LOCATION);
            String taOne = object.getString(JSON_TA_ONE);
            String taTwo = object.getString(JSON_TA_TWO);
            data.addRecitation(section,instructor,day,location,taOne,taTwo);
        }
    }
    
    public void loadScheduleTab(ScheduleData data, JsonArray array){
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getScheduleTab().setUndoState(true);
        JsonObject information = array.getJsonObject(0);
        JsonArray schedules = information.getJsonArray(JSON_SCHEDULES);
        
        data.setStartDate(LocalDate.parse(information.getString(JSON_START_DATE)));
        data.setEndDate(LocalDate.parse(information.getString(JSON_END_DATE)));
        
        for(int i = 0; i < schedules.size();i++){
            JsonObject object = schedules.getJsonObject(i);
            String type = object.getString(JSON_TYPE);
            String title = object.getString(JSON_TITLE);
            String topic = object.getString(JSON_TOPIC);
            String time = object.getString(JSON_TIME);
            String criteria = object.getString(JSON_CRITERIA);
            String link = object.getString(JSON_LINK);
            LocalDate date = LocalDate.parse(object.getString(JSON_DATE));
            
            data.addEvent(type,title,topic,time,criteria,link,date);
        }
        ((CourseSiteWorkspace)app.getWorkspaceComponent()).getScheduleTab().setUndoState(false);
    }
    
    public void loadProjectTab(ProjectData data, JsonArray array){
        JsonObject information = array.getJsonObject(0);
        JsonArray teams = information.getJsonArray(JSON_TEAMS);
        JsonArray students = information.getJsonArray(JSON_STUDENTS);
        
        for(int i = 0; i < teams.size();i++){
            JsonObject object = teams.getJsonObject(i);
            String name = object.getString(JSON_NAME);
            String color = object.getString(JSON_COLOR);
            String textColor = object.getString(JSON_TEXT_COLOR);
            String link = object.getString(JSON_LINK);
            Team team = new Team(name,color,textColor,link);
            data.addTeam(team);
        }
        
        for(int i = 0; i < students.size();i++){
            JsonObject object = students.getJsonObject(i);
            String firstName = object.getString(JSON_FIRST_NAME);
            String lastName = object.getString(JSON_LAST_NAME);
            String team = object.getString(JSON_TEAM);
            String role = object.getString(JSON_ROLE);
            data.addStudent(firstName,lastName,team,role);
        }
    }
    
    public void exportTAData(CourseSiteData data, String path) throws IOException {
        
        JsonObjectBuilder informationBuilder = saveTADataTab(data);
        
        
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(informationBuilder.build());
	jsonWriter.close();

        OutputStream os = new FileOutputStream(path);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(informationBuilder.build());
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(path);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    public void exportRecitationData(CourseSiteData data, String path) throws IOException{
        JsonObject object = saveRecitationTab(data).build();
        JsonArray recitations = object.getJsonArray(JSON_RECITATIONS);
        JsonArrayBuilder formattedRecitations = Json.createArrayBuilder();
        
        for(int i  = 0; i < recitations.size();i++){
            String sectionPart = recitations.getJsonObject(i).getString(JSON_SECTION);
            String instructor = recitations.getJsonObject(i).getString(JSON_INSTRUCTOR);
            String sectionFormat = "<strong>" + sectionPart + "</strong> (" + instructor + ")";
            JsonObject toReturn = Json.createObjectBuilder().add(JSON_SECTION,sectionFormat)
                    .add("day_time",recitations.getJsonObject(i).getString(JSON_DAY))
                    .add(JSON_LOCATION,recitations.getJsonObject(i).getString(JSON_LOCATION))
                    .add("ta_1",recitations.getJsonObject(i).getString(JSON_TA_ONE))
                    .add("ta_2",recitations.getJsonObject(i).getString(JSON_TA_TWO)).build();
            formattedRecitations.add(toReturn);
        }
        JsonObject information = Json.createObjectBuilder().add(JSON_RECITATIONS,formattedRecitations).build();
        
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(information);
	jsonWriter.close();

        OutputStream os = new FileOutputStream(path);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(information);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(path);
	pw.write(prettyPrinted);
	pw.close(); 
    }
    
    public void exportProjectData(CourseSiteData data, String path) throws IOException{
        JsonObject object = saveProjectTab(data).build();
        
        JsonArray teams = object.getJsonArray(JSON_TEAMS);
        JsonArray students = object.getJsonArray(JSON_STUDENTS);
        
        JsonArrayBuilder formattedTeamsArray = Json.createArrayBuilder();
        JsonArrayBuilder formattedStudentsArray = Json.createArrayBuilder();
        
        
        for(int i = 0; i < teams.size();i++){
            JsonObjectBuilder team = Json.createObjectBuilder();
            team.add(JSON_NAME,teams.getJsonObject(i).getString(JSON_NAME));
            String teamColor = teams.getJsonObject(i).getString(JSON_COLOR);
            String red = ""+Integer.parseInt(teamColor.substring(0,2),16);
            String green = ""+Integer.parseInt(teamColor.substring(2,4),16);
            String blue = ""+Integer.parseInt(teamColor.substring(4),16);
            String textColor = "#"+teams.getJsonObject(i).getString(JSON_TEXT_COLOR);
            team.add("red",red)
                .add("green",green)
                .add("blue",blue)
                .add("text_color", textColor);
            formattedTeamsArray.add(team.build());
        }
        
        for(int i = 0; i < students.size();i++){
            JsonObjectBuilder student = Json.createObjectBuilder();
            student.add("lastName",students.getJsonObject(i).getString(JSON_LAST_NAME))
                   .add("firstName",students.getJsonObject(i).getString(JSON_FIRST_NAME))
                   .add(JSON_TEAM,students.getJsonObject(i).getString(JSON_TEAM))
                   .add(JSON_ROLE,students.getJsonObject(i).getString(JSON_ROLE));
            formattedStudentsArray.add(student.build());
        }
        
        JsonObject teamAndStudent = Json.createObjectBuilder().add(JSON_TEAMS,formattedTeamsArray)
                                                              .add(JSON_STUDENTS,formattedStudentsArray).build();
        
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(teamAndStudent);
	jsonWriter.close();

        OutputStream os = new FileOutputStream(path+"\\TeamsAndStudents.json");
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(teamAndStudent);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(path+"\\TeamsAndStudents.json");
	pw.write(prettyPrinted);
	pw.close(); 
    }
    
    public void exportOtherProjectData(CourseSiteData data, String path) throws IOException{
        JsonArrayBuilder projectsArray = Json.createArrayBuilder();
        ProjectData projectData = data.getProjectData();
        ObservableList<Team> teamList = projectData.getTeams();
        for(Team t: teamList){
            JsonObjectBuilder projectElement = Json.createObjectBuilder();
            projectElement.add(JSON_NAME,t.getName());
            ArrayList<String> studentList = t.getStudents();
            JsonArrayBuilder jsonStudents = Json.createArrayBuilder();
            for(String s:studentList)
            {
                String[] student = s.split(";");
                jsonStudents.add(student[0] + " " + student[1]);
            }
            projectElement.add(JSON_STUDENTS,jsonStudents)
                          .add(JSON_LINK,t.getLink());
            projectsArray.add(projectElement.build());
        }
        
        CourseSiteWorkspace workspace = ((CourseSiteWorkspace)app.getWorkspaceComponent());
        String semester = workspace.getCourseEditorTab().getSemesterChoice().getSelectionModel().getSelectedItem();
        if(semester == null){
            semester = "";
        }
        String year = workspace.getCourseEditorTab().getYearChoice().getSelectionModel().getSelectedItem();
        if(year == null){
            year = "";
        }
        JsonArrayBuilder workArray = Json.createArrayBuilder();
        JsonObject containerObject = Json.createObjectBuilder()
                                         .add(JSON_COURSE_SEMESTER,semester + " " + year)
                                         .add("projects",projectsArray).build();
        workArray.add(containerObject);
        JsonObject overarchingObject = Json.createObjectBuilder().add("work",workArray).build();
        
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(overarchingObject);
	jsonWriter.close();

        OutputStream os = new FileOutputStream(path+"\\ProjectsData.json");
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(overarchingObject);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(path+"\\ProjectsData.json");
	pw.write(prettyPrinted);
	pw.close(); 
    }
    
    public void exportScheduleData(CourseSiteData data, String path) throws IOException{
        ScheduleData scheduleData = data.getScheduleData();
        ObservableList<ScheduleEvent> events = scheduleData.getSchedules();
        
        JsonObjectBuilder overarchingObject =  Json.createObjectBuilder();
        JsonArrayBuilder holidayArray = Json.createArrayBuilder();
        JsonArrayBuilder lectureArray = Json.createArrayBuilder();
        JsonArrayBuilder referenceArray = Json.createArrayBuilder();
        JsonArrayBuilder recitationArray = Json.createArrayBuilder();
        JsonArrayBuilder homeworkArray = Json.createArrayBuilder();
        
        for(ScheduleEvent e: events){
            JsonObjectBuilder scheduleEvent = Json.createObjectBuilder();
            String month = ""+Integer.parseInt(e.getDate().split("-")[1]);
            String day = ""+Integer.parseInt(e.getDate().split("-")[2]);
            String title = e.getTitle();
            scheduleEvent.add("month",month)
                         .add("day",day)
                         .add("title",title);
            switch(e.getType()){
                case "Fiesta":
                case "Holiday":{
                    scheduleEvent.add("link",e.getLink());
                    holidayArray.add(scheduleEvent.build());
                    break;
                }
                case "Clase":
                case "Lecture":{
                    String link = e.getLink();
                    if(link.equals(""))
                        link = "none";
                    scheduleEvent.add("topic",e.getTopic())
                                 .add("link",link);
                    lectureArray.add(scheduleEvent.build());
                    break;
                }
                case "Referencia":
                case "Reference":{
                    String link = e.getLink();
                    if(link.equals(""))
                        link = "none";
                    scheduleEvent.add("topic",e.getTopic())
                                 .add("link", link);
                    referenceArray.add(scheduleEvent.build());
                    break;
                }
                case "Recitación":
                case "Recitation":{
                    scheduleEvent.add("topic",e.getTopic());
                    recitationArray.add(scheduleEvent.build());
                    break;
                }
                case "HW":{
                    String link = e.getLink();
                    if(link.equals(""))
                        link = "none";
                    String criteria = e.getCriteria();
                    if(criteria.equals(""))
                        criteria = "none";
                    
                    scheduleEvent.add("topic",e.getTopic())
                                 .add("link",link)
                                 .add("time",e.getTime())
                                 .add("criteria",criteria);
                    homeworkArray.add(scheduleEvent.build());
                    break;
                }
                default:break;
            }
        }
        String startMonth = ""+Integer.parseInt(scheduleData.getStartDate().toString().split("-")[1]);
        String startDay = ""+Integer.parseInt(scheduleData.getStartDate().toString().split("-")[2]);
        String endMonth = ""+Integer.parseInt(scheduleData.getEndDate().toString().split("-")[1]);
        String endDay = ""+Integer.parseInt(scheduleData.getEndDate().toString().split("-")[2]);
        overarchingObject.add("startingMondayMonth",startMonth)
                         .add("startingMondayDay",startDay)
                         .add("endingFridayMonth",endMonth)
                         .add("endingFridayDay",endDay)
                         .add("holidays",holidayArray)
                         .add("lectures",lectureArray)
                         .add("references",referenceArray)
                         .add("recitations",recitationArray)
                         .add("hws",homeworkArray);
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(overarchingObject.build());
	jsonWriter.close();

        OutputStream os = new FileOutputStream(path);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(overarchingObject.build());
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(path);
	pw.write(prettyPrinted);
	pw.close(); 
    }
    
    public void exportCourseDetails(CourseSiteWorkspace workspace,String path) throws IOException{
        CourseEditorTab tab = workspace.getCourseEditorTab();
        JsonObject information = saveCourseDetailsTab(workspace).build();
        
        JsonObjectBuilder details = Json.createObjectBuilder();
        details.add(JSON_COURSE_SUBJECT,information.getString(JSON_COURSE_SUBJECT))
               .add(JSON_COURSE_SUBJECT_NUMBER,""+information.getInt(JSON_COURSE_SUBJECT_NUMBER))
               .add(JSON_COURSE_SEMESTER,information.getString(JSON_COURSE_SEMESTER))
               .add(JSON_COURSE_YEAR,information.getString(JSON_COURSE_YEAR))
               .add(JSON_COURSE_TITLE,information.getString(JSON_COURSE_TITLE))
               .add(JSON_COURSE_INSTRUCTOR,information.getString(JSON_COURSE_INSTRUCTOR))
               .add(JSON_COURSE_INSTRUCTOR_LINK,information.getString(JSON_COURSE_INSTRUCTOR_LINK));
        ObservableList<TemplateFile> files = tab.getTemplateFiles();
        for(TemplateFile f: files){
            details.add(f.getHeader(),f.getBoolean());
        }
        
        File bannerFile = new File(tab.getBannerImagePath());
        details.add("bannerImage",bannerFile.getName());
        File leftImage =  new File(tab.getLeftImagePath());
        details.add("leftFooterImage",leftImage.getName());
        File rightImage = new File(tab.getRightImagePath());
        details.add("rightFooterImage",rightImage.getName());
        File cssFile = tab.getStylesheets().getSelectionModel().getSelectedItem();
        if(cssFile == null)
            cssFile = new File("");
        details.add("cssFile",cssFile.getName());
        
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(details.build());
	jsonWriter.close();

        OutputStream os = new FileOutputStream(path);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(details.build());
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(path);
	pw.write(prettyPrinted);
	pw.close(); 
        
    }
    
}