package course_site.test_bed;

import course_site.CourseSiteManagerApp;
import course_site.data.CourseSiteData;
import course_site.data.ProjectData;
import course_site.data.RecitationData;
import course_site.data.ScheduleData;
import course_site.data.TADataTabData;
import course_site.data.Team;
import course_site.file.CourseSiteFiles;
import course_site.workspaces.CourseEditorTab;
import course_site.workspaces.CourseSiteWorkspace;
import course_site.workspaces.TemplateFile;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

public class TestSave {
    
    CourseSiteManagerApp app;
    CourseEditorTab courseDetails;
    TADataTabData taData;
    RecitationData recitationData;
    ScheduleData scheduleData;
    ProjectData projectData;
    
    static final String JSON_COURSE_DETAILS = "course_details";
    static final String JSON_TA_TAB = "ta_data";
    static final String JSON_RECITATION_TAB = "recitation_data";
    static final String JSON_SCHEDULE_TAB = "schedule_data";
    static final String JSON_PROJECT_TAB = "project_data";
    
    public TestSave(CourseSiteManagerApp app, CourseEditorTab detailsData, TADataTabData taPart, RecitationData recitationPart, ScheduleData schedulePart, ProjectData projectPart)
    {
        courseDetails = detailsData;
        taData = taPart;
        recitationData = recitationPart;
        scheduleData = schedulePart;
        projectData = projectPart;
    }
    
    public void generateTestDataOne(){
        courseDetails.getSubjectList().add("CSE");
        courseDetails.getSubjectChoices().getSelectionModel().select(0);
        courseDetails.getSubjectNumberList().add(219);
        courseDetails.getSubjectNumber().getSelectionModel().select(0);
        courseDetails.getSemesterList().add("Fall");
        courseDetails.getSemesterChoice().getSelectionModel().select(0);
        courseDetails.getYearList().add("2017");
        courseDetails.getYearChoice().getSelectionModel().select(0);
        courseDetails.getTitleField().setText("Computer Science III");
        courseDetails.getInstructorNameField().setText("Richard Mckenna");
        courseDetails.getInstructorHomeTextField().setText("http://www.google.com");
        courseDetails.setTemplateFile(new File(""),true);
        courseDetails.setExportFile(new File(""));
        courseDetails.setBannerImagePath("C:\\Users\\cool7\\Desktop\\pagefiles\\CSE219_files\\SBUDarkRedShieldLogo.png");
        courseDetails.setLeftImagePath("C:\\Users\\cool7\\Desktop\\pagefiles\\CSE219_files\\SBUWhiteShieldLogo.png");
        courseDetails.setRightImagePath("C:\\Users\\cool7\\Desktop\\pagefiles\\CSE219_files\\SBUDarkRedShieldLogo.png");
        ObservableList<TemplateFile> files = courseDetails.getTemplateFiles();
        files.add(new TemplateFile(true,new File(""),new File(""),"main",app));
        files.add(new TemplateFile(true,new File(""),new File(""),"syllabus",app));
        files.add(new TemplateFile(true,new File(""),new File(""),"schedule",app));
        files.add(new TemplateFile(true,new File(""),new File(""),"hws",app));
        files.add(new TemplateFile(false,new File(""),new File(""),"projects",app));
        courseDetails.getCSSFiles().add(new File(""));
        courseDetails.getStylesheets().getSelectionModel().select(0);
        
        taData.addTA("testfirst","testemail",false);
        taData.addTA("testfirst1","testemail1",true);
        taData.addOfficeHoursReservation("MONDAY", "12_00pm", "testfirst");
        taData.addOfficeHoursReservation("TUESDAY", "1_00pm", "testfirst1");
        taData.addOfficeHoursReservation("WEDNESDAY", "3_00pm", "testfirst");
        taData.addOfficeHoursReservation("TUESDAY", "4_00pm", "testfirst1");
        
        recitationData.addRecitation("01", "Mckenna", "Monday 11-12pm", "New CS 1111", "testfirst", "");
        recitationData.addRecitation("02", "Banerjee", "Thursday 1-2pm", "New CS 1111", "testfirst1", "");
        recitationData.addRecitation("03", "Mckenna", "Monday 11-12pm", "New CS 1111", "testfirst", "testfirst1");
        recitationData.addRecitation("04", "Banerjee", "Monday 11-12pm", "New CS 1111", "", "");
        
        scheduleData.addEvent("holiday","Spring Break", "123", "456","789","http://www.google.com", LocalDate.now());
        scheduleData.addEvent("hw","hw1", "testing", "test","rest","http://www.google.com", LocalDate.now());
        scheduleData.addEvent("lecture","multithreading", "", "","","http://www.google.com", LocalDate.now());
        scheduleData.addEvent("references","javafx api", "", "","","http://www.google.com", LocalDate.now());
        
        projectData.addTeam(new Team("C4 Comics","ffffff","552211","http://www.google.com"));
        projectData.addTeam(new Team("Atomic Comics","235399","ffffff","http://www.google.com"));
        projectData.addStudent("Beau","Brummell","Atomic Comics","Lead Designer");
        projectData.addStudent("Jane","Doe","C4 Comics","Lead Programmer");
        projectData.addStudent("Noonian","Soong","Atomic Comics","Data Designer");
    }
    

}
