package course_site.data;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;


public class Team<E extends Comparable<E>> implements Comparable<E> {
    private final StringProperty name,color,textColor,link;
    private ArrayList<String> students;
    
    public Team(String name, String color,String textColor,String link){
        this.name = new SimpleStringProperty(name);
        this.color = new SimpleStringProperty(color);
        this.textColor = new SimpleStringProperty(textColor);
        this.link = new SimpleStringProperty(link);
        students = new ArrayList<>();
    }
    
    public String getName(){
        return name.get();
    }
    
    public String getColor(){
        return color.get();
    }
    
    public String getTextColor(){
        return textColor.get();
    }
    
    public String getLink(){
        return link.get();
    }
    
    public void setName(String name){
        this.name.set(name);
    }
    
    public void setColor(Color color){
        this.color.set(color.toString().substring(2, 8));
    }
    
    public void setTextColor(Color color){
        textColor.set(color.toString().substring(2,8));
    }
    
    public void setLink(String link){
        this.link.set(link);
    }
      
    public ArrayList<String> getStudents(){
        return students;
    }
    
    public void removeStudent(String firstName,String lastName){
        String toRemove = "";
        for(String s:students){
            String[] parts = s.split(";");
            if(parts[0].equals(firstName)&&parts[1].equals(lastName)){
                toRemove = s;
                break;
            }
        }
        if(toRemove.equals(""))
            return;
        students.remove(toRemove);
    }
    
    public void addStudent(Student student){
        students.add(student.toString());
    }
    
    public void addStudent(String firstName,String lastName,String teamName,String role){
        String roleReplace = role;
        if(roleReplace.equals(""))
            roleReplace = "^&";
        students.add(firstName+";"+lastName+";"+teamName+";"+roleReplace);
    }
    
    public void replaceStudentsListWith(ArrayList<String> newStudents){
        students = newStudents;
    }
    
    public String transcribeData(){
        return getName()+";"+getColor()+";"+getTextColor()+";"+getLink();
    }
    
    @Override
    public String toString(){
        return getName();
    }
    
    @Override
    public int compareTo(E other){
        return getName().compareTo(((Team)other).getName());
    }
}
