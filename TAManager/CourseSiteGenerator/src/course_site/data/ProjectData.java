package course_site.data;

import course_site.CourseSiteManagerApp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;

public class ProjectData {
    CourseSiteManagerApp main;
    ObservableList<Student> students;
    ObservableList<Team> teams;
    HashMap<String,Team> teamList;
    
    public ProjectData(CourseSiteManagerApp app){
        main = app;
        students = FXCollections.observableList(new ArrayList<>());
        teams = FXCollections.observableList(new ArrayList<>());
        teamList = new HashMap<>();
    }
    
    public ObservableList<Student> getStudents(){
        return students;
    }
    
    public ObservableList<Team> getTeams(){
        return teams;
    }
    
    public HashMap<String,Team> getTeamMap(){
        return teamList;
    }
    
    public Team getTeam(String name){
        return teamList.get(name);
    }
    
    public void addTeam(Team team){
        teamList.put(team.getName(),team);
        teams.add(team);
        Collections.sort(teams);
    }
    
    public void addTeam(String name,String chosenTeamColor, String chosenTextColor, String link){
        Team team = new Team(name,chosenTeamColor,chosenTextColor,link);
        teamList.put(name,team);
        teams.add(team);
        Collections.sort(teams);
    }
    
    public void removeTeam(String name){
        Team selectedTeam = teamList.get(name);
        if(selectedTeam != null){
            
            teams.remove(selectedTeam);
            teamList.remove(name);
        }
    }
    
    public Student getStudent(String firstName,String lastName){
        for(Student s:students){
            if(s.getFirstName().equals(firstName)&& s.getLastName().equals(lastName))
                return s;
        }
        return null;
    }
    
    public void addStudent(String firstName,String lastName, String teamName, String role){
        Student student = new Student(firstName, lastName, teamName, role);
        if(!teamName.equals(""))
            getTeam(teamName).addStudent(student);
        students.add(student);
        Collections.sort(students);
    }
    
    public void removeStudent(String firstName, String lastName,String teamName){
        if(!teamName.equals("")){
            Team t = teamList.get(teamName);
            t.removeStudent(firstName, lastName);
        }
        students.remove(getStudent(firstName,lastName));
    }
    
    public void removeStudent(Student s){
        if(!s.getTeamName().equals("")){
            Team t = teamList.get(s.getTeamName());
            t.removeStudent(s.getFirstName(),s.getLastName());
        }
        students.remove(s);
    }
    
    public String convertColorToHexCode(Color color){
        return color.toString().substring(2, 8);
    }
    
    public void replaceAllStudentTeamInstancesWith(String originalTeam,String newTeam){
        for(Student s:students){
            if(s.getTeamName().equals(originalTeam))
                s.setTeamName(newTeam);
        }
    }
    
    public void moveHash(String oldName,String newName){
        Team t = teamList.get(oldName);
        teamList.remove(oldName);
        teamList.put(newName, t);
    }
    
    public void resetData(){
        students.clear();
        teams.clear();
        teamList.clear();
    }
}
