
package course_site.data;

import course_site.CourseSiteManagerApp;
import course_site.CourseSiteManagerProperties;
import course_site.workspaces.CourseSiteWorkspace;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;

public class ScheduleData {
    
    CourseSiteManagerApp main;
    ObservableList<ScheduleEvent> schedules;
    ObservableList<String> eventChoices;
    LocalDate startDateDefault = LocalDate.now().plusDays(7-(LocalDate.now().getDayOfWeek().getValue()-1));
    LocalDate endDateDefault = LocalDate.now().plusDays(7-(LocalDate.now().getDayOfWeek().getValue()-5));  
    LocalDate startDate,endDate;
    
    public ScheduleData(CourseSiteManagerApp app){
        main = app;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        schedules = FXCollections.observableList(new ArrayList<>());
        eventChoices = FXCollections.observableList(props.getPropertyOptionsList(CourseSiteManagerProperties.SCHEDULE_TYPE));
        startDate = startDateDefault;
        endDate = endDateDefault;
    }
    
    public ObservableList<ScheduleEvent> getSchedules(){
        return schedules;
    }
    
    public ObservableList<String> getEventChoices(){
        return eventChoices;
    }
    
    public void resetData(){
        schedules.clear();
        startDate = startDateDefault;
        endDate = endDateDefault;
    }
    
    public void addEvent(ScheduleEvent e){
        schedules.add(e);
        Collections.sort(schedules);
        ((CourseSiteWorkspace)main.getWorkspaceComponent()).getScheduleTab().clearSelectionFields();
        ((CourseSiteWorkspace)main.getWorkspaceComponent()).getScheduleTab().getTable().refresh();
    }
    
    public void addEvent(String type,String title, String topic, String time,String criteria,String link, LocalDate date){
        ScheduleEvent event = new ScheduleEvent(type,title,topic,time,criteria,link,date);
        schedules.add(event);
        Collections.sort(schedules);
        ((CourseSiteWorkspace)main.getWorkspaceComponent()).getScheduleTab().clearSelectionFields();
        ((CourseSiteWorkspace)main.getWorkspaceComponent()).getScheduleTab().getTable().refresh();
    }
    
    public void removeEvent(ScheduleEvent e){
        if(e != null){
            schedules.remove(e);
            ((CourseSiteWorkspace)main.getWorkspaceComponent()).getScheduleTab().clearSelectionFields();
        }
    }
    
    public ScheduleEvent getEvent(String type,String title, String topic, String time,String criteria,String link, LocalDate date){
        for(ScheduleEvent e:schedules){
            if(e.getType().equals(type)&&e.getTitle().equals(title)&&e.getTopic().equals(topic)&&e.getTime().equals(time)&&e.getCriteria().equals(criteria)&&e.getLink().equals(link)&& date.toString().equals(e.getDate())){
                return e;
            }
        }
        return null;
    }
    
    public LocalDate getStartDate(){
        return startDate;
    }
    
    public LocalDate getEndDate(){
        return endDate;
    }
    
    public CourseSiteManagerApp getMain(){
        return main;
    }
    
    public void setStartDate(LocalDate date)
    {
        startDate = date;
        ((CourseSiteWorkspace)main.getWorkspaceComponent()).getScheduleTab().getChosenStartDate().setValue(date);
    }
    
    public void setEndDate(LocalDate date){
        endDate = date;
        ((CourseSiteWorkspace)main.getWorkspaceComponent()).getScheduleTab().getChosenEndDate().setValue(date);
    }
    
    public ArrayList<String> getAffectedStartHours(LocalDate date){
        ArrayList<String> removed = new ArrayList<>();
        for(ScheduleEvent s:schedules){
            if(date.compareTo(LocalDate.parse(s.getDate()))>0){
                String time = s.getTime();
                String title = s.getTitle();
                String topic = s.getTopic();
                String link = s.getLink();
                String criteria = s.getCriteria();
                removed.add(generateInformationString(s.getType(),s.getDate(),time,title,topic,link,criteria));  
            }
        }
        return removed;
    }
    
    public ArrayList<String> getAffectedEndHours(LocalDate date){
        ArrayList<String> removed = new ArrayList<>();
        for(ScheduleEvent s:schedules){
            if(date.compareTo(LocalDate.parse(s.getDate()))<0){
                String time = s.getTime();
                String title = s.getTitle();
                String topic = s.getTopic();
                String link = s.getLink();
                String criteria = s.getCriteria();
                removed.add(generateInformationString(s.getType(),s.getDate(),time,title,topic,link,criteria));
            }
        }
        return removed;
    }
    
    public boolean checkIfAffectsStart(LocalDate date){
        for(ScheduleEvent s:schedules){
            if(date.compareTo(LocalDate.parse(s.getDate()))>0)
                return true;
        }
        return false;
    }
    
    public boolean checkIfAffectsEnd(LocalDate date){
        for(ScheduleEvent s:schedules){
            if(date.compareTo(LocalDate.parse(s.getDate()))<0)
                return true;
        }
        return false;
    }
    
    public String generateInformationString(String type,String date, String time,String title,String topic,String link,String criteria){
        String toReturn = type +";"+date+";";
        if(time.equals(""))
            toReturn +="^&;";
        else 
            toReturn += time + ";";
        
        if(title.equals(""))
            toReturn +="^&;";
        else 
            toReturn += title + ";";
        
        if(topic.equals(""))
            toReturn +="^&;";
        else 
            toReturn += topic + ";";
        
        if(link.equals(""))
            toReturn +="^&;";
        else 
            toReturn += link + ";";
        
        if(criteria.equals(""))
            toReturn +="^&";
        else 
            toReturn += criteria;
        return toReturn;
    }
    
    public String[] translateInformationString(String info){
        String[] pieces = info.split(";");
        for(int i = 0;i<pieces.length;i++){
            if(pieces[i].equals("^&"))
                pieces[i] = "";
        }
        return pieces;
    }
}
