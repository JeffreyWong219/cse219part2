package course_site.data;

import course_site.workspaces.TADataTab;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 * This class represents a Teaching Assistant for the table of TAs.
 * 
 * @author Richard McKenna
 */
public class TeachingAssistant<E extends Comparable<E>> implements Comparable<E>  {
    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty name;
    private final StringProperty email;
    private final BooleanProperty isUndergraduate;
    private TADataTab tab;

    /**
     * Constructor initializes the TA name
     */
    public TeachingAssistant(String initName, String initEmail, boolean isGrad,TADataTab tab) {
        name = new SimpleStringProperty(initName);
        email = new SimpleStringProperty(initEmail);
        isUndergraduate = new SimpleBooleanProperty(isGrad);
        this.tab = tab;
        
        isUndergraduate.addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> value,Boolean wasSelected,Boolean isSelected){
                if(!tab.isButtonPressed())
                    tab.setUndo(false);
            }
        });
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES

    public boolean getBoolean(){
        return isUndergraduate.get();
    }
    
    public BooleanProperty getBooleanProperty(){
        return isUndergraduate;
    }
    
    public void setBoolean(boolean b){
        isUndergraduate.set(b);
    }
    
    public String getName() {
        return name.get();
    }

    public void setName(String initName) {
        name.set(initName);
    }

    public String getEmail()
    {
        return email.get();
    }
    
    public void setEmail(String initEmail)
    {
        email.set(initEmail);
    }
    
    
    @Override
    public int compareTo(E otherTA) {
        return getName().compareTo(((TeachingAssistant)otherTA).getName());
    }
    
    @Override
    public String toString() {
        return name.getValue();
    }
}