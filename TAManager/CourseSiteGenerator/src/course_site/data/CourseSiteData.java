/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package course_site.data;

import course_site.CourseSiteManagerApp;
import djf.components.AppDataComponent;

/**
 *
 * @author cool7
 */
public class CourseSiteData implements AppDataComponent{
    
    CourseSiteManagerApp app;
    TADataTabData taData;
    RecitationData recitationData;
    ScheduleData scheduleData;
    ProjectData projectData;
    
    public CourseSiteData(CourseSiteManagerApp app){
        this.app = app;
        taData = new TADataTabData(app);
        recitationData = new RecitationData(app,this);
        scheduleData = new ScheduleData(app);
        projectData = new ProjectData(app);
    }
    
    @Override
    public void resetData(){
        taData.resetData();
        recitationData.resetData();
        scheduleData.resetData();
        projectData.resetData();
    }
    
    public TADataTabData getTAData()
    {
        return taData;
    }
    
    public RecitationData getRecitationData(){
        return recitationData;
    }
    
    public ScheduleData getScheduleData(){
        return scheduleData;
    }
    
    public ProjectData getProjectData(){
        return projectData;
    }
    
    
    
}
