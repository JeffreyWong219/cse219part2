/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package course_site.data;

import course_site.CourseSiteManagerApp;
import course_site.workspaces.CourseSiteWorkspace;
import java.util.ArrayList;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class RecitationData {
    CourseSiteManagerApp main;
    ObservableList<Recitation> recitations;
    ObservableList<TeachingAssistant> tas;
    CourseSiteData otherData;
    
    public RecitationData(CourseSiteManagerApp app, CourseSiteData other){
        main = app;
        otherData = other;
        
        recitations = FXCollections.observableList(new ArrayList<>());
        tas = other.getTAData().getTeachingAssistants();
        
    }
    
    public void resetData(){
        recitations.clear();
        tas = otherData.getTAData().getTeachingAssistants();
    }
    
    public ObservableList<Recitation> getRecitations(){
        return recitations;
    }
    
    public ObservableList<TeachingAssistant> getTeachingAssistants(){
        return tas;
    }
    
    public void addRecitation(String section, String instructor, String day, String location, String taOne, String taTwo){
        Recitation recit = new Recitation(section,instructor,day,location,taOne,taTwo);
        recitations.add(recit);
        Collections.sort(recitations);
    }
    
    public void removeRecitation(Recitation target){
        if(target != null){
            recitations.remove(target);
            ((CourseSiteWorkspace)main.getWorkspaceComponent()).getRecitationTab().clearTextFields();
        }
    }
    
    public boolean containsSection(String section){
        for(Recitation r: recitations){
            if(r.getSection().equals(section))
                return true;
        }
        return false;
    }
    
    public boolean containsTime(String time){
        for(Recitation r: recitations){
            if(r.getDay().equals(time)){
                return true;
            }
        }
        return false;
    }
    
    public boolean containsTA(String name){
        return otherData.getTAData().containsTA(name);
    }
    
    public Recitation getRecitation(String section){
        for(Recitation r: recitations){
            if(r.getSection().equals(section)){
                return r;
            }
        }
        return null;   
    }
    
    public TeachingAssistant getTA(String taName){
        return otherData.getTAData().getTA(taName);
    }
    
    public void replaceAllInstancesOfWith(String taName,String input){
        for(Recitation r:recitations){
            if(r.getTaOne().equals(taName)){
                r.setTAOne(input);
            }
            if(r.getTaTwo().equals(taName)){
                r.setTATwo(input);
            }
        }
        Collections.sort(recitations);
        ((CourseSiteWorkspace)main.getWorkspaceComponent()).getRecitationTab().getTable().refresh();
    }

}
