package course_site.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Student<E extends Comparable<E>> implements Comparable<E> {
    
    private final StringProperty firstName,lastName,teamName,role;
    
    public Student(String first, String last, String teamName,String role){
        firstName = new SimpleStringProperty(first);
        lastName = new SimpleStringProperty(last);
        this.teamName = new SimpleStringProperty(teamName);
        this.role = new SimpleStringProperty(role);
    }
    
    public String getFirstName(){
        return firstName.get();
    }
    
    public String getLastName(){
        return lastName.get();
    }
    
    public String getTeamName(){
        return teamName.get();
    }
    
    public String getRole(){
        return role.get();
    }
    
    public void setFirstName(String first){
        firstName.set(first);
    }
    
    public void setLastName(String last){
        lastName.set(last);
    }
    
    public void setTeamName(String name){
        teamName.set(name);
    }
    
    public void setRole(String role){
        this.role.set(role);
    }
    
    @Override
    public String toString(){
        String studentTeam = getTeamName();
        if(studentTeam.equals(""))
            studentTeam = "^&";
        String studentRole = getRole();
        if(studentRole.equals(""))
            studentRole = "^&";
        return getFirstName()+";"+getLastName()+";"+studentTeam+";"+studentRole;
    }
    
    @Override
    public int compareTo(E other){
        return getFirstName().compareTo(((Student)other).getFirstName());
    }
            
}
