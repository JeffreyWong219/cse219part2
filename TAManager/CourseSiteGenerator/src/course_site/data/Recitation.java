/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package course_site.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Recitation<E extends Comparable<E>> implements Comparable<E>{
    private final StringProperty section;
    private final StringProperty instructor;
    private final StringProperty day;
    private final StringProperty location;
    private final StringProperty taOne;
    private final StringProperty taTwo;
    
    public Recitation(String sect, String instr, String day, String location, String taOne, String taTwo)
    {
        section = new SimpleStringProperty(sect);
        instructor = new SimpleStringProperty(instr);
        this.day = new SimpleStringProperty(day);
        this.location = new SimpleStringProperty(location);
        this.taOne = new SimpleStringProperty(taOne);
        this.taTwo = new SimpleStringProperty(taTwo);
    }
    
    public String getSection(){
        return section.get();
    }
    
    public String getInstructor(){
        return instructor.get();
    }
    
    public String getDay(){
        return day.get();
    }
    
    public String getLocation(){
        return location.get();
    }
    
    public String getTaOne(){
        return taOne.get();
    }
    
    public String getTaTwo(){
        return taTwo.get();
    }
    
    public void setSection(String s){
       section.set(s);
    }
    
    public void setInstructor(String s){
        instructor.set(s);
    }
    
    public void setDay(String s){
        day.set(s);
    }
    
    public void setLocation(String s){
        location.set(s);
    }
    
    public void setTAOne(String s){
        taOne.set(s);
    }
    
    public void setTATwo(String s){
        taTwo.set(s);
    }
    
    public String transcribeData(){
        String firstTA = getTaOne();
        if(taOne.get().equals("")){
            firstTA = "?";
        }
        String secondTA = getTaTwo();
        if(taTwo.get().equals("")){
            secondTA = "?";
        }
        return ""+getSection()+";"+getInstructor()+";"+getDay()+";"+getLocation()+";"+firstTA+";"+secondTA;
    }
    
    @Override
    public String toString(){
        return getSection();
    }
    
    @Override
    public int compareTo(E other){
        return getSection().compareTo(((Recitation)other).getSection());
    }
    
}
