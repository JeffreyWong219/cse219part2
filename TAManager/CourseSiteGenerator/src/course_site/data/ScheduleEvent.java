package course_site.data;

import java.time.LocalDate;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ScheduleEvent<E extends Comparable<E>> implements Comparable<E> {
    private final StringProperty type,title,topic,time,criteria,link,date;
    
    public ScheduleEvent(String type,String title,String topic,String time,String criteria,String link, LocalDate date){
        this.type = new SimpleStringProperty(type);
        this.title = new SimpleStringProperty(title);
        this.topic = new SimpleStringProperty(topic);
        this.time = new SimpleStringProperty(time);
        this.criteria = new SimpleStringProperty(criteria);
        this.link = new SimpleStringProperty(link);
        this.date = new SimpleStringProperty(date.toString());
    }
    
    public String getType(){
        return type.get();
    }
    
    public String getTitle(){
        return title.get();
    }
    
    public String getTopic(){
        return topic.get();
    }
    
    public String getTime(){
        return time.get();
    }
    
    public String getCriteria(){
        return criteria.get();
    }
    
    public String getLink(){
        return link.get();
    }
    
    public String getDate(){
        return date.get();
    }
    
    public void setType(String newType){
        type.set(newType);
    }
    
    public void setTitle(String newTitle){
        title.set(newTitle);
    }
    
    public void setTopic(String newTopic){
        topic.set(newTopic);
    }
    
    public void setTime(String newTime){
        time.set(newTime);
    }
    
    public void setCriteria(String newCriteria){
        criteria.set(newCriteria);
    }
    
    public void setLink(String newLink){
        link.set(newLink);
    }
    
    public void setDate(LocalDate date){
        this.date.set(date.toString());
    }
       
    @Override
    public String toString(){
        return getType()+":"+getTitle();
    }
    
    @Override
    public int compareTo(E other){
        return this.getDate().compareTo(((ScheduleEvent)other).getDate());
    }
}
