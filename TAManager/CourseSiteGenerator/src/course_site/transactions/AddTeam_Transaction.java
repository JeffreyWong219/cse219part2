

package course_site.transactions;

import course_site.data.ProjectData;
import jtps.jTPS_Transaction;

public class AddTeam_Transaction implements jTPS_Transaction{
    
    String[] information;
    ProjectData data;
    
    static final int NAME = 0;
    static final int TEAM_COLOR = 1;
    static final int TEXT_COLOR = 2;
    static final int LINK = 3;
    
    public AddTeam_Transaction(String information,ProjectData data){
        this.data = data;
        this.information = information.split(";");
    }
    
    @Override
    public void doTransaction(){
        data.addTeam(information[NAME],information[TEAM_COLOR],information[TEXT_COLOR], information[LINK]);
    }
    
    @Override
    public void undoTransaction(){
        data.removeTeam(information[NAME]);
    }
    
}
