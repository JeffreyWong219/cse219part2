
package course_site.transactions;
import java.util.ArrayList;
import jtps.jTPS_Transaction;
import course_site.data.TADataTabData;
import course_site.workspaces.TADataTab;
/**
 *
 * @author cool7
 */
public class UpdateOfficeHours_Transaction implements jTPS_Transaction{
    
    private String oldStart;// all of these in pm/am standard
    private String oldEnd;
    private String newStart;
    private String newEnd;
    private TADataTabData data;
    private TADataTab workspace;
    private ArrayList<String> originalHours;
    private ArrayList<String> newHours;
    
    
    public UpdateOfficeHours_Transaction(String previousStartHour, String previousEndHour,String newStartHour,String newEndHour, TADataTabData data, TADataTab workspace)
    {
        oldStart = previousStartHour;
        oldEnd = previousEndHour;
        newStart = newStartHour;
        newEnd = newEndHour;
        this.data = data;
        this.workspace = workspace;
        originalHours = data.retrieveAllCellValues();
        newHours = data.realignHours(newStart,newEnd);
    }
    
    @Override
    public void doTransaction(){
        data.initHours(data.getMilitaryHour(newStart),data.getMilitaryHour(newEnd));
        workspace.resetWorkspace();
        workspace.reloadWorkspace();
        data.migrateHours(newHours);
        workspace.resetComboBoxSelection();
    }
    
    @Override
    public void undoTransaction(){
        data.initHours(data.getMilitaryHour(oldStart),data.getMilitaryHour(oldEnd));
        workspace.resetWorkspace();
        workspace.reloadWorkspace();
        data.migrateHours(originalHours);
        workspace.resetComboBoxSelection();
    }
}
