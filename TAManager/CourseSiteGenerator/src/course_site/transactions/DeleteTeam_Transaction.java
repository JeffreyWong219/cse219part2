
package course_site.transactions;

import course_site.data.ProjectData;
import course_site.data.Student;
import course_site.data.Team;
import course_site.workspaces.ProjectTab;
import java.util.ArrayList;
import jtps.jTPS_Transaction;

public class DeleteTeam_Transaction implements jTPS_Transaction{
    
    String name,color,textColor,link;
    ArrayList<String> students;
    ProjectData data;
    ProjectTab tab;
    
    public DeleteTeam_Transaction(String name,String color,String textColor,String link,ArrayList<String> removedStudents,ProjectData data,ProjectTab tab){
        this.name = name;
        this.color = color;
        this.textColor = textColor;
        this.link = link;
        students = removedStudents;
        this.data = data;
        this.tab = tab;
    }
    
    @Override
    public void doTransaction(){
        data.removeTeam(name);
        data.replaceAllStudentTeamInstancesWith(name,"");
        tab.getStudentTable().refresh();
        tab.clearStudentSelectionFields();
    }
    
    @Override
    public void undoTransaction(){
        data.addTeam(name,color,textColor,link);
        Team t = data.getTeam(name);
        for(String s:students){
            String[] information = s.split(";");
            t.addStudent(information[0], information[1], information[2], information[3]);
            Student student = data.getStudent(information[0],information[1]);
            student.setTeamName(name);
        }
        tab.getStudentTable().refresh();
        tab.clearStudentSelectionFields();
    }
}
