
package course_site.transactions;

import course_site.data.ProjectData;
import course_site.data.Student;
import course_site.data.Team;
import course_site.workspaces.ProjectTab;
import java.util.Collections;
import jtps.jTPS_Transaction;

public class UpdateStudent_Transaction implements jTPS_Transaction{
    String[] oldInformation,newInformation;
    ProjectData data;
    ProjectTab tab;
    String oldStudentString,newStudentString;
    boolean differentTeam;
    
    static final int FIRST_NAME = 0;
    static final int LAST_NAME = 1;
    static final int TEAM_NAME = 2;
    static final int ROLE = 3;
    
    public UpdateStudent_Transaction(String oldInfo,String newInfo,ProjectData data,ProjectTab tab,boolean differentTeam){
        oldStudentString = oldInfo;
        newStudentString = newInfo;
        oldInformation = oldInfo.split(";");
        newInformation = newInfo.split(";");
        this.data = data;
        this.tab = tab;
        this.differentTeam = differentTeam;
    }
    
    @Override
    public void doTransaction(){
        Student s = data.getStudent(oldInformation[FIRST_NAME], oldInformation[LAST_NAME]);
        s.setFirstName(newInformation[FIRST_NAME]);
        s.setLastName(newInformation[LAST_NAME]);
        if(newInformation[TEAM_NAME].equals("^&"))
            s.setTeamName("");
        else
            s.setTeamName(newInformation[TEAM_NAME]);
        if(newInformation[ROLE].equals("^&"))
            s.setRole("");
        else
            s.setRole(newInformation[ROLE]);
        if(differentTeam){
            Team oldT = data.getTeam(oldInformation[TEAM_NAME]);
            Team newT = data.getTeam(newInformation[TEAM_NAME]);
            if(oldT != null){
                oldT.removeStudent(oldInformation[FIRST_NAME],oldInformation[LAST_NAME]);
            }
            if(newT != null){
                newT.addStudent(newInformation[FIRST_NAME],newInformation[LAST_NAME],newInformation[TEAM_NAME],newInformation[ROLE]);
            }
        }
        else{
            Team oldT = data.getTeam(oldInformation[TEAM_NAME]);
            if(oldT != null){
                oldT.removeStudent(oldInformation[FIRST_NAME],oldInformation[LAST_NAME]);
                oldT.addStudent(newInformation[FIRST_NAME],newInformation[LAST_NAME],newInformation[TEAM_NAME],newInformation[ROLE]);
            }
        }
        
        Collections.sort(data.getStudents());
        tab.getStudentTable().refresh();
        tab.getController().handleStudentTableSelection();
    }
    
    @Override
    public void undoTransaction(){
        Student s = data.getStudent(newInformation[FIRST_NAME], newInformation[LAST_NAME]);
        s.setFirstName(oldInformation[FIRST_NAME]);
        s.setLastName(oldInformation[LAST_NAME]);
        if(oldInformation[TEAM_NAME].equals("^&"))
            s.setTeamName("");
        else
            s.setTeamName(oldInformation[TEAM_NAME]);
        if(oldInformation[ROLE].equals("^&"))
            s.setRole("");
        else
            s.setRole(oldInformation[ROLE]);
        if(differentTeam){
            Team oldT = data.getTeam(newInformation[TEAM_NAME]);
            Team newT = data.getTeam(oldInformation[TEAM_NAME]);
            if(oldT != null){
                oldT.removeStudent(newInformation[FIRST_NAME],newInformation[LAST_NAME]);
            }
            if(newT != null){
                newT.addStudent(oldInformation[FIRST_NAME],oldInformation[LAST_NAME],oldInformation[TEAM_NAME],oldInformation[ROLE]);
            }
        }
        else{
            Team oldT = data.getTeam(newInformation[TEAM_NAME]);
            if(oldT != null){
                oldT.removeStudent(newInformation[FIRST_NAME],newInformation[LAST_NAME]);
                oldT.addStudent(oldInformation[FIRST_NAME],oldInformation[LAST_NAME],oldInformation[TEAM_NAME],oldInformation[ROLE]);
            }
        }
        
        Collections.sort(data.getStudents());
        tab.getStudentTable().refresh();
        tab.getController().handleStudentTableSelection();
    }
}
