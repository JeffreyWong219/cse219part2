
package course_site.transactions;

import course_site.workspaces.CourseEditorTab;
import java.io.File;
import jtps.jTPS_Transaction;


public class ChangeTemplateDirectory_Transaction implements jTPS_Transaction{
    
    String oldPath;
    String newPath;
    CourseEditorTab tab;
    
    public ChangeTemplateDirectory_Transaction(String oldTemplate, String newTemplate, CourseEditorTab workspace){
        oldPath = oldTemplate;
        newPath = newTemplate;
        tab = workspace;
    }
    
    @Override
    public void doTransaction(){
        tab.setTemplateFile(new File(newPath),false);
    }
    
    @Override
    public void undoTransaction(){
        if(oldPath == null)
            tab.setTemplateFile(null,false);
        else
            tab.setTemplateFile(new File(oldPath),false);
    }
    
}
