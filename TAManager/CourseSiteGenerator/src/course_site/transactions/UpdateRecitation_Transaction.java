
package course_site.transactions;

import course_site.data.Recitation;
import course_site.data.RecitationData;
import course_site.workspaces.RecitationTab;
import java.util.Collections;
import jtps.jTPS_Transaction;

public class UpdateRecitation_Transaction implements jTPS_Transaction{
    
    String[] newData;
    String[] oldData;
    RecitationData data;
    RecitationTab tab;
    
    static final int SECTION = 0;
    static final int INSTRUCTOR = 1;
    static final int DAY = 2;
    static final int LOCATION = 3;
    static final int TA_ONE = 4;
    static final int TA_TWO = 5;
    
    public UpdateRecitation_Transaction(String old, String newInfo,RecitationData data,RecitationTab tab){
        oldData = old.split(";");
        newData = newInfo.split(";");
        this.data = data;
        this.tab = tab;
    }
    
    @Override
    public void doTransaction(){
       Recitation r = data.getRecitation(oldData[SECTION]);
       r.setSection(newData[SECTION]);
       r.setInstructor(newData[INSTRUCTOR]);
       r.setDay(newData[DAY]);
       r.setLocation(newData[LOCATION]);
       if(!data.containsTA(newData[TA_ONE]))
           r.setTAOne("");
       else
           r.setTAOne(newData[TA_ONE]);
       if(!data.containsTA(newData[TA_TWO]))
           r.setTATwo("");
       else
           r.setTATwo(newData[TA_TWO]);
       Collections.sort(data.getRecitations());
       tab.getTable().refresh();
       if(tab.getTable().getSelectionModel().getSelectedItem() != null)
        tab.getController().handleTableSelection();
    }
    
    @Override
    public void undoTransaction(){
       Recitation r = data.getRecitation(newData[SECTION]);
       r.setSection(oldData[SECTION]);
       r.setInstructor(oldData[INSTRUCTOR]);
       r.setDay(oldData[DAY]);
       r.setLocation(oldData[LOCATION]);
       if(!data.containsTA(oldData[TA_ONE]))
           r.setTAOne("");
       else
           r.setTAOne(oldData[TA_ONE]);
       if(!data.containsTA(oldData[TA_TWO]))
           r.setTATwo("");
       else
           r.setTATwo(oldData[TA_TWO]);
       Collections.sort(data.getRecitations());
       tab.getTable().refresh();
       if(tab.getTable().getSelectionModel().getSelectedItem() != null)
        tab.getController().handleTableSelection();
    }
}
