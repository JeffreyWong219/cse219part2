package course_site.transactions;
import jtps.jTPS_Transaction;
import course_site.data.TADataTabData;
/**
 *
 * @author Jeffrey Wong
 */
public class ToggleTA_Transaction implements jTPS_Transaction{
    
    private TADataTabData data;
    private String name;
    private String cellKey;
    
    public ToggleTA_Transaction(TADataTabData data, String name, String cellKey){
        this.data = data;
        this.name = name;
        this.cellKey = cellKey;
    }
    
    @Override
    public void doTransaction()
    {
        data.toggleTAOfficeHours(cellKey, name);
    }
    
    @Override
    public void undoTransaction()
    {
        data.toggleTAOfficeHours(cellKey, name);
    }
}
