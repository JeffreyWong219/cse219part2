
package course_site.transactions;

import course_site.data.ScheduleData;
import course_site.workspaces.CourseSiteWorkspace;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import jtps.jTPS_Transaction;

public class UpdateScheduleStartEnd_Transaction implements jTPS_Transaction {
    
    String oldDate,newDate;
    ArrayList<String> changes;
    ScheduleData data;
    boolean reverse;
    
    static final int TYPE = 0;
    static final int DATE = 1;
    static final int TIME = 2;
    static final int TITLE = 3;
    static final int TOPIC = 4;
    static final int LINK = 5;
    static final int CRITERIA = 6;
    
    public UpdateScheduleStartEnd_Transaction(String oldDate, String newDate, ArrayList<String> changes,ScheduleData data,boolean reverse){
        this.oldDate = oldDate;
        this.newDate = newDate;
        this.changes = changes;
        this.data = data;
        this.reverse = reverse;
    }
    
    @Override
    public void doTransaction(){
        for(String e: changes){
            String[] information = data.translateInformationString(e);
            data.removeEvent(data.getEvent(information[TYPE],information[TITLE], information[TOPIC], information[TIME],information[CRITERIA],information[LINK], LocalDate.parse(information[DATE])));
            
        }
        if(!reverse){
            data.setStartDate(LocalDate.parse(newDate));
        }
        else
            data.setEndDate(LocalDate.parse(newDate));
        ((CourseSiteWorkspace)data.getMain().getWorkspaceComponent()).getScheduleTab().setUndoState(false);
    }
    
    @Override
    public void undoTransaction(){
        for(String e:changes){
            String[] information = data.translateInformationString(e);
            data.addEvent(information[TYPE],information[TITLE], information[TOPIC], information[TIME],information[CRITERIA],information[LINK], LocalDate.parse(information[DATE]));
        }
        if(!reverse)
            data.setStartDate(LocalDate.parse(oldDate));
        else
            data.setEndDate(LocalDate.parse(oldDate));
        ((CourseSiteWorkspace)data.getMain().getWorkspaceComponent()).getScheduleTab().getTable().refresh();
        Collections.sort(data.getSchedules());
        ((CourseSiteWorkspace)data.getMain().getWorkspaceComponent()).getScheduleTab().setUndoState(false);
    }
    
}
