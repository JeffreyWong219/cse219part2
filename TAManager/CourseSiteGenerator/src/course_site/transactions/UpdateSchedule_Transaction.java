
package course_site.transactions;

import course_site.data.ScheduleData;
import course_site.data.ScheduleEvent;
import course_site.workspaces.CourseSiteWorkspace;
import java.time.LocalDate;
import java.util.Collections;
import jtps.jTPS_Transaction;

public class UpdateSchedule_Transaction implements jTPS_Transaction{
    
    String oldType,oldTime,oldTitle,oldTopic,oldLink,oldCriteria,newType,newTime,newTitle,newTopic,newLink,newCriteria;
    String oldDate,newDate;
    ScheduleData data;
    
    public UpdateSchedule_Transaction(String oldType,String oldTime, LocalDate oldDate,String oldTitle,String oldTopic,String oldLink,String oldCriteria,
                                        String newType,String newTime, LocalDate newDate,String newTitle,String newTopic,String newLink,String newCriteria,ScheduleData data){
        this.oldType = oldType;
        this.oldTime = oldTime;
        this.oldTopic = oldTopic;
        this.oldLink = oldLink;
        this.oldTitle = oldTitle;
        this.oldCriteria = oldCriteria;
        this.newType = newType;
        this.newTime = newTime;
        this.newTitle = newTitle;
        this.newTopic = newTopic;
        this.newLink = newLink;
        this.newCriteria = newCriteria;
        this.oldDate = oldDate.toString();
        this.newDate = newDate.toString();
        this.data = data;
    }
    
    @Override
    public void doTransaction(){
        ScheduleEvent r = data.getEvent(oldType,oldTitle,oldTopic,oldTime,oldCriteria,oldLink,LocalDate.parse(oldDate));
        r.setType(newType);
        r.setTime(newTime);
        r.setTitle(newTitle);
        r.setTopic(newTopic);
        r.setCriteria(newCriteria);
        r.setLink(newLink);
        r.setDate(LocalDate.parse(newDate));
        ((CourseSiteWorkspace)data.getMain().getWorkspaceComponent()).getScheduleTab().getTable().refresh();
        Collections.sort(data.getSchedules());
        ((CourseSiteWorkspace)data.getMain().getWorkspaceComponent()).getScheduleTab().setUndoState(false);
    }
    
    @Override
    public void undoTransaction(){
        ScheduleEvent r = data.getEvent(newType,newTitle,newTopic,newTime,newCriteria,newLink,LocalDate.parse(newDate));
        r.setType(oldType);
        r.setTime(oldTime);
        r.setTitle(oldTitle);
        r.setTopic(oldTopic);
        r.setCriteria(oldCriteria);
        r.setLink(oldLink);
        r.setDate(LocalDate.parse(oldDate));
        ((CourseSiteWorkspace)data.getMain().getWorkspaceComponent()).getScheduleTab().getTable().refresh();
        Collections.sort(data.getSchedules());
        ((CourseSiteWorkspace)data.getMain().getWorkspaceComponent()).getScheduleTab().setUndoState(false);
    }
    
}
