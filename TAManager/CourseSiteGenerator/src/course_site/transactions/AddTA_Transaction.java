
package course_site.transactions;
import jtps.jTPS_Transaction;
import course_site.data.TADataTabData;
/**
 *
 * @author Jeffrey Wong
 */
public class AddTA_Transaction implements jTPS_Transaction {
    
    private String name;
    private String email;
    private TADataTabData data;
    
    public AddTA_Transaction(String name, String email, TADataTabData data)
    {
        this.name = name;
        this.email = email;
        this.data = data;
    }
    
    @Override
    public void doTransaction(){
        data.addTA(name,email,false);
    }
    
    @Override
    public void undoTransaction(){
        data.removeTA(name);
        
    }
}
