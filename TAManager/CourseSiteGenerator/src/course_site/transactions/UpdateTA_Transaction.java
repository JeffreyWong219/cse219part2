package course_site.transactions;
import java.util.Collections;
import jtps.jTPS_Transaction;
import course_site.data.TADataTabData;
import course_site.data.TeachingAssistant;
import course_site.workspaces.TADataTab;
/**
 *
 * @author Jeffrey Wong
 */
public class UpdateTA_Transaction implements jTPS_Transaction{
    
    private String oldName;
    private String newName;
    private String oldEmail;
    private String newEmail;
    private TADataTabData data;
    private TADataTab workspace;
    
    public UpdateTA_Transaction(String oldName, String oldEmail, String newName,String newEmail, TADataTabData data, TADataTab workspace)
    {
        this.oldName = oldName;
        this.oldEmail = oldEmail;
        this.newName = newName;
        this.newEmail = newEmail;
        this.data = data;
        this.workspace = workspace;
    }
    
    @Override
    public void doTransaction(){
        TeachingAssistant ta = data.getTA(oldName);
        ta.setName(newName);
        ta.setEmail(newEmail);
        data.replaceAllInstances(oldName,newName);
        workspace.getTATable().refresh();
        Collections.sort(data.getTeachingAssistants());
    }
    
    @Override
    public void undoTransaction(){
        TeachingAssistant ta = data.getTA(newName);
        ta.setName(oldName);
        ta.setEmail(oldEmail);
        data.replaceAllInstances(newName,oldName);
        workspace.getTATable().refresh();
        Collections.sort(data.getTeachingAssistants());
    }
}
