
package course_site.transactions;

import course_site.data.ProjectData;
import jtps.jTPS_Transaction;

public class AddOrDeleteStudent_Transaction implements jTPS_Transaction {
    String firstName,lastName,team,role;
    boolean delete;
    ProjectData data;
    
    public AddOrDeleteStudent_Transaction(String first,String last, String teamName,String role,boolean toDelete,ProjectData data){
        firstName = first;
        lastName = last;
        team = teamName;
        this.role = role;
        delete = toDelete;
        this.data = data;
    }
    
    @Override
    public void doTransaction(){
        if(!delete){
            data.addStudent(firstName,lastName,team,role);
        }
        else
            data.removeStudent(firstName,lastName,team);
    }
    
    @Override
    public void undoTransaction(){
        if(!delete){
            data.removeStudent(firstName,lastName,team);
        }
        else
            data.addStudent(firstName,lastName,team,role);
    }
}
