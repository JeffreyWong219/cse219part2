
package course_site.transactions;
import java.util.ArrayList;
import jtps.jTPS_Transaction;
import course_site.data.TADataTabData;
import course_site.data.TeachingAssistant;

/**
 *
 * @author Jeffrey Wong
 */
public class DeleteTA_Transaction implements jTPS_Transaction{
    
    private String deletedEmail;
    private String deletedName;
    private boolean wasChecked;
    private ArrayList<String> oldPositions;
    private TADataTabData data;
    
    public DeleteTA_Transaction(String name, String email,boolean wasChecked, ArrayList<String> oldPositions,TADataTabData data)
    {
        deletedName = name;
        deletedEmail = email;
        this.wasChecked = wasChecked;
        this.oldPositions = oldPositions;
        this.data = data;
    }
    
    @Override
    public void doTransaction()
    {
        TeachingAssistant ta = data.getTA(deletedName);
        data.getTeachingAssistants().remove(ta);
        data.replaceAllInstances(deletedName, "");
        
    }
    
    @Override
    public void undoTransaction(){
        data.addTA(deletedName,deletedEmail,wasChecked);
        
        for(int i = 0; i < oldPositions.size();i++)
        {
            String[] oldInfo = oldPositions.get(i).split(";");
            if(oldInfo.length == 1)
                continue;
            data.getOfficeHours().get(oldInfo[0]).setValue(oldInfo[1]);
        }
    }
}
