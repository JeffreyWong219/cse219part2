
package course_site.transactions;

import course_site.data.TADataTabData;
import course_site.data.TeachingAssistant;
import jtps.jTPS_Transaction;

/**
 *
 * @author cool7
 */
public class ToggleGraduate_Transaction implements jTPS_Transaction {
    private boolean wasGraduate;
    private String taName;
    private TADataTabData data;
    
    public ToggleGraduate_Transaction(boolean toggled, String name, TADataTabData data){
        wasGraduate = !toggled;
        taName = name;
        this.data = data;
    }
    
    @Override
    public void doTransaction(){
        TeachingAssistant ta = data.getTA(taName);
        ta.setBoolean(!wasGraduate);
    }
    
    @Override
    public void undoTransaction(){
        TeachingAssistant ta = data.getTA(taName);
        ta.setBoolean(wasGraduate);
    }
}
