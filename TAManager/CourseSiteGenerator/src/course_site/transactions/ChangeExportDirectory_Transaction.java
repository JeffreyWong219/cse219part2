
package course_site.transactions;

import course_site.workspaces.CourseEditorTab;
import java.io.File;
import jtps.jTPS_Transaction;

public class ChangeExportDirectory_Transaction implements jTPS_Transaction{
    
    private final String oldPath;
    private final String newPath;
    CourseEditorTab tab;
    
    public ChangeExportDirectory_Transaction(String oldPath, String newPath,CourseEditorTab workspace){
        this.oldPath = oldPath;
        this.newPath = newPath;
        tab = workspace;
    }
    
    @Override
    public void doTransaction(){
        tab.setExportFile(new File(newPath));
    }
    
    @Override
    public void undoTransaction(){
        if(oldPath == null)
            tab.setExportFile(null);
        else
            tab.setExportFile(new File(oldPath));
    }
    
}
