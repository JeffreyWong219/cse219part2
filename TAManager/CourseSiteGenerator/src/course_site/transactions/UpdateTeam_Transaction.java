
package course_site.transactions;

import course_site.data.ProjectData;
import course_site.data.Team;
import course_site.workspaces.ProjectTab;
import java.util.ArrayList;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

public class UpdateTeam_Transaction implements jTPS_Transaction{
    String[] oldInformation,newInformation;
    ProjectData data;
    ProjectTab tab;
    
    static final int NAME = 0;
    static final int TEAM_COLOR = 1;
    static final int TEXT_COLOR = 2;
    static final int LINK = 3;
    
    public UpdateTeam_Transaction(String oldData,String newData, ProjectData data, ProjectTab tab){
        oldInformation = oldData.split(";");
        newInformation = newData.split(";");
        this.data = data;
        this.tab = tab;
    }
    
    @Override
    public void doTransaction(){
        Team t = data.getTeam(oldInformation[NAME]);
        ArrayList<String> students = t.getStudents();
        ArrayList<String> newStudents = new ArrayList<>();
        for(String s:students){
            String[] parts = s.split(";");
            newStudents.add(parts[0]+";"+parts[1]+";"+newInformation[NAME]+";"+parts[3]);
        }
        t.replaceStudentsListWith(newStudents);
        data.moveHash(oldInformation[NAME], newInformation[NAME]);
        t.setName(newInformation[NAME]);
        t.setColor(Color.web(newInformation[TEAM_COLOR]));
        t.setTextColor(Color.web(newInformation[TEXT_COLOR]));
        t.setLink(newInformation[LINK]);
        data.replaceAllStudentTeamInstancesWith(oldInformation[NAME], newInformation[NAME]);
        tab.getStudentTable().refresh();
        tab.getTeamTable().refresh();
        int index = tab.getTeamSelection().getItems().indexOf(t);
        tab.getTeamSelection().getItems().remove(t);
        tab.getTeamSelection().getItems().add(index,t);
        if(tab.getStudentTable().getSelectionModel().getSelectedItem() != null&&tab.getTeamSelection().getSelectionModel().getSelectedItem() != t)
            tab.getTeamSelection().setValue(t);
        tab.getTeamTable().getSelectionModel().select(t);
        tab.getController().handleTeamTableSelection();
    }
    
    @Override
    public void undoTransaction(){
        Team t = data.getTeam(newInformation[NAME]);
        ArrayList<String> students = t.getStudents();
        ArrayList<String> newStudents = new ArrayList<>();
        for(String s:students){
            String[] parts = s.split(";");
            newStudents.add(parts[0]+";"+parts[1]+";"+oldInformation[NAME]+";"+parts[3]);
        }
        t.replaceStudentsListWith(newStudents);
        data.moveHash(newInformation[NAME], oldInformation[NAME]);
        t.setName(oldInformation[NAME]);
        t.setColor(Color.web(oldInformation[TEAM_COLOR]));
        t.setTextColor(Color.web(oldInformation[TEXT_COLOR]));
        t.setLink(oldInformation[LINK]);
        tab.getTeamSelection().setItems(data.getTeams());
        data.replaceAllStudentTeamInstancesWith(newInformation[NAME], oldInformation[NAME]);
        tab.getStudentTable().refresh();
        tab.getTeamTable().refresh();
        int index = tab.getTeamSelection().getItems().indexOf(t);
        tab.getTeamSelection().getItems().remove(t);
        tab.getTeamSelection().getItems().add(index,t);
        if(tab.getStudentTable().getSelectionModel().getSelectedItem() != null&&tab.getTeamSelection().getSelectionModel().getSelectedItem() != t)
            tab.getTeamSelection().setValue(t);
        tab.getTeamTable().getSelectionModel().select(t);
        tab.getController().handleTeamTableSelection();
        
    }
}
