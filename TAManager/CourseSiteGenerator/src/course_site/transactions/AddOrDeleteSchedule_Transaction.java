
package course_site.transactions;

import course_site.data.ScheduleData;
import course_site.data.ScheduleEvent;
import course_site.workspaces.CourseSiteWorkspace;
import java.time.LocalDate;
import jtps.jTPS_Transaction;

public class AddOrDeleteSchedule_Transaction implements jTPS_Transaction{
    String type,time,title,topic,criteria;
    boolean reverse;
    ScheduleData data;
    LocalDate date;
    String link;
   
    
    public AddOrDeleteSchedule_Transaction(String type,String time, LocalDate date,String title,String topic,String link,String criteria,boolean delete,ScheduleData data){
        this.type = type;
        this.data = data;
        this.time = time;
        this.date = date;
        this.title = title;
        this.topic = topic;
        this.link = link;
        this.criteria = criteria;
        reverse = delete;
    }
    
    @Override
    public void doTransaction(){
        if(!reverse)
            data.addEvent(type,title,topic,time,criteria,link,date);
        else{
            ScheduleEvent r = data.getEvent(type,title,topic,time,criteria,link,date);
            data.removeEvent(r);
        }
        ((CourseSiteWorkspace)data.getMain().getWorkspaceComponent()).getScheduleTab().setUndoState(false);
    }
    
    @Override
    public void undoTransaction(){
        if(!reverse){
            ScheduleEvent r = data.getEvent(type,title,topic,time,criteria,link,date);
            data.removeEvent(r);
        }
        else{
            data.addEvent(type,title,topic,time,criteria,link,date);
        }
        ((CourseSiteWorkspace)data.getMain().getWorkspaceComponent()).getScheduleTab().setUndoState(false);
    }
    
    
}
