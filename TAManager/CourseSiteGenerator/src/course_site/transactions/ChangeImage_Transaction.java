package course_site.transactions;

import course_site.workspaces.CourseEditorTab;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;

public class ChangeImage_Transaction implements jTPS_Transaction{
    String oldImagePath;
    String newImagePath;
    String whichOne;
    ImageView container;
    CourseEditorTab tab;
    
    public ChangeImage_Transaction(String old,String newImage,String whichOne, ImageView containing,CourseEditorTab workspace)
    {
        oldImagePath = old;
        newImagePath = newImage;
        this.whichOne = whichOne;
        container = containing;
        tab = workspace;
    }
    
    @Override
    public void doTransaction(){
        container.setImage(new Image("file:"+newImagePath));
        switch(whichOne){
            case "left":{
                tab.setLeftImagePath(newImagePath);
                break;
            }
            case "right":{
                tab.setRightImagePath(newImagePath);
                break;
            }
            case "banner":{
                tab.setBannerImagePath(newImagePath);
                break;
            }
            default:break;
        }
    }
    
    @Override
    public void undoTransaction(){
        if(oldImagePath == null || oldImagePath.equals("")){
            container.setImage(null);
        }
        else
            container.setImage(new Image("file:"+oldImagePath));
        switch(whichOne){
            case "left":{
                tab.setLeftImagePath(oldImagePath);
                break;
            }
            case "right":{
                tab.setRightImagePath(oldImagePath);
                break;
            }
            case "banner":{
                tab.setBannerImagePath(oldImagePath);
                break;
            }
            default:break;
        }
    }
    
}
