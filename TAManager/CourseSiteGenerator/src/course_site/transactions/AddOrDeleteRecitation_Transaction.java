
package course_site.transactions;

import course_site.data.RecitationData;
import jtps.jTPS_Transaction;

public class AddOrDeleteRecitation_Transaction implements jTPS_Transaction{
    
    String[] info;
    boolean reverse;
    RecitationData data;
    
    static final int SECTION = 0;
    static final int INSTRUCTOR = 1;
    static final int DAY = 2;
    static final int LOCATION = 3;
    static final int TA_ONE = 4;
    static final int TA_TWO = 5;
    
    public AddOrDeleteRecitation_Transaction(String recitation, boolean reversed, RecitationData data){
        info = recitation.split(";");
        reverse = reversed;
        this.data = data;
    }
    
    @Override
    public void doTransaction(){
        if(!reverse)
            adding();
        else
            removing();
    }
    
    @Override
    public void undoTransaction(){
        if(!reverse){
            removing();
        }
        else
            adding();
    }
    
    public void adding(){
        String taOne = info[TA_ONE];
        if(!data.containsTA(info[TA_ONE])||taOne.equals("?"))
            taOne = "";
        String taTwo = info[TA_TWO];
        if(!data.containsTA(info[TA_TWO])||taOne.equals("?"))
            taTwo = "";
        data.addRecitation(info[SECTION],info[INSTRUCTOR],info[DAY],info[LOCATION],taOne,taTwo);
    }
    
    public void removing(){
        data.removeRecitation(data.getRecitation(info[SECTION]));
    }
    
}
