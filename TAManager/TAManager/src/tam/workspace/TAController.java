package tam.workspace;

import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.ArrayList;
import java.util.Collections;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import javafx.collections.ObservableList;
import tam.style.TAStyle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jtps.jTPS;
import tam.transactions.AddTA_Transaction;
import tam.transactions.DeleteTA_Transaction;
import tam.transactions.ToggleTA_Transaction;
import tam.transactions.UpdateOfficeHours_Transaction;
import tam.transactions.UpdateTA_Transaction;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class TAController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    jTPS transactions = new jTPS();
    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        if(checkValidity(name, email, data)){
            // ADD THE NEW TA TO THE DATA
            transactions.addTransaction(new AddTA_Transaction(name,email,data));
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            app.getGUI().fileController.markAsEdited(app.getGUI());
        }
    }
    
    public boolean checkValidity(String nameText,String emailText, TAData data)
    { 
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        return checkIfEmptyName(props,nameText)&&checkIfEmptyEmail(props,emailText)
                &&checkIfUniqueAndValidEmail(props,emailText,data)&&checkIfUniqueTAName(props,nameText,data);
    }
    
    public boolean checkIfEmptyName(PropertiesManager props, String nameText){
        if (nameText.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));  
            return false;
        }
        return true;
    }
    
    public boolean checkIfEmptyEmail(PropertiesManager props, String emailText)
    {
        if(emailText.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
            return false;
        }
        return true;
    }
    
    public boolean checkIfUniqueAndValidEmail(PropertiesManager props, String emailText, TAData data)
    {
        Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher emailMatcher = emailPattern.matcher(emailText);
        if(!emailMatcher.matches())
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TA_EMAIL_NOT_RIGHT_FORMAT_TITLE), props.getProperty(TA_EMAIL_NOT_RIGHT_FORMAT_MESSAGE));
            return false;
        }
        else if(data.containsTAEmail(emailText))
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE)); 
            return false;
        }
        return true;
    }
    
    public boolean checkIfUniqueTAName(PropertiesManager props, String nameText, TAData data)
    {
        if(data.containsTA(nameText))
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE)); 
            return false;
        }
        return true;
    }

    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        
        // GET THE TA
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String taName = ta.getName();
        String cellKey = pane.getId();
        transactions.addTransaction(new ToggleTA_Transaction((TAData)app.getDataComponent(),taName,cellKey));
        app.getGUI().fileController.markAsEdited(app.getGUI());
        
    }
    
    public void handleTableDeletion(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        TableView table = workspace.getTATable();
        PropertiesManager props = PropertiesManager.getPropertiesManager();    
        AppYesNoCancelDialogSingleton confirmation = AppYesNoCancelDialogSingleton.getSingleton();
        confirmation.show(props.getProperty(DELETE_TABLE_ROW_TITLE),props.getProperty(DELETE_TABLE_ROW_MESSAGE));
        String choice = confirmation.getSelection();
        if(choice.equals(AppYesNoCancelDialogSingleton.YES))
        {
            Object selected = table.getSelectionModel().getSelectedItem();
            if(selected != null)
            {
                TeachingAssistant ta = (TeachingAssistant)selected;
                transactions.addTransaction(new DeleteTA_Transaction(ta.getName(),ta.getEmail(),data.retrieveAllInstances(ta.getName()),data));
                app.getGUI().fileController.markAsEdited(app.getGUI());
            }
        }
    }
    
    public void handleCellHighlighting(TAWorkspace workspace, int row, int column)
    {
        ((TAStyle)app.getStyleComponent()).changeStyleOnHover(workspace, row, column);
    }
    
    public void handleCellLeaving(TAWorkspace workspace, int row, int column)
    {
        ((TAStyle)app.getStyleComponent()).revertChanges(workspace, row, column);
    }
    
    public void handleUpdateAction()
    {
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        ObservableList<TeachingAssistant> list  = data.getTeachingAssistants();
        
        TableView table = workspace.getTATable();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        
        TeachingAssistant selected = ((TAData)app.getDataComponent()).getTA(((TeachingAssistant)table.getSelectionModel().getSelectedItem()).getName());
        String originalName = selected.getName();
        String originalEmail = selected.getEmail();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        boolean proceed = true;
        if(!originalEmail.equals(email) && originalName.equals(name))
           proceed = checkIfUniqueAndValidEmail(props,email,data) && checkIfEmptyEmail(props,email);
        if(originalEmail.equals(email)&& !originalName.equals(name))
           proceed = checkIfUniqueTAName(props,name,data) && checkIfEmptyName(props,name);
        if(!originalEmail.equals(email)&& !originalName.equals(name))
           proceed = checkValidity(name,email,data);
        if(proceed){
            transactions.addTransaction(new UpdateTA_Transaction(originalName,originalEmail,name,email,data,workspace));
            app.getGUI().fileController.markAsEdited(app.getGUI());
        }
    }
    
    public void handleDisableComboButton(TAWorkspace workspace)
    {   
        int startWeight = workspace.getComboBoxHours().indexOf(workspace.getChosenStartTime());
        int endWeight = workspace.getComboBoxHours().indexOf(workspace.getChosenEndTime());
        if(startWeight >= endWeight)
            workspace.getConfirmButton().setDisable(true);
        else if(workspace.getConfirmButton().isDisabled())
            workspace.getConfirmButton().setDisable(false);
    }
            
    public void handleHourChanges(TAWorkspace workspace,TAData data){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppYesNoCancelDialogSingleton confirmation = AppYesNoCancelDialogSingleton.getSingleton();
        String newStart = data.getMilitaryHour(workspace.getChosenStartTime());
        String newEnd = data.getMilitaryHour(workspace.getChosenEndTime());
        String earliestHour = data.findEarliestHour();
        String latestHour = data.findLatestHour();
        boolean overwriteEarliestHour = false;
        boolean overwriteLatestHour = false;
        if(latestHour != null && earliestHour != null)
        {
            overwriteEarliestHour = data.compareMilitaryHour(earliestHour, newStart) < 0;
            overwriteLatestHour = data.compareMilitaryHour(latestHour, newEnd) > 0;
        }
        String choice = "";
        if(overwriteEarliestHour || overwriteLatestHour){
            confirmation.show(props.getProperty(REMOVE_TA_FROM_TIMES_TITLE),props.getProperty(REMOVE_TA_FROM_TIMES_MESSAGE));
            choice = confirmation.getSelection();
        }
        if(choice.equals(AppYesNoCancelDialogSingleton.YES)|| (!overwriteEarliestHour && !overwriteLatestHour))
        {
            transactions.addTransaction(new UpdateOfficeHours_Transaction(data.getTimeString(data.getStartHour(),data.getStartBoolean()),
                    data.getTimeString(data.getEndHour(),data.getEndBoolean()),workspace.getChosenStartTime(),workspace.getChosenEndTime(),data,workspace));
            app.getGUI().fileController.markAsEdited(app.getGUI());
        }
    }
    
    public void handleUndoAction()
    {
        transactions.undoTransaction();
        if(!app.getGUI().fileController.isSaved() && !transactions.nothingToUndo())
            app.getGUI().fileController.markAsEdited(app.getGUI());
    }
    
    public void handleRedoAction(){
        transactions.doTransaction();
        if(!app.getGUI().fileController.isSaved() && !transactions.nothingToRedo())
            app.getGUI().fileController.markAsEdited(app.getGUI());
    }
}