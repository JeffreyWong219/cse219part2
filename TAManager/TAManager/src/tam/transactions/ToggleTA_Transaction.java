package tam.transactions;
import jtps.jTPS_Transaction;
import tam.data.TAData;
/**
 *
 * @author Jeffrey Wong
 */
public class ToggleTA_Transaction implements jTPS_Transaction{
    
    private TAData data;
    private String name;
    private String cellKey;
    
    public ToggleTA_Transaction(TAData data, String name, String cellKey){
        this.data = data;
        this.name = name;
        this.cellKey = cellKey;
    }
    
    @Override
    public void doTransaction()
    {
        data.toggleTAOfficeHours(cellKey, name);
    }
    
    @Override
    public void undoTransaction()
    {
        data.toggleTAOfficeHours(cellKey, name);
    }
}
