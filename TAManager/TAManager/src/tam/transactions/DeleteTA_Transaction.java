
package tam.transactions;
import java.util.ArrayList;
import jtps.jTPS_Transaction;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author Jeffrey Wong
 */
public class DeleteTA_Transaction implements jTPS_Transaction{
    
    private String deletedEmail;
    private String deletedName;
    private ArrayList<String> oldPositions;
    private TAData data;
    
    public DeleteTA_Transaction(String name, String email, ArrayList<String> oldPositions,TAData data)
    {
        deletedName = name;
        deletedEmail = email;
        this.oldPositions = oldPositions;
        this.data = data;
    }
    
    @Override
    public void doTransaction()
    {
        TeachingAssistant ta = data.getTA(deletedName);
        data.getTeachingAssistants().remove(ta);
        data.replaceAllInstances(deletedName, "");
        
    }
    
    @Override
    public void undoTransaction(){
        data.addTA(deletedName,deletedEmail);
        
        for(int i = 0; i < oldPositions.size();i++)
        {
            String[] oldInfo = oldPositions.get(i).split(";");
            if(oldInfo.length == 1)
                continue;
            data.getOfficeHours().get(oldInfo[0]).setValue(oldInfo[1]);
        }
    }
}
