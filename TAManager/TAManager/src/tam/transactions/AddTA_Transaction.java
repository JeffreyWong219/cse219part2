
package tam.transactions;
import jtps.jTPS_Transaction;
import tam.data.TAData;
/**
 *
 * @author Jeffrey Wong
 */
public class AddTA_Transaction implements jTPS_Transaction {
    
    private String name;
    private String email;
    private TAData data;
    
    public AddTA_Transaction(String name, String email, TAData data)
    {
        this.name = name;
        this.email = email;
        this.data = data;
    }
    
    @Override
    public void doTransaction(){
        data.addTA(name,email);
    }
    
    @Override
    public void undoTransaction(){
        data.removeTA(name);
        
    }
}
