package tam.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.TAManagerProp;
import tam.workspace.TAWorkspace;

/**
 * This is the data component for TAManagerApp. It has all the data needed
 * to be set by the user via the User Interface and file I/O can set and get
 * all the data from this object
 * 
 * @author Richard McKenna
 */
public class TAData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    TAManagerApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<TeachingAssistant> teachingAssistants;

    // THIS WILL STORE ALL THE OFFICE HOURS GRID DATA, WHICH YOU
    // SHOULD NOTE ARE StringProperty OBJECTS THAT ARE CONNECTED
    // TO UI LABELS, WHICH MEANS IF WE CHANGE VALUES IN THESE
    // PROPERTIES IT CHANGES WHAT APPEARS IN THOSE LABELS
    HashMap<String, StringProperty> officeHours;
    
    // THESE ARE THE LANGUAGE-DEPENDENT VALUES FOR
    // THE OFFICE HOURS GRID HEADERS. NOTE THAT WE
    // LOAD THESE ONCE AND THEN HANG ON TO THEM TO
    // INITIALIZE OUR OFFICE HOURS GRID
    ArrayList<String> gridHeaders;

    // THESE ARE THE TIME BOUNDS FOR THE OFFICE HOURS GRID. NOTE
    // THAT THESE VALUES CAN BE DIFFERENT FOR DIFFERENT FILES, BUT
    // THAT OUR APPLICATION USES THE DEFAULT TIME VALUES AND PROVIDES
    // NO MEANS FOR CHANGING THESE VALUES
    int startHour;
    int endHour;
    
    //incase the user chooses half minute intervals
    boolean startsOnHalfHour;
    boolean endsOnHalfHour;
    
    // DEFAULT VALUES FOR START AND END HOURS IN MILITARY HOURS
    public static final int MIN_START_HOUR = 9;
    public static final int MAX_END_HOUR = 20;

    /**
     * This constructor will setup the required data structures for
     * use, but will have to wait on the office hours grid, since
     * it receives the StringProperty objects from the Workspace.
     * 
     * @param initApp The application this data manager belongs to. 
     */
    public TAData(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // CONSTRUCT THE LIST OF TAs FOR THE TABLE
        teachingAssistants = FXCollections.observableArrayList();

        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        startsOnHalfHour = false;
        endsOnHalfHour = false;
        
        //THIS WILL STORE OUR OFFICE HOURS
        officeHours = new HashMap();
        
        // THESE ARE THE LANGUAGE-DEPENDENT OFFICE HOURS GRID HEADERS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> timeHeaders = props.getPropertyOptionsList(TAManagerProp.OFFICE_HOURS_TABLE_HEADERS);
        ArrayList<String> dowHeaders = props.getPropertyOptionsList(TAManagerProp.DAYS_OF_WEEK);
        gridHeaders = new ArrayList();
        gridHeaders.addAll(timeHeaders);
        gridHeaders.addAll(dowHeaders);
    }
    
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    @Override
    public void resetData() {
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        startsOnHalfHour = false;
        endsOnHalfHour = false;
        teachingAssistants.clear();
        officeHours.clear();
        ((TAWorkspace)app.getWorkspaceComponent()).clearTransactionHistory();
        ((TAWorkspace)app.getWorkspaceComponent()).resetComboBoxSelection();
    }
    
    // ACCESSOR METHODS

    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }
    
    public String getMilitaryStartHour(){
        String timeString = startHour +"_";
        if(startsOnHalfHour)
            timeString += "30";
        else
            timeString += "00";
        return timeString;
    }
    
    public String getMilitaryEndHour(){
        String timeString = endHour +"_";
        if(endsOnHalfHour)
            timeString += "30";
        else
            timeString += "00";
        return timeString;
    }
    
    public void setStartHour(int hour){
        startHour = hour;
    }
    
    public void setEndHour(int hour){
        endHour = hour;
    }
    
    public void setStartBoolean(boolean thirty){
        startsOnHalfHour = thirty;
    }
    
    public void setEndBoolean(boolean thirty){
        endsOnHalfHour = thirty;
    }
    
    public boolean getStartBoolean(){
        return startsOnHalfHour;
    }
    
    public boolean getEndBoolean(){
        return endsOnHalfHour;
    }
    
    public ArrayList<String> getGridHeaders() {
        return gridHeaders;
    }

    public ObservableList getTeachingAssistants() {
        return teachingAssistants;
    }
    
    public String getCellKey(int col, int row) {
        return col + "_" + row;
    }

    public StringProperty getCellTextProperty(int col, int row) {
        String cellKey = getCellKey(col, row);
        return officeHours.get(cellKey);
    }

    public HashMap<String, StringProperty> getOfficeHours() {
        return officeHours;
    }
    
    public int getNumRows() {
        int offset = 0;
        if(getEndBoolean())
            offset++;
        if(getStartBoolean())
            offset--;
        return ((endHour - startHour) * 2) + 1 + offset;
    }
    
    public String getTimeString(int militaryHour, boolean onHalfHour) {
        String minutesText = "00";
        if (onHalfHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
    public String getMilitaryHour(String timeString)
    {
        String time = "";
        String[] parts = timeString.split(":");
        if(parts[1].contains("pm") && Integer.parseInt(parts[0])==12)
            time += 12+"_";
        if(parts[1].contains("pm") && Integer.parseInt(parts[0])<12)
            time += Integer.parseInt(parts[0]) + 12 + "_";
        else
            if(parts[1].contains("am"))
            {
                if(parts[0].equals("12"))
                    time += 0 + "_";
                else
                    time += Integer.parseInt(parts[0])+"_";
            }
        if(parts[1].contains("30"))
            time += "30";
        else
            time += "00";
        return time;
    }
    
    public String getCellKey(String day, String time) {
        int col = gridHeaders.indexOf(day);
        int row = 1;
        int hour = Integer.parseInt(time.substring(0, time.indexOf("_")));
        int milHour = hour;
        if (hour < startHour || hour == startHour && time.contains("pm") || hour > 0 && hour < 12 && time.contains("pm"))//left behidn to let me know hwere to continue from
            milHour += 12;
        if(hour == 12 && time.contains("am"))
            milHour = 0;
        row += (milHour - startHour) * 2;
        if (time.contains("_30"))
            row += 1;
        return getCellKey(col, row);
    }
    
    public TeachingAssistant getTA(String testName) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return ta;
            }
        }
        return null;
    }
    
    public void removeTA(String taName){
         TeachingAssistant ta = getTA(taName);
         if(ta != null)
            teachingAssistants.remove(ta);
    }
    
    /**
     * This method is for giving this data manager the string property
     * for a given cell.
     */
    public void setCellProperty(int col, int row, StringProperty prop) {
        String cellKey = getCellKey(col, row);
        officeHours.put(cellKey, prop);
    }    
    
    /**
     * This method is for setting the string property for a given cell.
     */
    public void setGridProperty(ArrayList<ArrayList<StringProperty>> grid,
                                int column, int row, StringProperty prop) {
        grid.get(row).set(column, prop);
    }
    
    private void initOfficeHours(int initStartHour, int initEndHour) {
        // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
        startHour = initStartHour;
        endHour = initEndHour;
        
        // EMPTY THE CURRENT OFFICE HOURS VALUES
        officeHours.clear();
            
        // WE'LL BUILD THE USER INTERFACE COMPONENT FOR THE
        // OFFICE HOURS GRID AND FEED THEM TO OUR DATA
        // STRUCTURE AS WE GO
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.reloadOfficeHoursGrid(this);
    }
    
    public int compareMilitaryHour(String firstHour, String secondHour)
    {
        String[] firstHourComp = firstHour.split("_");
        String[] secondHourComp = secondHour.split("_");
        if(Integer.parseInt(firstHourComp[0]) < Integer.parseInt(secondHourComp[0]))
            return -1;
        else if(firstHourComp[0].equals(secondHourComp[0])){
            if(firstHourComp[1].equals(secondHourComp[1]))
                return 0;
            if(Integer.parseInt(firstHourComp[1])<Integer.parseInt(secondHourComp[1]))
                return -1;
        }
        return 1;
    }
    
    public void initHours(String startHourText, String endHourText) {
        String[] initStartHour = startHourText.split("_");
        int initStartHourFirstComponent = Integer.parseInt(initStartHour[0]);
        int initStartHourSecondComponent = Integer.parseInt(initStartHour[1]);
        String[] initEndHour = endHourText.split("_");
        int initEndHourFirstComponent = Integer.parseInt(initEndHour[0]);
        int initEndHourSecondComponent = Integer.parseInt(initEndHour[1]);
        if(compareMilitaryHour(startHourText,endHourText) < 0){
            startsOnHalfHour = initStartHourSecondComponent == 30;
            endsOnHalfHour = initEndHourSecondComponent == 30;
            initOfficeHours(initStartHourFirstComponent, initEndHourFirstComponent);
        }
        else
        {
            startsOnHalfHour = false;
            endsOnHalfHour = false;
            initOfficeHours(MIN_START_HOUR,MAX_END_HOUR);
        }
    }

    public boolean containsTA(String testName) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsTAEmail(String testEmail){
        for(TeachingAssistant ta: teachingAssistants)
            if(ta.getEmail().equals(testEmail)){
                return true;
            }
        return false;
    }

    public void addTA(String initName, String initEmail){
        // MAKE THE TA
        TeachingAssistant ta = new TeachingAssistant(initName,initEmail);

        // ADD THE TA
        if (!containsTA(initName)) {
            teachingAssistants.add(ta);
        }

        // SORT THE TAS
        Collections.sort(teachingAssistants);
    }

    public void addOfficeHoursReservation(String day, String time, String taName) {
        String cellKey = getCellKey(day, time);
        toggleTAOfficeHours(cellKey, taName);
    }
    
    /**
     * This function toggles the taName in the cell represented
     * by cellKey. Toggle means if it's there it removes it, if
     * it's not there it adds it.
     */
    public void toggleTAOfficeHours(String cellKey, String taName) {
        StringProperty cellProp = officeHours.get(cellKey);
        String cellText = cellProp.getValue();
        String[] tas = cellText.split("\n");
        if(cellText.length() == 0)
            cellProp.setValue(taName);
        else if(!containsTA(taName,tas))
            cellProp.setValue(cellText + "\n" + taName);
        else
            replaceWith(cellProp, taName,"");
    }
    
    public void replaceAllInstances(String taName,String toReplaceWith)
    {
        for(int column = 2; column < 7; column++)
            for(int row = 1; row < getNumRows(); row++)
            {
                StringProperty cell = getCellTextProperty(column,row);
                String cellText = cell.getValue();
                if(cellText.contains(taName))
                    replaceWith(cell,taName,toReplaceWith);
            }       
    }
    
    public ArrayList<String> retrieveAllInstances(String taName)// returns timeslots not the actual ta
    {
        ArrayList<String> list = new ArrayList<>();
        for(int column = 2; column < 7; column++)
            for(int row = 1; row < getNumRows(); row++)
            {
                StringProperty cell = getCellTextProperty(column,row);
                String cellText = cell.getValue();
                if(cellText.contains(taName))
                    list.add(column+"_"+row+";"+cellText);
            }   
        return list;
    }
    
    public ArrayList<String> retrieveAllCellValues()
    {
         ArrayList<String> list = new ArrayList<>();
        for(int column = 2; column < 7; column++)
            for(int row = 1; row < getNumRows(); row++)
            {
                StringProperty cell = getCellTextProperty(column,row);
                String cellText = cell.getValue();
                if(!cellText.equals(""))
                    list.add(column+"_"+row+";"+cellText);
            }   
        return list;
    }
    
    public boolean containsTA(String taName, String[] taContainer)
    {
        for(String s: taContainer)
            if(s.equals(taName))
                return true;
        return false;
    }
    
     public void replaceWith(StringProperty cellProp, String taName, String toReplaceWith) {
        // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(taName)) {
            cellProp.setValue(toReplaceWith);
        }
        // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(taName) == 0 && cellText.substring(0,cellText.indexOf("\n") + 1).length() == taName.length()+1) {
            int startIndex = cellText.indexOf("\n") + 1;
            cellText = cellText.substring(startIndex);
            if(toReplaceWith.length() == 0)
                cellProp.setValue(cellText);
            else
                cellProp.setValue(toReplaceWith+"\n"+cellText);
        }
        // IT MUST BE ANOTHER TA IN THE CELL
        else {
            String[] tas = cellText.split("\n");
            String completePart = "";
            for(int i = 0; i < tas.length; i++)
            {
                if(!tas[i].equals(taName) && i < tas.length -1)
                    completePart += tas[i] + "\n";
                else if(!tas[i].equals(taName) && i == tas.length-1 )
                    completePart += tas[i];
                else if(tas[i].equals(taName)&& i == tas.length-1 && toReplaceWith.length() == 0)
                    completePart = completePart.substring(0,completePart.length()-1);
                if(toReplaceWith.length() != 0 && tas[i].equals(taName))
                    if(i < tas.length-1)
                        completePart += toReplaceWith + "\n";
                    else if(i == tas.length-1)
                        completePart += toReplaceWith;
            }
            cellProp.setValue(completePart);
        }
    }    
     
     public int getStartRowOffset(String newStart)
     {
         String[] parts = newStart.split("_");
         int difference = (startHour - Integer.parseInt(parts[0]))*2;
         if(getStartBoolean())
             difference++;
         if(Integer.parseInt(parts[1]) == 30)
             difference--;
         return difference;
     }
     
     public int getEndRowOffset(String newEnd)
     {
         String[] parts = newEnd.split("_");
         int difference = (endHour - Integer.parseInt(parts[0]))*2;
         if(getEndBoolean())
             difference++;
         if(Integer.parseInt(parts[1]) == 30)
             difference--;
         return difference;
     }
     
     public ArrayList<String> realignHours(String startHour,String endHour) //parameters in regular hours e.g. pm/am
     {
        int startOffset = getStartRowOffset(getMilitaryHour(startHour));
        int endOffset = getEndRowOffset(getMilitaryHour(endHour));
        ArrayList<String> changedHours = new ArrayList<>();
        int start = 1;
        int end = getNumRows();
        if(startOffset == 0 && endOffset == 0)
            start = end;
        if(startOffset < 0)
           start += startOffset*-1;
        if(endOffset > 0)
            end -= endOffset;
        for(int i = start;i < end;i++)
        {
            for(int c = 2; c < 7;c++)
            {
                int rowOffset = 0;
                if(startOffset != 0)
                    rowOffset = startOffset;
                String offsettedHours = c+"_"+(i+rowOffset)+";";
                String tas = officeHours.get(c+"_"+i).get();
                offsettedHours+=tas;
                changedHours.add(offsettedHours);
            }
        }
        return changedHours;
     }
     
     public void migrateHours(ArrayList<String> newHours)
     {
         for(int i = 0; i < newHours.size();i++)
         {
            String[] taLabel = newHours.get(i).split(";");
            if(taLabel.length == 1)
                continue;
            StringProperty label = officeHours.get(taLabel[0]);
            label.setValue(taLabel[1]);
         }
     }
     
     public String findEarliestHour(){
         int earliest = -1;
         for(int i = 1; i < getNumRows();i++)
         {
             for(int c = 2; c < 7; c++)
                 if(!officeHours.get(c+"_"+i).get().equals(""))
                 {
                     earliest = i;
                     break;
                 }
             if(earliest != -1)
                 break;
         }
         if(earliest == -1)
            return null;
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        return getMilitaryHour(workspace.getOfficeHoursGridTimeCellLabels().get("0_"+earliest).textProperty().get());
    }
     
    public String findLatestHour(){
        int latest = -1;
        for(int i = 1; i < getNumRows();i++)
         {
             for(int c = 2; c < 7; c++)
                 if(!officeHours.get(c+"_"+i).get().equals(""))
                 {
                     latest = i;
                 }                
         }
        if(latest == -1)
            return null;
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        return getMilitaryHour(workspace.getOfficeHoursGridTimeCellLabels().get("1_"+latest).textProperty().get());
    }
}